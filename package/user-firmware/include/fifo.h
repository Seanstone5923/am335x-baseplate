#ifndef FIFO_H
#define FIFO_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FIFO_t {
    volatile uint32_t in;
    volatile uint32_t out;
    uint32_t maxIndex;
    size_t itemSize;
    #ifndef __PRU__
    void (*AdvanceOutCallback)(void*);
    #else
    uint32_t AdvanceOutCallback;
    #endif
    uint8_t data[];
} FIFO_t;

FIFO_t *FIFO_Init(void *mem, size_t capacity, size_t itemSize);
size_t FIFO_Put(FIFO_t *fifo, void *item, size_t len);
size_t FIFO_Get(FIFO_t *fifo, void *item, size_t len);
void *FIFO_PeekIn(FIFO_t *fifo);
void *FIFO_PeekOut(FIFO_t *fifo);
void *FIFO_PeekN(FIFO_t *fifo, uint32_t n);
void *FIFO_AdvanceIn(FIFO_t *fifo, size_t num);
void *FIFO_AdvanceOut(FIFO_t *fifo, size_t num);
bool FIFO_IsFull(const FIFO_t *fifo);
bool FIFO_IsEmpty(const FIFO_t *fifo);
size_t FIFO_Length(const FIFO_t *fifo);
size_t FIFO_Available(const FIFO_t *fifo);
void FIFO_Reset(FIFO_t *fifo);

#ifdef __cplusplus
}
#endif

#endif

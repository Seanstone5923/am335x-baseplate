#ifndef MYASSERT_H
#define MYASSERT_H

#ifdef __cplusplus
extern "C" {
#endif

void _myassert(const char *exp, const char *file, int line);

#if defined(NDEBUG) || defined(__KERNEL__)
#define Assert(x) ((void)0)
#else
#define Assert(x) ((x) ? (void)0 : _myassert(#x, __FILE__, __LINE__))
#endif

#ifdef __cplusplus
}
#endif

#endif

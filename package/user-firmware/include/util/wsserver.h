#ifndef WSSERVER_H
#define WSSERVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <libwebsockets.h>

typedef struct WsMsg_t
{
    uint32_t len;
    enum lws_write_protocol protocol;
    unsigned char payload[2048];
} WsMsg_t;

typedef void (*WsServer_Handler_t)(void *in, size_t len);
typedef void (*WsServer_ConnectHandler_t)(void);

void WsServer_SetHandler (WsServer_Handler_t handler);
void WsServer_SetConnectHandler (WsServer_ConnectHandler_t handler);
int WsServer_Start (const char* docroot, const char* index);
void* WsServer_Main (void* settings);
int WsServer_Send(const char* str, int len, enum lws_write_protocol protocol);
int WsServer_vprintf(const char* fmt, va_list args);

#ifdef __cplusplus
}
#endif

#endif

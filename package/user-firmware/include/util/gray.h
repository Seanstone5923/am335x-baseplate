#ifndef UTIL_GRAY_H
#define UTIL_GRAY_H

#include <stdint.h>

static const uint8_t gray_1[2] =
{
    0b0,
    0b0,
};

static const uint8_t gray_2[3] =
{
    0b0,
    0b1,
    0b0,
};

static const uint8_t gray_4[5] =
{
    0b00,
    0b01,
    0b11,
    0b10,
    0b00,
};

static const uint8_t gray_8[9] =
{
    0b000,
    0b001,
    0b011,
    0b010,
    0b110,
    0b111,
    0b101,
    0b100,
    0b000,
};

static const uint8_t gray_16[17] =
{
    0b0000,
    0b0001,
    0b0011,
    0b0010,
    0b0110,
    0b0111,
    0b0101,
    0b0100,
    0b1100,
    0b1101,
    0b1111,
    0b1110,
    0b1010,
    0b1011,
    0b1001,
    0b1000,
    0b0000,
};

static inline const uint8_t* gray_n (int width)
{
    switch (width)
    {
        case 0: return gray_1;
        case 1: return gray_2;
        case 2: return gray_4;
        case 3: return gray_8;
        case 4: return gray_16;
        default: return 0;
    }
}

#endif

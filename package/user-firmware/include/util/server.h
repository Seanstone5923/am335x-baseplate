#ifndef SERVER_H
#define SERVER_H

#include <pthread.h>
#include <stdarg.h>
#include <soc/can.h>

#ifdef __cplusplus
#include <lib/json.hpp>
#include <set>
#include <string>
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern pthread_t Server_thread;
int Server_start(const char* docroot, const char* index);
void* Server_main(void* settings);
void Server_printf(const char *fmt, ...);
void Server_vprintf(const char *fmt, va_list args);
int Server_CAN_event(CAN_Frame_t *frame);

#ifdef __cplusplus
void Server_send (nlohmann::json msg);
void Server_update_client (std::set<std::string> entries);
void Server_onCommand (nlohmann::json::iterator it);
#endif

#ifdef __cplusplus
}
#endif

#endif

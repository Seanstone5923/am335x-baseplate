#ifndef MINMAX_H
#define MINMAX_H

#ifdef __cplusplus
extern "C" {
#endif

// Make sure we don't do double evaluation.

#define MIN(a,b) ({                    \
    __typeof__ (a) _a = (a);           \
    __typeof__ (b) _b = (b);           \
    _a < _b ? _a : _b;                 \
})

#define MAX(a,b) ({                    \
    __typeof__ (a) _a = (a);           \
    __typeof__ (b) _b = (b);           \
    _a > _b ? _a : _b;                 \
})

#ifdef __cplusplus
}
#endif

#endif

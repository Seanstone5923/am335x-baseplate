#ifndef SOC_I2C_H
#define SOC_I2C_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif
#include "soc/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__((packed)) I2C_t
{
    int I2Cx;
    GPIO_t SCL;
    GPIO_t SDA;
} I2C_t;

int I2C_ModuleClkConfig (const I2C_t* i2c);
int I2C_Reset (const I2C_t* i2c);
int I2C_PinMuxSetUp (const I2C_t* i2c);
int I2C_Init (const I2C_t* i2c);
int I2C_TransmitWithHeader (const I2C_t* i2c, uint32_t addr, int header_len, uint8_t* header, int len, uint8_t* data);
int I2C_Transmit (const I2C_t* i2c, uint32_t addr, int len, uint8_t* data);
int I2C_Receive (const I2C_t* i2c, uint32_t addr, int len, uint8_t* buffer);

#ifdef __cplusplus
}
#endif

#endif

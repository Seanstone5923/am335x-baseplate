#ifndef SOC_IRQ_H
#define SOC_IRQ_H

#ifdef __KERNEL__

#include <linux/interrupt.h>
#ifdef __COBALT__
#include <cobalt/kernel/intr.h>
#endif

typedef struct IRQ_t
{
    int irqn;
    const char* name;
    void* handler;
    #ifdef __COBALT__
    struct xnintr intr;
    #endif
} IRQ_t;

static int IRQ_register(IRQ_t* irq);
static int IRQ_unregister(IRQ_t* irq);

#endif

#endif

#ifndef SOC_TIMING_H
#define SOC_TIMING_H

#ifndef __PRU__

#include <sys/time.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define udelay usleep

static inline uint64_t millis (void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * (uint64_t)1000 + tv.tv_usec / 1000;
}

static inline uint64_t micros (void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}

static inline void printTime (void)
{
    static struct tm* tm_info = NULL;
    static struct timeval tv = {};
    tm_info = localtime(&tv.tv_sec);
    char format[64], buffer[64];
    strftime(format, sizeof(format), "[%Y/%m/%d %H:%M:%S:%%04u] ", tm_info);
    snprintf(buffer, sizeof(buffer), format, tv.tv_usec/1000);
    fprintf(stderr, buffer);
}

static inline unsigned int get_cyclecount (void)
{
    unsigned int value;
    // Read CCNT Register
    asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
    return value;
}

#else

static inline void udelay (uint32_t us)
{
    while (us--)
    {
        __delay_cycles(200000000/1000000);
    }
}

#endif

static inline void mdelay (uint32_t ms)
{
    udelay(1000 * ms);
}

#endif

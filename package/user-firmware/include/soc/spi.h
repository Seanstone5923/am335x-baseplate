#ifndef SOC_SPI_H
#define SOC_SPI_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdbool.h>
#endif
#include "soc/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__((packed)) SPI_t
{
    int SPIx;
    uint32_t baud;
    GPIO_t SCLK;
    GPIO_t CS;
    GPIO_t MISO;
    GPIO_t MOSI;
    uint8_t channel;
    uint8_t CPOL;
    uint8_t CPHA;
    bool single;
} SPI_t;

int SPI_ModuleClkConfig (const SPI_t* spi);
int SPI_PinMuxSetUp (const SPI_t* spi);
int SPI_config (const SPI_t* spi);
int SPI_init (const SPI_t* spi);
int SPI_set_baud (const SPI_t* spi);
uint8_t SPI_transfer (const SPI_t* spi, uint8_t data);
void SPI_transfern  (const SPI_t* spi, uint8_t* p_tx, uint8_t* p_rx, unsigned int length);
void SPI_transfern_with_header (const SPI_t* spi, volatile uint8_t* header, unsigned int header_length, volatile uint8_t* p_tx, volatile uint8_t* p_rx, unsigned int length);
int SPI_close (const SPI_t* spi);

#ifdef __cplusplus
}
#endif

#endif

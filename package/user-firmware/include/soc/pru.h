#ifndef SOC_PRU_H
#define SOC_PRU_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

int PRU_ModuleClkConfig (void);
int PRU_config (void);
int PRU_reset (int x);
int PRU_halt (int x);
int PRU_start (int x);
int PRU_cycle_reset (int x);
int PRU_RAM_zero (void);
int PRU_init (void);

#ifdef __KERNEL__
void PRU_mmap (void);
void PRU_unmap (void);
#endif

#ifdef __PRU__
#ifdef __GNUC__
#define __SIM__
#include <pru/io.h>
#else
volatile register unsigned __R30;
volatile register unsigned __R31;
#define read_r30() __R30
#define read_r31() __R31
#endif
void PRU_printf(const char *fmt, ...);
#define snprintf tfp_snprintf
#else
int PRU_firmware_load (int x, const char* filename);
int PRU_firmware_load_bin (int x, const char* filename);
int PRU_init_printf (void);
void* PRU_printf_main (void* data);
void PRU_printf_poll (void);
void PRU_printf_setEnabled (int x, bool enabled);
#endif

#define PRU0 0
#define PRU1 1

#define PRU_SHARED __attribute__((section(".shared_mem")))

extern const uint8_t PRUx;

#ifdef __cplusplus
}
#endif

#endif

#ifndef SOC_SPINLOCK_H
#define SOC_SPINLOCK_H

#ifdef __cplusplus
extern "C" {
#endif

void Spinlock_init (void);

#ifdef __cplusplus
}
#endif

#endif

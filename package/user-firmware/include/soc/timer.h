#ifndef SOC_TIMER_H
#define SOC_TIMER_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

int Timer_Init(uint8_t id);
int Timer_ModuleClkConfig(uint8_t id);
int Timer_SwReset(uint8_t id);
int Timer_SetCompare(uint8_t id, uint32_t compareVal);
int Timer_SetOverflow(uint8_t id, uint32_t overflowVal);
int Timer_SetFreq(uint8_t id, uint32_t freq);
void Timer_Start(uint8_t id);
void Timer_Stop(uint8_t id);
void Timer_Reload(uint8_t id);
void Timer_EnableIrq(uint8_t id, uint32_t irqFlags);
void Timer_DisableIrq(uint8_t id, uint32_t irqFlags);
void Timer_ClearPendingIrq(uint8_t id, uint32_t irqFlags);
uint32_t Timer_GetTicks(uint8_t id);

#ifdef __cplusplus
}
#endif

#endif

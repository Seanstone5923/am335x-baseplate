#ifndef SOC_DMA_H
#define SOC_DMA_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

int DMA_Init (void);
int DMA_ClkConfig (void);
void DMA_Channel_SetPaEntry (uint8_t channel, uint8_t paEntry);
void QDMA_Channel_SetPaEntry (uint8_t channel, uint8_t paEntry);
void DMA_EventEnable (uint8_t channel);
void DMA_EventSet (uint8_t channel);
int DMA_GetFreePaRAM (uint8_t number);

#define DMA_Channel(channel) channel
#define DMA_PaEntry(paEntry) paEntry

#ifdef __cplusplus
}
#endif

#endif

#ifndef SOC_GPIO_H
#define SOC_GPIO_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef uint8_t GPIO_t;

typedef enum
{
    GPIO_DIR_INPUT,
    GPIO_DIR_OUTPUT,
} GPIO_Dir;

typedef enum
{
    GPIO_PUD_OFF,
    GPIO_PUD_DOWN,
    GPIO_PUD_UP,
} GPIO_PUD;

typedef struct GPIO_Config_t
{
    GPIO_Dir  Dir;
    GPIO_PUD  PUD;
} GPIO_Config_t;

#define GPIO_HIGH 1
#define GPIO_LOW  0

int GPIO_enable (void);
int GPIO_init (void);
void GPIO_set_dir (GPIO_t gpio, GPIO_Dir dir);
void GPIO_write (GPIO_t gpio, uint8_t val);
uint8_t GPIO_read (GPIO_t gpio);

#ifdef __cplusplus
}
#endif

#endif

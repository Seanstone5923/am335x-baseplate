#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <stdint.h>
#include <time.h>
#include "soc/timer.h"
#include "am335x/dmtimer.h"

typedef struct TimestampSync_t
{
    volatile uint32_t ARM_ack;
    volatile uint32_t PRU_ack;
    volatile uint32_t ARM_ns;
    volatile uint32_t PRU_ns;
    volatile int32_t delta;
    volatile uint32_t samples;
} TimestampSync_t;

uint32_t TimespecToTOCR (struct timespec* ts);
void Timestamp_Get(struct timespec* ts);

#ifndef __PRU__
void Timestamp_Init (void);
void Timestamp_Sync (TimestampSync_t* sync);
#endif

#endif

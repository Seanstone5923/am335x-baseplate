#ifndef SOC_PRU_INTC_H
#define SOC_PRU_INTC_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

void PRU_INTC_SysEvent_Enable (uint8_t sysEvent);
void PRU_INTC_SysEvent_Disable (uint8_t sysEvent);
void PRU_INTC_SysEvent_SetChannel (uint8_t sysEvent, uint8_t channel);
void PRU_INTC_SysEvent_SetPending (uint8_t sysEvent);
void PRU_INTC_SysEvent_ClearPending (uint8_t sysEvent);
void PRU_INTC_Channel_SetHostInterrupt (uint8_t channel, uint8_t hostIntr);
void PRU_INTC_HostInterrupt_Enable (uint8_t hostIntr);
void PRU_INTC_HostInterrupt_Disable (uint8_t hostIntr);
int PRU_INTC_CheckInterrupt (uint8_t host);
int PRU_INTC_GetSysEvent (uint8_t host);

#ifndef __PRU__
#ifndef __KERNEL__
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
int PRU_INTC_Open (struct pollfd* pru_intc);
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif

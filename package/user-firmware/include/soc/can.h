#ifndef SOC_CAN_H
#define SOC_CAN_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdbool.h>
#endif

#include <soc/gpio.h>

#ifdef __cplusplus
extern "C" {
#endif

// The packed attribute is necessary to have the same offset on host and pru.
typedef struct __attribute__((__packed__)) CAN_Frame_t {
    uint16_t dlc : 4;
    uint16_t id : 11;
    uint64_t data;
} CAN_Frame_t;

typedef enum {
    SILENT = 0,
    LOOPBACK,
    EXT_LOOPBACK,
    NUM_TESTMODE
} CAN_TestMode_t;

typedef enum {
    CAN_RX = 0,
    CAN_TX,
} CAN_Dir_t;

#define CAN0 0
#define CAN1 1
#define CAN2 2

typedef struct __attribute__((packed)) CAN_Error_t
{
    bool warning :1;
    bool passive :1;
    bool busOff :1;
    uint8_t lec;
    uint8_t rec;
    uint8_t tec;
} CAN_Error_t;

typedef struct __attribute__((packed)) CAN_t
{
    int CANx;
    uint32_t bitrate;
    uint32_t numRxBufs;
    GPIO_t txPin;
    GPIO_t rxPin;
    bool loopback;
    bool extLoopback;
    CAN_Error_t error;
} CAN_t;

int CAN_Init(CAN_t* can);
int CAN_Transmit(CAN_t* can, CAN_Frame_t *frame);
int CAN_Receive(CAN_t* can, CAN_Frame_t *frame);
int CAN_SetTestMode(CAN_t* can, CAN_TestMode_t mode);
void CAN_PrintFrame(CAN_Frame_t *frame);
void CAN_xPrintFrame(CAN_t* can, CAN_Dir_t dir, CAN_Frame_t *frame);
void CAN_snPrintFrame(char* buffer, int size, CAN_Frame_t *frame);
void CAN_CheckError(CAN_t* can);
void CAN_PrintError(CAN_t* can);

#ifdef __cplusplus
}
#endif

#endif

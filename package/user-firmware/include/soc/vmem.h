#ifndef SOC_MMAP_H
#define SOC_MMAP_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __PRU__
#ifndef __KERNEL__
int vmem_init (void);
int vmem_deinit (void);
#else
void* vmem_mmap (unsigned long phys_addr, unsigned long size);
int vmem_unmap (void* addr);
#endif // #ifndef __KERNEL__
#endif // #ifndef __PRU__

#ifdef __cplusplus
}
#endif

#endif

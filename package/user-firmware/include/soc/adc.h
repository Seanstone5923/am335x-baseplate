#ifndef SOC_ADC_H
#define SOC_ADC_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

int ADC_clkconfig (void);
volatile uint32_t* ADC_STEPCONFIG (int step);
int ADC_step_continuous (int step, int continuous);
int ADC_step_input (int step, int port);
int ADC_step_enable (int step, int enable);
int ADC_enable (int enable);
int ADC_Init (void);
int ADC_config (void);
void ADC_clear_FIFO (void);
uint32_t ADC_convert_blocking (int step);
int ADC_EnableIRQ (uint32_t irq);
int ADC_DisableIRQ (uint32_t irq);
int ADC_ClearIRQ (uint32_t irq);
uint32_t ADC_CheckIRQ (uint32_t irq);
int ADC_EnableDMA (uint32_t* buffer, uint8_t numSteps);
int ADC_step_delay (int step, int delay);

#ifdef __cplusplus
}
#endif

#endif

#ifndef CBOR_UTIL_H
#define CBOR_UTIL_H

#ifndef __PRU__

#include <cbor.h>

void CBOR_add_Uint32(cbor_item_t * item, const char* name);
void CBOR_add_Uint8(cbor_item_t * item, const char* name);

#endif

#endif

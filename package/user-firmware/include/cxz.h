#ifndef CXZ_H
#define CXZ_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

static inline int clz (uint32_t x)
{
    #if defined(__PRU__) && !defined(__GNUC__)
    return 31 - __lmbd(x, 1);
    #else
    return __builtin_clz(x);
    #endif
}

static inline int clzll (uint64_t x)
{
    #if defined(__PRU__) && !defined(__GNUC__)
    for (int i = 0; i < 64; i++)
        if (x & (1ull << (63 - i))) return i;
    return 64;
    #else
    return __builtin_clzll(x);
    #endif
}

static inline int ctzll (uint64_t x)
{
    #if defined(__PRU__) && !defined(__GNUC__)
    for (int i = 0; i < 64; i++)
        if (x & (1ull << i)) return i;
    return 64;
    #else
    return __builtin_ctzll(x);
    #endif
}

static inline int ctz (uint32_t n)
{
    #if defined(__PRU__) && !defined(__GNUC__)
    static const signed char Mod37BitPosition[] = // map a bit value mod 37 to its position
    {
        -1, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4,
        7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5,
        20, 8, 19, 18
    };
    return Mod37BitPosition[(-n & n) % 37];
    #else
    return __builtin_ctz(n);
    #endif
}

#ifdef __cplusplus
}
#endif

#endif

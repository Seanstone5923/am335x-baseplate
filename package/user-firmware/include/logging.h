#ifndef LOGGING_H
#define LOGGING_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __KERNEL__

#define lprintf(args...) printk(KERN_INFO "pru-rt: " args)

#else

#include <stdbool.h>

#ifdef __PRU__

#include <tinyprintf/tinyprintf.h>
#include "soc/pru.h"
#define lprintf PRU_printf

#else

#include <stdarg.h>
void lprintf(const char *fmt, ...);
int Serial_open(void);
int (*Server_vprintf)(const char* fmt, va_list args);

void lprintf_Status(const char *str, bool status);

#endif // #ifdef __PRU__
#endif // #ifdef __KERNEL__



#ifdef __cplusplus
}
#endif

#endif

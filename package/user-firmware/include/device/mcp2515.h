/**
 * @file mcp2515.h
 * @brief MCP2515 CAN bus controller
 */


#ifndef DEVICE_MCP2515_H
#define DEVICE_MCP2515_H

#include <stdint.h>
#include <soc/can.h>
#include <soc/spi.h>
#include <soc/gpio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__((__packed__)) MCP2515_t
{
    SPI_t SPI;
    GPIO_t RESET;
    GPIO_t INT;
    GPIO_t SOF;
    GPIO_t RX0BF;
    GPIO_t RX1BF;
    GPIO_t TX0RTS;
    GPIO_t TX1RTS;
    GPIO_t TX2RTS;
    CAN_t CAN;
} MCP2515_t;

#define MCP2515_OSC 16000000    // PiCAN2
//#define MCP2515_OSC 20000000  // OBU

void MCP2515_reset (MCP2515_t* mcp2515);
uint8_t MCP2515_read (MCP2515_t* mcp2515, uint8_t addr);
void MCP2515_write (MCP2515_t* mcp2515, uint8_t addr, uint8_t val);

int MCP2515_reqop (MCP2515_t* mcp2515, uint8_t opmode);

int MCP2515_receive (MCP2515_t* mcp2515, CAN_Frame_t* frame);

int MCP2515_init (MCP2515_t* mcp2515);
int MCP2515_SetBitrate(MCP2515_t* mcp2515, uint32_t bitrate);

void MCP2515_CheckError(MCP2515_t* mcp2515);
int MCP2515_Transmit(MCP2515_t* mcp2515, CAN_Frame_t *frame);

/* Instructions */

#define MCP2515_RESET  0xC0
#define MCP2515_READ   0x03
#define MCP2515_WRITE  0x02

/* Registers */

#define MCP2515_CANCTRL 0x0F
#define MCP2515_CANCTRL_REQOP 0xE0

#define MCP2515_CANSTAT 0x0E
#define MCP2515_CANSTAT_OPMOD 0xE0

#define MCP2515_CNF1 0x2A
#define MCP2515_CNF1_SJW (0b11 << 6)
#define MCP2515_CNF1_BRP (0b111111 << 0)

#define MCP2515_CNF2 0x29
#define MCP2515_CNF2_BTLMODE    (1 << 7)
#define MCP2515_CNF2_SAM        (1 << 6)
#define MCP2515_CNF2_PHSEG1     (0b111 << 3)
#define MCP2515_CNF2_PRSEG      (0b111 << 0)

#define MCP2515_CNF3 0x28
#define MCP2515_CNF3_SOF        (1 << 7)
#define MCP2515_CNF3_WAKFIL     (1 << 6)
#define MCP2515_CNF3_PHSEG2     (0b111 << 0)

#define MCP2515_CANINTF 0x2C
#define MCP2515_CANINTF_MERRF   (1 << 7)
#define MCP2515_CANINTF_WAKIF   (1 << 6)
#define MCP2515_CANINTF_ERRIF   (1 << 5)
#define MCP2515_CANINTF_TX2IF   (1 << 4)
#define MCP2515_CANINTF_TX1IF   (1 << 3)
#define MCP2515_CANINTF_TX0IF   (1 << 2)
#define MCP2515_CANINTF_RX1IF   (1 << 1)
#define MCP2515_CANINTF_RX0IF   (1 << 0)

#define MCP2515_RXB0SIDH 0x61
#define MCP2515_RXB1SIDH 0x71

#define MCP2515_RXB0SIDL 0x62
#define MCP2515_RXB1SIDL 0x72

#define MCP2515_RXB0DLC 0x65
#define MCP2515_RXB1DLC 0x75

#define MCP2515_RXB0DM 0x66
#define MCP2515_RXB1DM 0x76

#define MCP2515_TXB0CTRL 0x30
#define MCP2515_TXB1CTRL 0x40
#define MCP2515_TXB2CTRL 0x50
#define MCP2515_TXBnCTRL_TXREQ (1 << 3)

#define MCP2515_TXB0CTL 0x30
#define MCP2515_TXB1CTL 0x40
#define MCP2515_TXB2CTL 0x50
#define MCP2515_TXBnCTL_TXREQ (1 << 3)

#define MCP2515_TXB0SIDH 0x31
#define MCP2515_TXB1SIDH 0x41
#define MCP2515_TXB2SIDH 0x51

#define MCP2515_TXB0SIDL 0x32
#define MCP2515_TXB1SIDL 0x42
#define MCP2515_TXB2SIDL 0x52
#define MCP2515_TXBnSIDL_SID (0x7 << 5)

#define MCP2515_TXB0DLC 0x35
#define MCP2515_TXB1DLC 0x45
#define MCP2515_TXB2DLC 0x55

#define MCP2515_TXB0D0 0x36
#define MCP2515_TXB1D0 0x46
#define MCP2515_TXB2D0 0x56

#define MCP2515_TEC 0x1C
#define MCP2515_REC 0x1D

#define MCP2515_EFLG 0x2D
#define MCP2515_EFLG_TXBO (1 << 5)
#define MCP2515_EFLG_TXEP (1 << 4)
#define MCP2515_EFLG_RXEP (1 << 3)
#define MCP2515_EFLG_TXWAR (1 << 2)
#define MCP2515_EFLG_RXWAR (1 << 1)
#define MCP2515_EFLG_EWARN (1 << 0)

/* Definitions */

#define MCP2515_OP_NORMAL       0x0
#define MCP2515_OP_SLEEP        0x1
#define MCP2515_OP_LOOPBACK     0x2
#define MCP2515_OP_LISTENONLY   0x3
#define MCP2515_OP_CONFIG       0x4

#ifdef __cplusplus
}
#endif

#endif

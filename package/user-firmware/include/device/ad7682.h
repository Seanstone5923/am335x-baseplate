#ifndef AD7682_H
#define AD7682_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdbool.h>
#endif

#include <soc/gpio.h>
#include <soc/spi.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__((__packed__)) AD7682_t
{
    SPI_t SPI;
} AD7682_t;

int AD7682_init (const AD7682_t* device);
int AD7682_read (const AD7682_t* device, uint16_t config, uint16_t* value);

#ifdef __cplusplus
}
#endif

#endif

#ifndef ADS1147_H
#define ADS1147_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stdbool.h>
#endif

#include <soc/gpio.h>
#include <soc/spi.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __attribute__((__packed__)) ADS1147_t
{
    SPI_t   SPI;
    GPIO_t  RESET;
    GPIO_t  START;
    GPIO_t  DRDY;
} ADS1147_t;

int ADS1147_init (const ADS1147_t* ads1147);
int ADS1147_setup (const ADS1147_t* ads1147);

#define ADS1147_MUX0   0x0
#define ADS1147_VBIAS  0x1
#define ADS1147_MUX1   0x2
#define ADS1147_SYS0   0x3
#define ADS1147_IDAC0  0xA
#define ADS1147_IDAC1  0xB

#define ADS1147_MUX0_MUX_SN        0b111
#define ADS1147_MUX0_MUX_SP        (0b111 << 3)
#define ADS1147_MUX0_BCS           (0b11 << 6)

#define ADS1147_MUX1_MUXCAL        0b111
#define ADS1147_MUX1_REFSELT       (0b11 << 3)
#define ADS1147_MUX1_VREFCON       (0b11 << 5)
#define ADS1147_MUX1_CLKSTAT       (0b1 << 7)

#define ADS1147_SYS0_DR            0b1111
#define ADS1147_SYS0_PGA           (0b111 << 4)

#define ADS1147_IDAC0_IMAG         0b111
#define ADS1147_IDAC0_DRDY_MODE    (0b1 << 3)
#define ADS1147_IDAC0_ID           (0b1111 << 4)

#define ADS1147_IDAC1_I2DIR        0b1111
#define ADS1147_IDAC1_I1DIR        (0b1111 << 4)

void        ADS1147_command (const ADS1147_t* ads1147, uint8_t command);
uint8_t     ADS1147_read    (const ADS1147_t* ads1147, uint8_t addr);
int         ADS1147_write   (const ADS1147_t* ads1147, uint8_t addr, uint8_t val);
uint8_t     ADS1147_readf   (const ADS1147_t* ads1147, uint8_t addr, uint8_t mask);
int         ADS1147_writef  (const ADS1147_t* ads1147, uint8_t addr, uint8_t mask, uint8_t val);
void        ADS1147_sync    (const ADS1147_t* ads1147);
int16_t     ADS1147_rdata   (const ADS1147_t* ads1147);
int16_t     ADS1147_rdatac  (const ADS1147_t* ads1147);

#ifdef __cplusplus
}
#endif

#endif

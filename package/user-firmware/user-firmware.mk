USER_FIRMWARE_VERSION := 0.7
USER_FIRMWARE_SITE := $(BR2_EXTERNAL)/package/user-firmware
USER_FIRMWARE_SITE_METHOD := local
USER_FIRMWARE_DEPENDENCIES += libwebsockets host-gnupru libcbor
USER_FIRMWARE_INSTALL_TARGET := YES

USER_FIRMWARE_MODULE_SUBDIRS += \
	src/driver/am335x/pru-rt

define USER_FIRMWARE_BUILD_CMDS
	$(MAKE) WORKING_DIR=$(@D) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" LD="$(TARGET_LD)" -C $(@D)
endef

define USER_FIRMWARE_INSTALL_TARGET_CMDS
	for d in `find $(@D) -type d -name bin`; do \
		$(INSTALL) -D -m 0755 $$d/* $(TARGET_DIR)/bin/; \
	done
	for f in `find $(@D) -path "*/pru/build*" -name *.elf`; do \
		$(INSTALL) -D -m 0755 $$f $(TARGET_DIR)/lib/firmware/; \
	done
	for f in `find $(@D) -path "*/pru/build*" -name *.bin`; do \
		$(INSTALL) -D -m 0755 $$f $(TARGET_DIR)/lib/firmware/; \
	done

	mkdir -p $(TARGET_DIR)/srv/
	cp -r $(@D)/www/* $(TARGET_DIR)/srv/
	cd $(TARGET_DIR)/srv; echo CACHE MANIFEST > cache.manifest; echo \# $$(date) >> cache.manifest; find * -type f \( ! -iname "cache.manifest" \) >> cache.manifest
endef

# $(eval $(kernel-module))
$(eval $(generic-package))

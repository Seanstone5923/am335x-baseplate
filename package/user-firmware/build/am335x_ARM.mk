SHELL := /bin/bash
include $(WORKING_DIR)/build/color.mk

################################ Compile Options ###############################
SUFFIX 		:= ARM
COLOR 		:= $(CYAN)
VPATH 		+= build ..
CPPFLAGS    += -march=armv7-a -mtune=cortex-a8 -O3 \
				-flto -ffat-lto-objects -ffunction-sections -fdata-sections \
				-Wall -Winline -Wno-packed-bitfield-compat \
				-fdiagnostics-color=always
CFLAGS      += -std=gnu11
CXXFLAGS    += -std=c++17
LDFLAGS     += -Wl,--gc-sections
LDFLAGS 	+= -lpthread
LDFLAGS 	+= -L$(WORKING_DIR)/lib/$(SUFFIX)

DEBUG ?= 0
ifeq ($(DEBUG),1)
	CPPFLAGS += -g -Og
else
	CPPFLAGS += -O3
endif
#===============================================================================

include $(WORKING_DIR)/build/common.mk

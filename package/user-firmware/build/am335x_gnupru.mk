SHELL := /bin/bash
include $(WORKING_DIR)/build/color.mk

################################ Compile Options ###############################
SUFFIX 		:= gnupru
COLOR		:= $(MAGENTA)
PATH 		:= $(HOST_DIR)/bin:$(PATH)
CPPFLAGS	+= -D__PRU__ -Os -flto \
				-fdiagnostics-color=always \
				-Wall -Wextra -Wno-main -Wno-packed-bitfield-compat
# Define this to squeeze code size by removing atexit, exit, constructors
# and destructors from CRT.
CPPFLAGS 	+= -minrt

CFLAGS      += -std=gnu11
CXXFLAGS    += -std=c++17
#LDFLAGS 	+= -mabi=ti
LDFLAGS 	+= -D__HEAP_SIZE=256 -D__STACK_SIZE=512
LDFLAGS 	+= -T$(WORKING_DIR)/build/am335x_gnupru.x
LDFLAGS 	+= -L$(HOST_DIR)/pru/lib -L$(WORKING_DIR)/lib/$(SUFFIX)

INC 		+= $(HOST_DIR)/pru/include
#===============================================================================

.PHONY: gnupru-elf
gnupru-elf: $(patsubst %.c, build/%.gnupru.elf, $(wildcard *.c))

override CC		:= pru-gcc
override CXX 	:= pru-g++
override AR		:= pru-gcc-ar
include $(WORKING_DIR)/build/common.mk

INC += $(WORKING_DIR)/include $(WORKING_DIR)/src/driver $(WORKING_DIR)/lib
COMMON_SRC_DIR += $(WORKING_DIR)/src
LIBS := $(patsubst -l%, $(WORKING_DIR)/lib/$(SUFFIX)/lib%.a, $(LDLIBS))
MSGOUT = 2>&1 | tee -a gcc_warnings.txt
DEPFLAGS += -MMD -MP -MF $@.d
CFLAGS += -D'lengthof(array)=(sizeof(array)/sizeof(array[0]))'

.SECONDARY:

################################### Commands ###################################
# CC command for CLPRU
ifeq ($(SUFFIX),clpru)
MAKECC 	= $(CC)  $(addprefix -I,$(INC)) $(CFLAGS) -fe $@ $<
MAKEELF = $(CXX) $(addprefix -I,$(INC)) $(CFLAGS) -z $^ $(LDFLAGS) $(LDLIBS) -o $@
MAKEBIN = hexpru -b -o $@ $<
else
# CC command for GNU toolchain (ARM & GNUPRU)
MAKECC 	= $(CC)  $(DEPFLAGS) $(addprefix -I,$(INC)) $(CPPFLAGS) $(CFLAGS)   -c $< -o $@ $(MSGOUT)
MAKECXX = $(CXX) $(DEPFLAGS) $(addprefix -I,$(INC)) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@ $(MSGOUT)
MAKEELF = $(CXX) $(DEPFLAGS) $(addprefix -I,$(INC)) $(CPPFLAGS) $(CXXFLAGS) $(CFLAGS) $^ $(LDFLAGS) $(LDLIBS) -o $@ $(MSGOUT)
MAKEBIN = $(MAKEELF)
endif
MAKELIB = $(MAKE) -C $(WORKING_DIR)/src/driver/$(patsubst lib%,%,$(notdir $*)) $(SUFFIX)

define lock_build
	@ # Create lock to prevent concurrent access by sub-Makes
	@ if [ -f $@.lock ]; then \
		while [ ! -f $@ ]; do true; done \
	else \
		mkdir -p $(@D)/build; \
		echo -e "$(COLOR)$(SUFFIX) $(1)       $<$(NC)"; \
		flock $@.lock $(2); \
	fi
endef

#===============================================================================


############################## Object files (*.o) ##############################
VPATH += $(WORKING_DIR)

# Add common sources
SRC += $(wildcard $(addsuffix /*.c,$(COMMON_SRC_DIR)))

OBJ += $(patsubst %, %.$(SUFFIX).o, $(SRC))

# For PRU, each source explicitly listed in the upper Makefile compiles to a firmware
ifneq ($(BUILD_LIB),y)
ifneq (,$(filter $(SUFFIX),gnupru clpru))
OBJ += %.c.$(SUFFIX).o
endif
endif

%.c.$(SUFFIX).o: %.c
	$(call lock_build, CC, $(MAKECC))

%.cpp.$(SUFFIX).o: %.cpp
	$(call lock_build, CXX, $(MAKECXX))

.PHONY: obj
obj: $(OBJ)
#===============================================================================


############################ Dependency lists (*.d) ############################
ifeq ($(SUFFIX),clpru)
-include $(OBJ:%.$(SUFFIX).o=%.pp)
else
-include $(OBJ:%.o=%.o.d)
%.d:
	@echo ""
endif
#===============================================================================


################################### Libraries ##################################
ifeq ($(BUILD_LIB),y)
%.a: $(OBJ)
	@mkdir -p $(@D)
	@echo -e "$(COLOR)$(SUFFIX) AR       $@$(NC)"
	@$(AR) rcsv $@ $^
else
%.a:
	$(call lock_build, LIB, $(MAKELIB))
endif
#===============================================================================


###################################### ELF #####################################
build/%.$(SUFFIX).elf: $(LIBS)
build/%.$(SUFFIX).elf: $(OBJ)
	@echo -e "$(COLOR)$(SUFFIX) ELF      $@$(NC)"
	@$(MAKEELF)
#===============================================================================


################################### Binaries ###################################
.PHONY: bin
bin: bin/$(BIN)
ifeq ($(SUFFIX),clpru)
build/$(BIN): build/%.elf
else
bin/$(BIN): $(LIBS)
bin/$(BIN): $(OBJ)
endif
	@mkdir -p $(@D)
	@echo -e "$(COLOR)$(SUFFIX) BIN      $@$(NC)"
	@$(MAKEBIN)
#===============================================================================


#################################### Clean #####################################
.PHONY: clean
clean:
	@echo -e "$(COLOR)CLEAN$(NC)"
	@rm -rf build bin lib
	@rm -f *.log
#===============================================================================

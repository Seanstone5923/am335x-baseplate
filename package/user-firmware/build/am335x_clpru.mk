SHELL := /bin/bash
include $(WORKING_DIR)/build/color.mk

################################ Compile Options ###############################
SUFFIX 	:= clpru
COLOR 	:= $(GREEN)
TI_CGT_PRU_INSTALLDIR := $(BASE_DIR)/toolchain/host/usr/share/ti-cgt-pru
PATH 	:= $(TI_CGT_PRU_INSTALLDIR)/bin:$(PATH)

CFLAGS	+= -D__PRU__ -O4
CFLAGS 	+= --c99 --mem_model:data=far -v3 --display_error_number --endian=little --hardware_mac=on \
			--obj_directory=. --pp_directory=. -ppd -ppa
LDFLAGS := $(WORKING_DIR)/build/am335x_clpru.x --search_path=$(WORKING_DIR)/lib/$(SUFFIX) $(LDFLAGS)
LDFLAGS += --stack_size=0x400 --heap_size=0x100
LDFLAGS += --reread_libs --warn_sections
LDFLAGS += -l $(TI_CGT_PRU_INSTALLDIR)/lib/libc.a
LDLIBS := $(patsubst -l%, -llib%.a, $(LDLIBS))

INC 	+= $(TI_CGT_PRU_INSTALLDIR)/usr/include $(TI_CGT_PRU_INSTALLDIR)/usr/include/am335x $(TI_CGT_PRU_INSTALLDIR)/include
#===============================================================================

.PHONY: pru-elf
pru-elf: $(patsubst %.c, build/%.clpru.elf, $(wildcard *.c))

override CC		:= clpru
override CXX 	:= clpru
include $(WORKING_DIR)/build/common.mk

CFLAGS +=  	-I$(HOST_DIR)/usr/include/xenomai/cobalt \
			-I$(HOST_DIR)/usr/include/xenomai \
			-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -Os -D_GNU_SOURCE -D_REENTRANT \
			-fasynchronous-unwind-tables -D__COBALT__  \
			-I$(HOST_DIR)/usr/include/xenomai/alchemy

LDFLAGS += 	-lm -lpthread -Wl,--no-as-needed -lalchemy \
			-lcopperplate $(TARGET_DIR)/usr/lib/xenomai/bootstrap.o \
			-Wl,--wrap=main -Wl,--dynamic-list=$(TARGET_DIR)/usr/lib/dynlist.ld \
			-L$(TARGET_DIR)/usr/lib \
			-lcobalt -lmodechk -lpthread -lrt \
			-L$(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib \
			-lfuse -pthread

"use strict";

var Bindings = {};

Bindings.looseJsonParse = function (obj)
{
    return Function('"use strict";return (' + obj + ')')();
}

Bindings.name2var = function (name)
{
    var index = null;
    var match = /(.*)(?:\[(\d+)\])/g.exec(name); // extract base name and array index if is array
    if (match !== null)
    {
        name = match[1];
        index = match[2];
    }

    let subnames = name.split(".");
    var scope = window;
    for (var i = 0; i < subnames.length - 1; i++)
        scope = scope[subnames[i]];
    var prop = subnames[subnames.length - 1];
    return {name: name, scope: scope, prop: prop, index: index};
}

Bindings.bind = function (name)
{
    let target = Bindings.name2var(name);
    let val = target.scope[target.prop];
    if (delete target.scope[target.prop]) { // can't watch constants
        Object.defineProperty(target.scope, target.prop, {
            val: val,
            get: function () { return val; },
            set: function (newval) { return val = Bindings.varChangeHandler(name, newval); },
            enumerable: true,
            configurable: true
        });
    }

    console.debug("\"%s\" binded", name);
    //return target.scope;
}

Bindings.varChangeHandler = function (name, newval)
{
    if (typeof(newval) !== 'undefined')
    {
        document.querySelectorAll(`[bind^="${name}"]`).forEach(function(element) {
            var val = newval;
            var index = Bindings.name2var(element.getAttribute("bind")).index;
            if (index !== null) val = val[index];

            if (element.hasAttribute("format"))
            {
                element.getAttribute("format").split(",").forEach(function(format) {
                    val = eval(format)(val);
                });
            }

            switch (element.nodeName)
            {
                case "DIV":
                case "SPAN":
                case "TD":
                    element.innerHTML = val;
                break;
                case "INPUT":
                    switch (element.type)
                    {
                        case "radio":
                            if (element.value == val) element.checked = true;
                        break;
                        case "checkbox":
                            if      (val == 1) element.checked = true;
                            else if (val == 0) element.checked = false;
                        break;
                        case "number":
                        default:
                            element.value = val;
                        break;
                    }
                break;
                case "SELECT":
                    element.value = val;
                break;
                default:
                    element.handler(val);
                break;
            }
        });
    }

    //console.debug(name, "=", newval);
    return newval;
}

Bindings.domChangeHandler = function (e)
{
    let element = e.target;
    let name = element.getAttribute("bind");
    let target = Bindings.name2var(name);
    let scope = target.scope;
    let prop = target.prop;

    switch (element.nodeName)
    {
        case "DIV":
        case "SPAN":
        case "TD":
            scope[prop] = element.innerHTML;
            break;
        case "INPUT":
            switch (element.type)
            {
                case "radio":
                    scope[prop] = document.querySelector(`input[bind="${name}"]:checked`).value;
                    break;
                case "checkbox":
                    scope[prop] = element.checked ? 1 : 0;
                    break;
                case "number":
                default:
                    scope[prop] = element.value;
            }
            break;
        case "SELECT":
            scope[prop] = element.value;
            break;
    }

    var v = scope[prop];
    var msg = {};
    msg[name] = isNaN(v) ? v : +v;
    //console.log(msg);
    Socket.send(CBOR.encode(msg));

    //console.debug(name, "->", scope[prop]);
    return scope[prop];
};

Bindings.setup = function ()
{
    let names = [];
    document.querySelectorAll("[bind]").forEach(function(element){
        element.addEventListener('change', Bindings.domChangeHandler);
        element.addEventListener('input', Bindings.domChangeHandler);
        var name = Bindings.name2var(element.getAttribute("bind")).name;
        if (!names.includes(name)) names.push(name);
    });

    names.forEach(function(name) {
        let target = Bindings.name2var(name);
        Bindings.varChangeHandler(name, target.scope[target.prop]);
        Bindings.bind(name);
    });

    console.debug("Bindings set up");
}

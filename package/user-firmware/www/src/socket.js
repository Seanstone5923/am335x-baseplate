"use strict";

var Socket = new ReconnectingWebSocket(
    "ws://" + document.domain + (location.port ? (':' + location.port) : ""),
    ["lws-minimal"], {debug: false, reconnectInterval: 500, binaryType: 'arraybuffer'}
);

Socket.readyStateFormat = function (e)
{
    if (e.val == 1) $("#offline-modal").hide();
    else $("#offline-modal").show();
    console.debug("Socket", e.val == 1 ? "connected" : "disconnected");
    return e.val;
}

Socket.onmessage = function (msg)
{
    if (typeof msg.data === "string")
        console.log(msg.data.replace(/\[.*m/g, '').replace(/\n/g, ''));
    else
    {
        let data = CBOR.decode(msg.data);
        //console.log(data);
        let keys = Object.keys(data);
        keys.forEach( function(key) {
            let target = Bindings.name2var(key);
            (target.index == null) ?
                target.scope[target.prop]               = data[key]:
                target.scope[target.prop][target.index] = data[key];
        });
    }
}

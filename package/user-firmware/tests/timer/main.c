#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <logging.h>
#include <soc/vmem.h>
#include <soc/pru.h>
#include "am335x/pru.h"
#include <soc/adc.h>
#include <soc/timer.h>
#include "am335x/dmtimer.h"

volatile int running = 0;
void CleanUp (int signum);

int main (int argc, char **argv)
{
    lprintf(
        "******************** Timer Main Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     CleanUp);
    signal (SIGTERM,    CleanUp);
    signal (SIGQUIT,    CleanUp);
    signal (SIGHUP,     CleanUp);

    vmem_init();
    PRU_init();
    PRU_firmware_load(PRU0, "/lib/firmware/timer_test.gnupru.elf");
    PRU_start(PRU0);

    uint32_t TimerFreq[8] = {0, 0, 0, 4000, 100, 20, 2, 0};
    for (int x = DMTIMER3; x <= DMTIMER6; x++)
    {
        Timer_Init(x);
        Timer_SetFreq(x, TimerFreq[x]);
    }

    for (int x = DMTIMER3; x <= DMTIMER6; x++)
        Timer_Start(x);

    running = 1;
    while (running)
    {
    }

    return 0;
}

void CleanUp (int signum)
{
    running = 0;
    for (int x = DMTIMER3; x <= DMTIMER6; x++)
    {
        Timer_Stop(x);
    }
    PRU_halt(PRU0);
    lprintf("Exit.\r\n");
    exit(0);
}

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <soc/pru.h>
#include <logging.h>
#include <soc/timer.h>
#include "am335x/dmtimer.h"

const uint8_t PRUx = PRU0;

void TimerHandler (int x)
{
    static int count[8] = {0};
    Timer_ClearPendingIrq(x, DMTIMER_IRQ_OVF);
    lprintf("[Timer %u] %u TCRR = %u.\n", x, ++count[x], DMTIMER[x]->TCRR);
}

void main (void)
{
    lprintf("Main loop start\r\n");
	while (1)
	{
        for (int x = DMTIMER3; x <= DMTIMER7; x++)
            if (DMTIMER[x]->IRQSTATUS & DMTIMER_IRQ_OVF)
                TimerHandler(x);
	}
}

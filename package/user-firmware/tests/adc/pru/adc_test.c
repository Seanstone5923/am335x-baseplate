#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <soc/pru.h>
#include <am335x/spinlock.h>
#include <soc/timing.h>
#include <soc/gpio.h>
#include <logging.h>

#include <soc/adc.h>
#include "am335x/adc_tsc.h"
#include "cxz.h"

const uint8_t PRUx = PRU0;

void main (void)
{
    ADC_Init();
    ADC_enable(true);
    for (int i = 1; i <= 8; i++)
    {
        ADC_step_continuous(i, false);
        ADC_step_input(i, i - 1);
    }

    int n = 0;
    uint32_t sum[8] = {0};
	while (1)
	{
        if (n < 50000)
        {
            for (int i = 1; i <= 8; i++)
            sum[i-1] += ADC_convert_blocking(i) * 1800 / 4096;
            n++;
        }
        else
        {
            lprintf(" %u %u %u %u %u %u %u %u\n", sum[0]/n, sum[1]/n, sum[2]/n, sum[3]/n, sum[4]/n, sum[5]/n, sum[6]/n, sum[7]/n);
            n = 0;
            for (int i = 1; i <= 8; i++) sum[i-1] = 0;
        }
	}
}

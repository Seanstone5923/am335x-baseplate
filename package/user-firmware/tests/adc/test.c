#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <logging.h>
#include <soc/vmem.h>
#include <soc/timing.h>
#include <soc/gpio.h>
#include <soc/spinlock.h>

#include "soc/pru.h"

void signal_handler (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** ADC Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     signal_handler);
    signal (SIGTERM,    signal_handler);
    signal (SIGQUIT,    signal_handler);
    signal (SIGHUP,     signal_handler);

    vmem_init();
    GPIO_init();
    Spinlock_init();

    PRU_init();
    PRU_firmware_load(PRU0, "/lib/firmware/adc_test.gnupru.elf");
    PRU_start(PRU0);

    running = 1;
    while (running)
    {
    }

    return 0;
}

void signal_handler (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    exit(0);
}

#include <stdint.h>
#include <string.h>
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"
#include "logging.h"
#include "soc/timing.h"

const uint8_t PRUx = PRU0;

uint32_t counter = 0;

PRU_SHARED uint32_t shared_test_1 = 0xDEADBEEF;
PRU_SHARED uint32_t shared_test_2 = 0xCAFECAFE;

void main(void)
{
	lprintf(
		"******************** PRU Shared Memory Test Program ********************\r\n"
		"Build: " __DATE__ " " __TIME__"\r\n\r\n"
	);

	lprintf("0x%08x: 0x%08x\n", &shared_test_1, shared_test_1);
	lprintf("0x%08x: 0x%08x\n", &shared_test_2, shared_test_2);

	while (1)
	{
	}
}

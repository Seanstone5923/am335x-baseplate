#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"

void Cleanup (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    lprintf(
        "******************** PRU Shared Memory Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     Cleanup);
    signal (SIGTERM,    Cleanup);
    signal (SIGQUIT,    Cleanup);
    signal (SIGHUP,     Cleanup);

    vmem_init();
    PRU_init();
    PRU_firmware_load(PRU0, "/lib/firmware/shared_mem.gnupru.elf");
    PRU_start(PRU0);

    running = 1;
    pause();

    return 0;
}

void Cleanup (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    exit(0);
}

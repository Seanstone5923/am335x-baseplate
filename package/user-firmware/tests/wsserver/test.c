#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include "logging.h"
#include "util/wsserver.h"

volatile int running = 0;
void signal_handler (int signum);

void Server_handler (void *in, size_t len)
{
    lprintf("%.*s\r\n", len, in);
}

int main (int argc, char **argv)
{
    lprintf(
        "******************** WsServer Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     signal_handler);
    signal (SIGTERM,    signal_handler);
    signal (SIGQUIT,    signal_handler);
    signal (SIGHUP,     signal_handler);

    WsServer_SetHandler(Server_handler);
    WsServer_Start("/srv", "index.html");

    running = 1;
    while (running)
    {
        char buffer[] = "Hello from server!";
        WsServer_Send(buffer, sizeof(buffer), LWS_WRITE_TEXT);
        sleep(1);
    }

    return 0;
}

void signal_handler (int signum)
{
    running = 0;
    lprintf("Exit.\r\n");
    exit(0);
}

#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

void signal_handler (int signum);

#ifdef __cplusplus
}
#endif

#endif

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <signal.h>
#include <unistd.h>
#include "logging.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/vmem.h"
#include "soc/dma.h"
#include "am335x/edma3.h"
#include "cxz.h"

void Cleanup (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** DMA Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     Cleanup);
    signal (SIGTERM,    Cleanup);
    signal (SIGQUIT,    Cleanup);
    signal (SIGHUP,     Cleanup);

    vmem_init();
    PRU_init();
    DMA_Init();

    EDMA3PaRAM[255] = (AM335X_EDMA3_PaRAM_t)
    {
        .DST = 0xC0000000,
        .ACNT = 1,
        .BCNT = 1,
        .BCNTRLD = 1,
        .CCNT = 2,
        .SRC = 0x00000000, // value stored
        .SRCCIDX = 1, // value to increment
        .OPT = EDMA3PaRAM_OPT_DAM | EDMA3PaRAM_OPT_ITCCHEN | (62 << ctz(EDMA3PaRAM_OPT_TCC)),
    };
    DMA_Channel_SetPaEntry(63, 255);

    EDMA3PaRAM[254] = (AM335X_EDMA3_PaRAM_t)
    {
        .SRC = (uint32_t)&EDMA3PaRAM[254].BCNT,
        .DST = (uint32_t)&EDMA3PaRAM[255].CCNT,
        .ACNT = 1,
        .BCNT = 2, // for storing value to transfer to EDMA3PaRAM[255].CCNT only. Only single transfer since transfer is A-synchronized
        .CCNT = 1,
        .OPT = EDMA3PaRAM_OPT_STATIC | EDMA3PaRAM_OPT_ITCCHEN | (63 << ctz(EDMA3PaRAM_OPT_TCC)),
    };
    DMA_Channel_SetPaEntry(62, 254);

    DMA_EventSet(63);

    running = 1;
    while (running)
    {
        lprintf("EDMA3PaRAM[255].SRC: 0x%08x\n", EDMA3PaRAM[255].SRC);
        //usleep(1000000);
    }

    return 0;
}

void Cleanup (int signum)
{
    running = 0;
    exit(0);
}

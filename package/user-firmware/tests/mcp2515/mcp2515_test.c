#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <logging.h>
#include <soc/vmem.h>
#include <soc/can.h>
#include "am335x/pinmux.h"
#include <soc/pru.h>
#include "am335x/pru.h"
#include <device/mcp2515.h>
#include "am335x/mcspi.h"
#include "am335x/control_module.h"
#include "main.h"

void signal_handler (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** MCP2515 CAN Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );
    vmem_init();
    PRU_init();

    *MCP2515 = (MCP2515_t)
    {
        .RESET = MCASP0_ACLKX,
        .INT = MCASP0_FSX,
        .SOF = MCASP0_AXR0,
        .TX0RTS = MCASP0_AHCLKR,
        .TX1RTS = MCASP0_ACLKR,
        .TX2RTS = MCASP0_FSR,
        .RX0BF = MCASP0_AXR1,
        .RX1BF = MCASP0_AHCLKX,
        .SPI.SPIx = SPI1,
        .SPI.SCLK = ECAP0_IN_PWM0_OUT,
        .SPI.MISO = UART0_CTSN,
        .SPI.MOSI = UART0_RTSN,
        .SPI.CS = XDMA_EVENT_INTR0,
        .SPI.baud = 10000000,
        .SPI.CPOL = 0,
        .SPI.CPHA = 0,
        .SPI.single = true,
        .CAN.CANx = CAN2,
        .CAN.bitrate = 1000000,
    };

    const char* firmware = "/lib/firmware/mcp2515_test.gnupru.elf";
    int c;
    while ((c = getopt (argc, argv, "b:")) != -1)
    {
        switch (c)
        {
            case 'b':
                MCP2515->CAN.bitrate = atoi(optarg);
                lprintf("MCP2515 bitrate: %u\n", MCP2515->CAN.bitrate);
            break;
            default:
                goto fail;
        }
    }
    while (optind < argc)
    {
        optind++;
    };

    if (MCP2515_init(MCP2515) < 0)
        return -1;

    lprintf("MCP2515_init ok\n");

    PRU_firmware_load(PRU0, firmware);
    PRU_start(PRU0);

    signal (SIGINT,     signal_handler);
    signal (SIGTERM,    signal_handler);
    signal (SIGQUIT,    signal_handler);
    signal (SIGHUP,     signal_handler);

    running = 1;
    pause();

    return 0;

fail:
    lprintf("Usage: mcp2515_test -b bitrate\r\n");
    return -1;
}

void signal_handler (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    exit(0);
}

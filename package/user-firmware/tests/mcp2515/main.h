#ifndef MAIN_H
#define MAIN_H

#include <device/mcp2515.h>
#include <am335x/pru.h>

#define MCP2515 ((MCP2515_t*)(PRU_DRAM[2] + 0x2800))

#endif

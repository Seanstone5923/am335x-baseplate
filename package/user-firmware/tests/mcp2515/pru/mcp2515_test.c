#include <soc/pru.h>
#include "am335x/pru.h"
#include <logging.h>
#include <soc/can.h>
#include "am335x/dcan.h"
#include <soc/timing.h>
#include "device/mcp2515.h"
#include "am335x/mcspi.h"
#include "am335x/pinmux.h"
#include "main.h"

const uint8_t PRUx = PRU0;

void main (void)
{
	lprintf(
		"******************** Hapith-1 MCP2515 CAN Test Program ********************\r\n"
		"Build: " __DATE__ " " __TIME__"\r\n\r\n"
	);
	while (1)
	{
		CAN_Frame_t frame;
		if (MCP2515_receive(MCP2515 , &frame) >= 0)
		{
			CAN_xPrintFrame(&MCP2515->CAN, CAN_RX, &frame);
		}
	}
}

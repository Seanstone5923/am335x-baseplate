#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include "logging.h"
#include "util/wsserver.h"
#include <cbor.h>

volatile int running = 0;
void signal_handler (int signum);

void Server_handler (void *in, size_t len)
{
    lprintf("%.*s\r\n", len, in);
}

int main (int argc, char **argv)
{
    lprintf(
        "******************** WsServer Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     signal_handler);
    signal (SIGTERM,    signal_handler);
    signal (SIGQUIT,    signal_handler);
    signal (SIGHUP,     signal_handler);

    WsServer_SetHandler(Server_handler);
    WsServer_Start("/srv", "index.html");

    cbor_item_t * root = cbor_new_definite_map(1);
    cbor_map_add(root, (struct cbor_pair) {
        .key = cbor_build_string("Count"),
        .value = cbor_build_uint32(0)
    });

    running = 1;
    while (running)
    {
        static uint32_t count = 0;
        static uint8_t buffer[1024];
        cbor_set_uint32(cbor_map_handle(root)[0].value, count);
        cbor_describe(root, stdout);
        size_t length = cbor_serialize(root, buffer, sizeof(buffer));
        WsServer_Send((const char*)buffer, length, LWS_WRITE_BINARY);
        sleep(1);

        count++;
    }

    return 0;
}

void signal_handler (int signum)
{
    running = 0;
    lprintf("Exit.\r\n");
    exit(0);
}

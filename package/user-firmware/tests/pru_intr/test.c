#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

void Cleanup (int signum);
volatile int running = 0;

struct pollfd pru_intc;

int main (int argc, char **argv)
{
    lprintf(
        "******************** PRU Interrupt Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    system("modprobe pru-rt");

    vmem_init();
    PRU_init();

    signal (SIGINT,     Cleanup);
    signal (SIGTERM,    Cleanup);
    signal (SIGQUIT,    Cleanup);
    signal (SIGHUP,     Cleanup);

    PRU_firmware_load(PRU0, "/lib/firmware/pru_intr_0.gnupru.elf");
    PRU_firmware_load(PRU1, "/lib/firmware/pru_intr_1.gnupru.elf");

    PRU_INTC_SysEvent_Enable(16);               /* Enable SysEvent 16 */
    PRU_INTC_SysEvent_SetChannel(16, 0);        /* Map SysEvent 16 to Channel 0 */
    PRU_INTC_Channel_SetHostInterrupt(0, 0);    /* Map Channel 0 to Host Interrupt 0 */
    PRU_INTC_HostInterrupt_Enable(0);           /* Enable Host Interrupt 0 */

    PRU_INTC_SysEvent_Enable(17);               /* Enable SysEvent 17 */
    PRU_INTC_SysEvent_SetChannel(17, 2);        /* Map SysEvent 17 to Channel 2 */
    PRU_INTC_Channel_SetHostInterrupt(2, 2);    /* Map Channel 2 to Host Interrupt 2 */
    PRU_INTC_HostInterrupt_Enable(2);           /* Enable Host Interrupt 2 */

    PRU_INTC_SysEvent_Enable(18);               /* Enable SysEvent 18 */
    PRU_INTC_SysEvent_SetChannel(18, 3);        /* Map SysEvent 18 to Channel 3 */
    PRU_INTC_Channel_SetHostInterrupt(3, 3);    /* Map Channel 3 to Host Interrupt 3 */
    PRU_INTC_HostInterrupt_Enable(3);           /* Enable Host Interrupt 3 */

    PRU_INTC_Open(&pru_intc);

    PRU_start(PRU0);
    PRU_start(PRU1);

    running = 1;
    while (running)
    {
        poll(&pru_intc, 1, -1);
        uint32_t cycles = PRU_CTRL[PRU1]->CYCLE;
        uint8_t sysevent[32];
        int n = read(pru_intc.fd, sysevent, sizeof(sysevent));
        for (int i = 0; i < n; i++) lprintf("[%u] SysEvent %u!\n", cycles, sysevent[i]);
    }

    return 0;
}

void Cleanup (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    PRU_halt(PRU1);
    system("rmmod pru-rt");
    close(pru_intc.fd);    
    exit(0);
}

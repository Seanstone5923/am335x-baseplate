#include <stdint.h>
#include <string.h>
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"
#include "logging.h"
#include "soc/timing.h"

const uint8_t PRUx = PRU1;

uint32_t counter = 0;

void main(void)
{
	lprintf(
		"******************** PRU Interrupt Test Firmware (PRU1) ********************\r\n"
		"Build: " __DATE__ " " __TIME__"\r\n\r\n"
	);

	while (1)
	{
		uint32_t cycles;

		cycles = PRU_CTRL[PRU1]->CYCLE;
		PRU_INTC_SysEvent_SetPending(16);
		lprintf("[%u] PRU_INTC_SysEvent_SetPending %u!\r\n", cycles, 16);

		cycles = PRU_CTRL[PRU1]->CYCLE;
		PRU_INTC_SysEvent_SetPending(17);
		lprintf("[%u] PRU_INTC_SysEvent_SetPending %u!\r\n", cycles, 17);

		cycles = PRU_CTRL[PRU1]->CYCLE;
		PRU_INTC_SysEvent_SetPending(18);
		lprintf("[%u] PRU_INTC_SysEvent_SetPending %u!\r\n", cycles, 18);

		udelay(500000);
	}
}

#include <stdint.h>
#include <string.h>
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"
#include "logging.h"
#include "soc/timing.h"

const uint8_t PRUx = PRU0;

uint32_t counter = 0;

void main(void)
{
	lprintf(
		"******************** PRU Interrupt Test Firmware (PRU0) ********************\r\n"
		"Build: " __DATE__ " " __TIME__"\r\n\r\n"
	);

	while (1)
	{
		if (PRU_INTC_CheckInterrupt(0))
		{
			int event = PRU_INTC_GetSysEvent(0);
			if (event >= 0)
			{
				uint32_t cycles = PRU_CTRL[PRU1]->CYCLE;
				PRU_INTC_SysEvent_ClearPending(event);
				lprintf("[%u] SysEvent %u!\r\n", cycles, event);
			}
		}
	}
}

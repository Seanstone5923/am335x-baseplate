#ifndef MAIN_H
#define MAIN_H

#include <stdbool.h>
#include <soc/can.h>
#include <device/mcp2515.h>
#include <am335x/pru.h>

typedef struct __attribute__((packed)) Global_t
{
    CAN_t CAN[2];
    MCP2515_t MCP2515;
    bool CAN_Enabled[3];
} Global_t;

#define Global (*(Global_t*)PRU_DRAM[2])

#endif

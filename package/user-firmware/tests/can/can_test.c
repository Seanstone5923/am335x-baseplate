#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <logging.h>
#include <soc/vmem.h>
#include <soc/can.h>
#include "am335x/dcan.h"
#include "am335x/pinmux.h"
#include <soc/pru.h>
#include "am335x/pru.h"
#include "am335x/control_module.h"
#include "soc/gpio.h"
#include "am335x/gpio.h"
#include <fifo.h>
#include "main.h"
#include <device/mcp2515.h>
#include "am335x/mcspi.h"

void signal_handler (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** CAN Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    vmem_init();
    PRU_init();

    Global.CAN[0] = (CAN_t)
    {
        .CANx = CAN0,
        .bitrate = 1000000,
        .numRxBufs = 32,
        .loopback = false,
        .extLoopback = false,
    };
    Global.CAN[1] = (CAN_t)
    {
        .CANx = CAN1,
        .bitrate = 1000000,
        .numRxBufs = 32,
        .loopback = false,
        .extLoopback = false,
    };

    Global.MCP2515 = (MCP2515_t)
    {
        .RESET = MCASP0_ACLKX,
        .INT = MCASP0_FSX,
        .SOF = MCASP0_AXR0,
        .TX0RTS = MCASP0_AHCLKR,
        .TX1RTS = MCASP0_ACLKR,
        .TX2RTS = MCASP0_FSR,
        .RX0BF = MCASP0_AXR1,
        .RX1BF = MCASP0_AHCLKX,
        .SPI.SPIx = SPI1,
        .SPI.SCLK = ECAP0_IN_PWM0_OUT,
        .SPI.MISO = UART0_CTSN,
        .SPI.MOSI = UART0_RTSN,
        .SPI.CS = XDMA_EVENT_INTR0,
        .SPI.baud = 4000000,
        .CAN.bitrate = 1000000,
    };

    for (int x = CAN0; x <= CAN2; x++) Global.CAN_Enabled[x] = false;

    const char* firmware[2] = {0, 0};

    int c;
    while ((c = getopt (argc, argv, "b:l")) != -1)
    {
        switch (c)
        {
            case 'b':
                Global.CAN[0].bitrate = atoi(optarg);
                Global.CAN[1].bitrate = atoi(optarg);
                lprintf("CAN0 bitrate: %u\n", Global.CAN[0].bitrate);
                lprintf("CAN1 bitrate: %u\n", Global.CAN[1].bitrate);
            break;
            case 'l': /* CAN loopback mode */
                Global.CAN[0].loopback = true;
                Global.CAN[1].loopback = true;
            break;
            default:
                goto fail;
        }
    }
    while (optind < argc)
    {
        else if (!strcmp(argv[optind], "can0"))
        {
            Global.CAN_Enabled[CAN0] = true;
        }
        else if (!strcmp(argv[optind], "can1"))
        {
            Global.CAN_Enabled[CAN1] = true;
        }
        else if (!strcmp(argv[optind], "can2"))
        {
            Global.CAN_Enabled[CAN2] = true;
        }

        else if (!strcmp(argv[optind], "tx"))
        {
            firmware[0] = "/lib/firmware/can_tx.gnupru.elf";
        }
        else if (!strcmp(argv[optind], "rx"))
        {
            firmware[1] = "/lib/firmware/can_rx.gnupru.elf";
        }
        else if (!strcmp(argv[optind], "trx"))
        {
            firmware[0] = "/lib/firmware/can_tx.gnupru.elf";
            firmware[1] = "/lib/firmware/can_rx.gnupru.elf";
        }
        else goto fail;
        optind++;
    };

    Global.CAN[0].txPin = UART1_CTSN;
    Global.CAN[0].rxPin = UART1_RTSN;
    Global.CAN[1].txPin = UART1_RXD;
    Global.CAN[1].rxPin = UART1_TXD;

    for (int x = CAN0; x <= CAN1; x++)
    {
        if (Global.CAN_Enabled[x])
        {
            lprintf("CAN%u loopback:  %u\n", x, Global.CAN[x].loopback);
            lprintf("CAN%u numRxBufs: %u\n", x, Global.CAN[x].numRxBufs);
            lprintf("CAN%u bitrate:   %u\n", x, Global.CAN[x].bitrate);
            lprintf("CAN%u TX: %u\n", x, Global.CAN[x].txPin);
            lprintf("CAN%u RX: %u\n", x, Global.CAN[x].rxPin);
            CAN_Init(&Global.CAN[x]);
        }
    }

    if (Global.CAN_Enabled[CAN2])
    {
        if (MCP2515_init(&Global.MCP2515) == 0)
            lprintf("MCP2515_init ok\n");
        else
            return -1;
    }

    if (firmware[0])
    {
        PRU_firmware_load(PRU0, firmware[0]);
        PRU_start(PRU0);
    }
    if (firmware[1])
    {
        PRU_firmware_load(PRU1, firmware[1]);
        PRU_start(PRU1);
    }

    signal (SIGINT,     signal_handler);
    signal (SIGTERM,    signal_handler);
    signal (SIGQUIT,    signal_handler);
    signal (SIGHUP,     signal_handler);

    running = 1;
    pause();

    return 0;

fail:
    lprintf("Usage: can_test -b bitrate <obu/sdm> <tx/rx/trx>\r\n");
    return -1;
}

void signal_handler (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    PRU_halt(PRU1);
    exit(0);
}

#include <soc/pru.h>
#include "am335x/pru.h"
#include <logging.h>
#include <soc/can.h>
#include "am335x/dcan.h"
#include <soc/timing.h>
#include "main.h"

const uint8_t PRUx = PRU0;

void main (void)
{
    lprintf(
        "******************** CAN TX Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    uint32_t count = 0;
	while (1)
	{
        CAN_Frame_t frame = {.id = 0x121, .dlc = 0x8, .data = count};

        for (int x = CAN0; x <= CAN1; x++)
        {
            if (Global.CAN_Enabled[x])
            {
                while (CAN_Transmit(&Global.CAN[x], &frame));
                CAN_xPrintFrame(&Global.CAN[x], CAN_TX, &frame);

                CAN_CheckError(&Global.CAN[x]);
            }
        }

        count++;
        udelay(50000);
	}
}

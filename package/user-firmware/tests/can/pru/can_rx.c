#include <soc/pru.h>
#include "am335x/pru.h"
#include <logging.h>
#include <soc/can.h>
#include "am335x/dcan.h"
#include <soc/timing.h>
#include "main.h"

const uint8_t PRUx = PRU1;

void main (void)
{
	lprintf(
		"******************** CAN RX Program ********************\r\n"
		"Build: " __DATE__ " " __TIME__"\r\n\r\n"
	);

	while (1)
	{
		for (int x = CAN0; x <= CAN1; x++)
		{
			if (Global.CAN_Enabled[x])
			{
				CAN_Frame_t frame;
				if (CAN_Receive(&Global.CAN[x], &frame))
				{
					CAN_xPrintFrame(&Global.CAN[x], CAN_RX, &frame);
				}
				CAN_CheckError(&Global.CAN[x]);
			}
		}
		if (Global.CAN_Enabled[CAN2])
		{
			CAN_Frame_t frame;
			if (MCP2515_receive(&Global.MCP2515 , &frame) >= 0)
			{
				CAN_xPrintFrame(&Global.MCP2515.CAN, CAN_RX, &frame);
			}
		}
	}
}

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "logging.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/timer.h"
#include "am335x/dmtimer.h"
#include "soc/timestamp.h"
#include "main.h"

const uint8_t PRUx = PRU0;

void main (void)
{
    struct timespec ts;
	while (1)
	{
        if (DMTIMER[3]->IRQSTATUS & DMTIMER_IRQ_OVF)
        {
            Timestamp_Get(&ts);

            if (!Global.TsSync.PRU_ack)
            {
                Global.TsSync.PRU_ack = true;
                Global.TsSync.PRU_ns = ts.tv_nsec;

                lprintf("%lu.%09u\n", (uint32_t)ts.tv_sec, ts.tv_nsec);

                if (Global.TsSync.ARM_ack)
                {
                    Timer_ClearPendingIrq(DMTIMER3, DMTIMER_IRQ_OVF);
                    int32_t delta = (int32_t)Global.TsSync.PRU_ns - Global.TsSync.ARM_ns;
                    Global.TsSync.PRU_ack = false;
                    Global.TsSync.ARM_ack = false;
                    lprintf("delta=%d\n", delta);
                    lprintf("\n");
                }
            }
        }
	}
}

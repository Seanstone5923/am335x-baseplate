#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include "logging.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/timer.h"
#include "am335x/dmtimer.h"
#include "soc/timestamp.h"
#include "main.h"

const uint8_t PRUx = PRU0;

void main (void)
{
    TimestampSync_t* tsSync = &Global.TsSync;

    struct timespec ts;
	while (tsSync->samples < 1000)
	{
        if (DMTIMER[3]->IRQSTATUS & DMTIMER_IRQ_OVF)
        {
            if (!tsSync->PRU_ack)
            {
                Timestamp_Get(&ts);

                tsSync->PRU_ack = true;
                tsSync->PRU_ns = ts.tv_nsec;

                if (tsSync->ARM_ack)
                {
                    Timer_ClearPendingIrq(DMTIMER3, DMTIMER_IRQ_OVF);
                    tsSync->delta += (int32_t)tsSync->PRU_ns - tsSync->ARM_ns;
                    tsSync->samples++;
                    tsSync->PRU_ack = false;
                    tsSync->ARM_ack = false;
                }
            }
        }
	}
    PRU_halt(PRU0);
}

#ifndef MAIN_H
#define MAIN_H

#include <stdint.h>
#include <stdbool.h>
#include "am335x/pru.h"

typedef struct __attribute__((packed)) Global_t
{
    TimestampSync_t TsSync;
} Global_t;

#define Global (*(Global_t*)PRU_DRAM[2])

#endif

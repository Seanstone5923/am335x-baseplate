#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/timer.h"
#include "am335x/dmtimer.h"
#include "soc/timestamp.h"
#include "main.h"

volatile int running = 0;
void CleanUp (int signum);

int main (int argc, char **argv)
{
    lprintf(
        "******************** Timestamp Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     CleanUp);
    signal (SIGTERM,    CleanUp);
    signal (SIGQUIT,    CleanUp);
    signal (SIGHUP,     CleanUp);

    vmem_init();
    PRU_init();

    Timestamp_Init();
    Timestamp_Sync(&Global.TsSync);

    Global.TsSync.PRU_ack = false;
    Global.TsSync.ARM_ack = false;

    PRU_firmware_load(PRU0, "/lib/firmware/timestamp_test.gnupru.elf");
    PRU_start(PRU0);

    Timer_ClearPendingIrq(DMTIMER3, DMTIMER_IRQ_OVF);
    Timer_SetFreq(DMTIMER3, 10);
    Timer_Start(DMTIMER3);

    struct timespec ts;
    running = 1;
    while (running)
    {
        if (DMTIMER[3]->IRQSTATUS & DMTIMER_IRQ_OVF)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW, &ts);

            if (!Global.TsSync.ARM_ack)
            {
                Global.TsSync.ARM_ack = true;
                Global.TsSync.ARM_ns = ts.tv_nsec;

                lprintf("%u.%09u\n", ts.tv_sec, ts.tv_nsec);

                if (Global.TsSync.PRU_ack)
                {
                    Timer_ClearPendingIrq(DMTIMER3, DMTIMER_IRQ_OVF);
                    int32_t delta = (int32_t)Global.TsSync.PRU_ns - Global.TsSync.ARM_ns;
                    Global.TsSync.PRU_ack = false;
                    Global.TsSync.ARM_ack = false;
                    lprintf("delta=%d\n", delta);
                    lprintf("\n");
                }
            }
        }
    }

    return 0;
}

void CleanUp (int signum)
{
    running = 0;
    Timer_Stop(DMTIMER3);
    PRU_halt(PRU0);
    lprintf("Exit.\r\n");
    exit(0);
}

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <signal.h>
#include <unistd.h>
#include "logging.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/vmem.h"
#include "soc/dma.h"
#include "am335x/edma3.h"

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** DMA Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    vmem_init();
    PRU_init();
    DMA_Init();

    for (int i = 0; i < 100; i++) PRU_DRAM[0][i] = i;

    for (int i = 0; i < 100; i++) lprintf("%u ", PRU_DRAM[0][i]);
    lprintf("\n");
    for (int i = 0; i < 100; i++) lprintf("%u ", PRU_DRAM[1][i]);
    lprintf("\n");

    EDMA3PaRAM[255] = (AM335X_EDMA3_PaRAM_t)
    {
        .SRC = (uint32_t)PRU_DRAM[0],
        .DST = (uint32_t)PRU_DRAM[1],
        .ACNT = 100,
        .BCNT = 1,
        .CCNT = 1,
    };

    DMA_Channel_SetPaEntry(63, 255);
    DMA_EventSet(63);

    usleep(100000);

    for (int i = 0; i < 100; i++) lprintf("%u ", PRU_DRAM[0][i]);
    lprintf("\n");
    for (int i = 0; i < 100; i++) lprintf("%u ", PRU_DRAM[1][i]);
    lprintf("\n");

    return 0;
}

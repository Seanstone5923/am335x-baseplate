#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"

void Cleanup (int signum);
volatile int running = 0;

int main (int argc, char **argv)
{
    fprintf(stderr,
        "******************** PRU Test Program ********************\r\n"
        "Build: " __DATE__ " " __TIME__"\r\n\r\n"
    );

    signal (SIGINT,     Cleanup);
    signal (SIGTERM,    Cleanup);
    signal (SIGQUIT,    Cleanup);
    signal (SIGHUP,     Cleanup);

    vmem_init();
    PRU_init();
    PRU_firmware_load(PRU0, "/lib/firmware/pru_test.clpru.elf");
    PRU_INTC_SysEvent_Enable(16);               /* Enable SysEvent 16 */
    PRU_INTC_SysEvent_SetChannel(16, 2);        /* Map SysEvent 16 to Channel 2 */
    PRU_INTC_Channel_SetHostInterrupt(2, 2);    /* Map Channel 2 to Host Interrupt 2 (ARM) */
    PRU_INTC_HostInterrupt_Enable(2);           /* Enable Host Interrupt 2 (ARM) */
    PRU_start(PRU0);

    running = 1;
    pause();

    return 0;
}

void Cleanup (int signum)
{
    running = 0;
    PRU_halt(PRU0);
    exit(0);
}

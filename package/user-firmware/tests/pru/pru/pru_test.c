#include <stdint.h>
#include <string.h>
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/pru_intc.h"
#include "logging.h"
#include "soc/timing.h"

const uint8_t PRUx = PRU0;

uint32_t counter = 0;

void main(void)
{
	while (1)
	{
		lprintf("Hello world %u!\r\n", counter++);
		PRU_INTC_SysEvent_SetPending(16);
		/* Set SysEvent 16 pending, so it will invoke Host Interrupt 2 on ARM.
		   The ARM interrupt handler is implemented in src/am335x/pru_intc.c
		*/
		udelay(100000);
	}
}

#include "logging.h"
#include <stdbool.h>

#ifndef __PRU__

#include <stdio.h>
#include <stdarg.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int Serial_fd;
int (*Server_vprintf)(const char* fmt, va_list args);

// __attribute__((constructor))
// int Serial_open (void)
// {
//     Serial_fd = open("/dev/ttyGS0", O_RDWR | O_NOCTTY | O_NONBLOCK);
//     return Serial_fd;
// }

void lprintf (const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    //vdprintf(Serial_fd, fmt, args);
    if (Server_vprintf) Server_vprintf(fmt, args);
    va_end(args);
}

#endif

void lprintf_Status(const char *str, bool status)
{
    status ?
    lprintf("[%s%s%s] %s\n", "\e[1;92m", "  OK  ", "\e[0m", str) :
    lprintf("[%s%s%s] %s\n", "\e[1;91m", "FAILED", "\e[0m", str);
}

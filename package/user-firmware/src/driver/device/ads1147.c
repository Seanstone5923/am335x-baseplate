#include <stdint.h>
#include <soc/spi.h>
#include <soc/gpio.h>
#include "cxz.h"
#include "logging.h"
#include "device/ads1147.h"
#include "soc/timing.h"

//#define DEBUG 1

int ADS1147_init (const ADS1147_t* ads1147)
{
    GPIO_init();

    GPIO_set_dir(ads1147->RESET, GPIO_DIR_OUTPUT);
    GPIO_write(ads1147->RESET, GPIO_LOW);

    GPIO_set_dir(ads1147->START, GPIO_DIR_OUTPUT);
    GPIO_write(ads1147->START, GPIO_LOW);

    GPIO_set_dir(ads1147->DRDY, GPIO_DIR_INPUT);
    //exti_set_trigger(EXTI2, EXTI_TRIGGER_FALLING); // TODO

    SPI_init(&ads1147->SPI);

    udelay(2);
    GPIO_write(ads1147->RESET, GPIO_HIGH);
    udelay(600);
    GPIO_write(ads1147->START, GPIO_HIGH);

    uint8_t id = ADS1147_readf(ads1147, ADS1147_IDAC0, ADS1147_IDAC0_ID);
    //lprintf("ADS1147 ID: 0x%x\n", id);
    if (id != 0x9) return -1;
    return 0;
}

int ADS1147_setup (const ADS1147_t* ads1147)
{
    int result = 0;

    if (ADS1147_writef(ads1147, ADS1147_MUX0,     ADS1147_MUX0_MUX_SP,    0x1) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_MUX0,     ADS1147_MUX0_MUX_SN,    0x2) < 0) result = 1;
    if (ADS1147_write (ads1147, ADS1147_VBIAS,    0x00) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_MUX1,     ADS1147_MUX1_VREFCON,   0x1) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_MUX1,     ADS1147_MUX1_REFSELT,   0x2) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_SYS0,     ADS1147_SYS0_PGA,       0x0) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_SYS0,     ADS1147_SYS0_DR,        0x2) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_IDAC0,    ADS1147_IDAC0_IMAG,     0x4) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_IDAC1,    ADS1147_IDAC1_I1DIR,    0x0) < 0) result = 1;
    if (ADS1147_writef(ads1147, ADS1147_IDAC1,    ADS1147_IDAC1_I2DIR,    0x3) < 0) result = 1;

    ADS1147_sync(ads1147);

    #if DEBUG
    for (int addr = 0x0; addr <= 0xE; addr++)
        lprintf("ADS1147 reg 0x%x: 0x%02x\n", addr, ADS1147_read(ads1147, addr));
    #endif

    return result;
}

void ADS1147_command (const ADS1147_t* ads1147, uint8_t command)
{
    SPI_transfer(&ads1147->SPI, command);
}

uint8_t ADS1147_read (const ADS1147_t* ads1147, uint8_t addr)
{
    uint8_t tx[3] = {0x20 | addr, 0x00, 0xFF};
    uint8_t rx[3];
    SPI_transfern(&ads1147->SPI, tx, rx, sizeof(tx));
    return rx[2];
}

uint8_t ADS1147_readf (const ADS1147_t* ads1147, uint8_t addr, uint8_t mask)
{
    return (ADS1147_read(ads1147, addr) & mask) >> ctz(mask);
}

int ADS1147_writef (const ADS1147_t* ads1147, uint8_t addr, uint8_t mask, uint8_t val)
{
    uint8_t wval = ADS1147_read(ads1147, addr);
    wval &= ~mask;
    wval |= ((val << ctz(mask)) & mask);

    uint8_t tx[4] = {0x40 | addr, 0x00, wval, 0xFF};
    uint8_t rx[4];
    SPI_transfern(&ads1147->SPI, tx, rx, sizeof(tx));

    uint8_t rval = ADS1147_read(ads1147, addr);
    if ((rval & mask) == (wval & mask))
        return 0; //lprintf("Write 0x%02x = 0x%02x ok\n", addr, wval);

    lprintf("Write 0x%02x = 0x%02x FAIL = 0x%02x\n", addr, wval, rval);
    return -1;
}

int ADS1147_write (const ADS1147_t* ads1147, uint8_t addr, uint8_t val)
{
    uint8_t tx[4] = {0x40 | addr, 0x00, val, 0xFF};
    uint8_t rx[4];
    SPI_transfern(&ads1147->SPI, tx, rx, sizeof(tx));
    uint8_t rval = ADS1147_read(ads1147, addr);
    if (rval == val)
        return 0; //lprintf("Write 0x%02x = 0x%02x ok\n", addr, val);

    lprintf("Write 0x%02x = 0x%02x FAIL = 0x%02x\n", addr, val, rval);
    return -1;
}

void ADS1147_sync (const ADS1147_t* ads1147)
{
    uint8_t tx[2] = {0x04, 0x04};
    SPI_transfern(&ads1147->SPI, tx, 0, sizeof(tx));
}

int16_t ADS1147_rdata (const ADS1147_t* ads1147)
{
    uint8_t tx[3] = {0x12, 0xFF, 0xFF};
    uint8_t rx[3];
    SPI_transfern(&ads1147->SPI, tx, rx, sizeof(tx));
    int16_t rdata = 0;
    rdata |= rx[1] << 8;
    rdata |= rx[2];
    return rdata;
}

int16_t ADS1147_rdatac (const ADS1147_t* ads1147)
{
    uint8_t tx[2] = {0xFF, 0xFF};
    uint8_t rx[2];
    SPI_transfern(&ads1147->SPI, tx, rx, sizeof(tx));
    int16_t rdata = 0;
    rdata |= rx[0] << 8;
    rdata |= rx[1];
    return rdata;
}

#include <stdint.h>
#include <soc/spi.h>
#include <am335x/mcspi.h>
#include "cxz.h"
#include "logging.h"
#include "device/ad7682.h"
#include "soc/timing.h"

int AD7682_init (const AD7682_t* device)
{
    SPI_init(&device->SPI);
    return ({uint16_t value; AD7682_read(device, 0xF120, &value);});
}

int AD7682_read (const AD7682_t* device, uint16_t config, uint16_t* value)
{
    /* The CFG register is latched (MSB first) on DIN with 14 SCK rising edges */
    uint8_t tx[] = {(config >> 8) & 0xFF, config & 0xFF, 0x00, 0x00};
    uint8_t rx[sizeof(tx)];
    SPI_transfern(&device->SPI, tx, rx, sizeof(tx));
    *value = rx[0] << 8 | rx[1];

    if ((rx[2] != ((config >> 8) & 0xFF)) || (rx[3] != (config & 0xFF)))
        return -1;

    return 0;
}

#include "device/mcp2515.h"
#include "soc/spi.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "cxz.h"
#include "soc/can.h"
#include <logging.h>
#include <soc/gpio.h>

void MCP2515_reset (MCP2515_t* mcp2515)
{
    uint8_t p_tx[1] = {MCP2515_RESET};
    SPI_transfern(&mcp2515->SPI, p_tx, 0, 1);
}

uint8_t MCP2515_read (MCP2515_t* mcp2515, uint8_t addr)
{
    uint8_t p_tx[3] = {MCP2515_READ, addr};
    uint8_t p_rx[3];
    SPI_transfern(&mcp2515->SPI, p_tx, p_rx, 3);
    return p_rx[2];
}

void MCP2515_write (MCP2515_t* mcp2515, uint8_t addr, uint8_t val)
{
    uint8_t p_tx[3] = {MCP2515_WRITE, addr, val};
    SPI_transfern(&mcp2515->SPI, p_tx, 0, 3);
}

/* Request operating mode switch */
int MCP2515_reqop (MCP2515_t* mcp2515, uint8_t opmode)
{
    // Read the CANCTRL register
    uint8_t canctrl = MCP2515_read(mcp2515, MCP2515_CANCTRL);

    // Clear the MCP2515_CANCTRL_REQOP bits and set it to "opmode"
    canctrl &= ~MCP2515_CANCTRL_REQOP;
    canctrl |= opmode << ctz(MCP2515_CANCTRL_REQOP);
    MCP2515_write(mcp2515, MCP2515_CANCTRL, canctrl);

    // Wait for opmode to take effect
    volatile int32_t timeout = 1000;
    while ((MCP2515_read(mcp2515, MCP2515_CANSTAT) & MCP2515_CANSTAT_OPMOD) >> ctz(MCP2515_CANSTAT_OPMOD) != opmode)
    {
        --timeout;
        if (timeout <= 0)
        {
            lprintf("Timeout waiting for MCP2515_reqop!\n");
            return -1;
        }
    }
    return 0;
}

/* Receive a CAN frame */
int MCP2515_receive (MCP2515_t* mcp2515, CAN_Frame_t* frame)
{
    uint32_t intf = MCP2515_read(mcp2515, MCP2515_CANINTF); // read interrupt flag

    if      (intf & MCP2515_CANINTF_RX0IF) // RX buffer 0 interrupt
    {
        intf &= ~MCP2515_CANINTF_RX0IF; // clear RX buffer 0 interrupt flag
        MCP2515_write(mcp2515, MCP2515_CANINTF, intf); // write back the interrupt flag
        frame->dlc = MCP2515_read(mcp2515, MCP2515_RXB0DLC);
        frame->id = (uint32_t)(MCP2515_read(mcp2515, MCP2515_RXB0SIDL) >> 5) | (MCP2515_read(mcp2515, MCP2515_RXB0SIDH) << 3);
        uint8_t* data = (uint8_t*) &frame->data;
        for (int i = 0; i < 8; i++) data[i] = MCP2515_read(mcp2515, MCP2515_RXB0DM + i);
        return 0;
    }
    else if (intf & MCP2515_CANINTF_RX1IF) // RX buffer 1 interrupt
    {
        intf &= ~MCP2515_CANINTF_RX1IF; // clear RX buffer 1 interrupt flag
        MCP2515_write(mcp2515, MCP2515_CANINTF, intf); // write back the interrupt flag
        frame->dlc = MCP2515_read(mcp2515, MCP2515_RXB1DLC);
        frame->id = (uint32_t)(MCP2515_read(mcp2515, MCP2515_RXB1SIDL) >> 5) | (MCP2515_read(mcp2515, MCP2515_RXB1SIDH) << 3);
        uint8_t* data = (uint8_t*) &frame->data;
        for (int i = 0; i < 8; i++) data[i] = MCP2515_read(mcp2515, MCP2515_RXB1DM + i);
        return 1;
    }

    return -1;
}

int MCP2515_init (MCP2515_t* mcp2515)
{
    GPIO_init();
    GPIO_set_dir(mcp2515->RESET, GPIO_DIR_OUTPUT);
    GPIO_set_dir(mcp2515->INT, GPIO_DIR_INPUT);
    GPIO_set_dir(mcp2515->SOF, GPIO_DIR_INPUT);
    GPIO_set_dir(mcp2515->RX0BF, GPIO_DIR_INPUT);
    GPIO_set_dir(mcp2515->RX1BF, GPIO_DIR_INPUT);
    GPIO_set_dir(mcp2515->TX0RTS, GPIO_DIR_OUTPUT);
    GPIO_set_dir(mcp2515->TX1RTS, GPIO_DIR_OUTPUT);
    GPIO_set_dir(mcp2515->TX2RTS, GPIO_DIR_OUTPUT);
    GPIO_write(mcp2515->TX0RTS, GPIO_HIGH);
    GPIO_write(mcp2515->TX1RTS, GPIO_HIGH);
    GPIO_write(mcp2515->TX2RTS, GPIO_HIGH);
    GPIO_write(mcp2515->RESET, GPIO_HIGH);

    SPI_init(&mcp2515->SPI);

    //uint8_t canctrl = MCP2515_read(mcp2515, MCP2515_CANCTRL);
    //lprintf("MCP2515_CANCTRL: 0x%02x\n", canctrl);

    MCP2515_reset(mcp2515);
    MCP2515_reqop(mcp2515, MCP2515_OP_CONFIG);
    MCP2515_SetBitrate(mcp2515, mcp2515->CAN.bitrate);
    return MCP2515_reqop(mcp2515, MCP2515_OP_NORMAL);
}

int MCP2515_SetBitrate(MCP2515_t* mcp2515, uint32_t bitrate)
{
    switch (bitrate)
    {
        case 1000000:
            /* Config for 20 MHz OSC
               https://www.kvaser.com/support/calculators/bit-timing-calculator/
            */
            MCP2515_write(mcp2515, MCP2515_CNF1, 0x00);
            MCP2515_write(mcp2515, MCP2515_CNF2, 0x92);
            MCP2515_write(mcp2515, MCP2515_CNF3, 0x02);
        break;
    }

    // lprintf("MCP2515_SetBitrate\n");
    // lprintf("    Synchonization Jump Witdh: %u TQ\n",
    //     ((MCP2515_read(mcp2515, MCP2515_CNF1) & MCP2515_CNF1) >> ctz(MCP2515_CNF1_SJW)) + 1);
    // lprintf("    Propagation segment:       %u TQ\n",
    //     ((MCP2515_read(mcp2515, MCP2515_CNF2) & MCP2515_CNF2_PRSEG) >> ctz(MCP2515_CNF2_PRSEG)) + 1);
    // lprintf("    PS1:                       %u TQ\n",
    //     ((MCP2515_read(mcp2515, MCP2515_CNF2) & MCP2515_CNF2_PHSEG1) >> ctz(MCP2515_CNF2_PHSEG1)) + 1);
    // lprintf("    PS2:                       %u TQ\n",
    //     ((MCP2515_read(mcp2515, MCP2515_CNF3) & MCP2515_CNF3_PHSEG2) >> ctz(MCP2515_CNF3_PHSEG2)) + 1);

    return 0;
}

void MCP2515_CheckError(MCP2515_t* mcp2515)
{
    uint8_t eflg = MCP2515_read(mcp2515, MCP2515_EFLG);
    mcp2515->CAN.error.warning = (eflg & MCP2515_EFLG_EWARN) >> ctz(MCP2515_EFLG_EWARN);
    mcp2515->CAN.error.passive = ((eflg & MCP2515_EFLG_TXEP) >> ctz(MCP2515_EFLG_TXEP))
                                    | ((eflg & MCP2515_EFLG_RXEP) >> ctz(MCP2515_EFLG_RXEP));
    mcp2515->CAN.error.busOff  = (eflg & MCP2515_EFLG_TXBO) >> ctz(MCP2515_EFLG_TXBO);

    mcp2515->CAN.error.rec = MCP2515_read(mcp2515, MCP2515_REC);
    mcp2515->CAN.error.tec = MCP2515_read(mcp2515, MCP2515_TEC);
}

int MCP2515_Transmit(MCP2515_t* mcp2515, CAN_Frame_t *frame)
{
    int n = 0; // buffer num
    uint8_t txbnctl;
    for (;;)
    {
        txbnctl = MCP2515_read(mcp2515, n * 0x10 + MCP2515_TXB0CTL);
        if (txbnctl & MCP2515_TXBnCTL_TXREQ) n++;
        else break;
        if (n > 2) return -1;
    }

    MCP2515_write(mcp2515, n * 0x10 + MCP2515_TXB0SIDH, frame->id >> 3);
    MCP2515_write(mcp2515, n * 0x10 + MCP2515_TXB0SIDL, (frame->id << ctz(MCP2515_TXBnSIDL_SID)) & MCP2515_TXBnSIDL_SID);

    MCP2515_write(mcp2515, n * 0x10 + MCP2515_TXB0DLC, frame->dlc);
    for (int i = 0; i < frame->dlc; i++)
        MCP2515_write(mcp2515, n * 0x10 + MCP2515_TXB1D0 + i, (frame->data >> (8 * i)) & 0xFF);

    txbnctl |= MCP2515_TXBnCTL_TXREQ;
    MCP2515_write(mcp2515, n * 0x10 + MCP2515_TXB0CTL, txbnctl);

    return 0;
}

#include "soc/dma.h"
#include "am335x/edma3.h"
#include "soc/vmem.h"
#include "logging.h"
#include "cxz.h"

int DMA_Init (void)
{
    DMA_ClkConfig();
    return 0;
}

int DMA_ClkConfig (void)
{
    return 0;
}

/* Sets the PaRAM number for the DMA channel */
void DMA_Channel_SetPaEntry (uint8_t channel, uint8_t paEntry)
{
    EDMA3CC->DCHMAP[channel] = paEntry << ctz(EDMA3CC_CHMAP_PAENTRY);
}

/* Sets the PaRAM number for the QDMA channel */
void QDMA_Channel_SetPaEntry (uint8_t channel, uint8_t paEntry)
{
    EDMA3CC->QCHMAP[channel] = paEntry << ctz(EDMA3CC_CHMAP_PAENTRY);
}

void DMA_EventEnable (uint8_t channel)
{
    EDMA3CC->EESR |= 1ull << channel;
}

void DMA_EventSet (uint8_t channel)
{
    EDMA3CC->ESR |= 1ull << channel;
}

int DMA_GetFreePaRAM (uint8_t number)
{
    static uint8_t FreePaRAM = 255;
    if (FreePaRAM >= number)
    {
        FreePaRAM -= number;
        return FreePaRAM;
    }
    return -1;
}

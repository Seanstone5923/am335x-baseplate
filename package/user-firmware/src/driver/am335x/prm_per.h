#ifndef AM335X_PRM_PER_H
#define AM335X_PRM_PER_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_PRM_PER_t
{
    volatile uint32_t RSTCTRL;
    volatile uint32_t Reserved0;
    volatile uint32_t PWRSTST;
    volatile uint32_t PWRSTCTRL;
} AM335X_PRM_PER_t;

#define PRM_PER_BASE 0x44E00C00

#ifndef __KERNEL__
static AM335X_PRM_PER_t* const PRM_PER = (AM335X_PRM_PER_t*)PRM_PER_BASE;
#else
static AM335X_PRM_PER_t* PRM_PER;
#endif

#define PRM_PER_RSTCTRL_PRU_ICSS_LRST (1 << 1)

#endif

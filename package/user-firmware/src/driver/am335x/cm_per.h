#ifndef AM335X_CM_PER_H
#define AM335X_CM_PER_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_CM_PER_t
{
    /* 00 */    volatile uint32_t L4LS_CLKSTCTRL;
    /* 04 */    volatile uint32_t L3S_CLKSTCTRL;
                volatile uint32_t Reserved0;
    /* 0C */    volatile uint32_t L3_CLKSTCTRL;
                volatile uint32_t Reserved1;
    /* 14 */    volatile uint32_t CPGMAC0_CLKCTRL;
    /* 18 */    volatile uint32_t LCDC_CLKCTRL;
    /* 1C */    volatile uint32_t USB0_CLKCTRL;
                volatile uint32_t Reserved2;
    /* 24 */    volatile uint32_t TPTC0_CLKCTRL;
    /* 28 */    volatile uint32_t EMIF_CLKCTRL;
    /* 2C */    volatile uint32_t OCMCRAM_CLKCTRL;
    /* 30 */    volatile uint32_t GPMC_CLKCTRL;
    /* 34 */    volatile uint32_t MCASP0_CLKCTRL;
    /* 38 */    volatile uint32_t UART5_CLKCTRL;
    /* 3C */    volatile uint32_t MMC0_CLKCTRL;
    /* 40 */    volatile uint32_t ELM_CLKCTRL;
    /* 44 */    volatile uint32_t I2C2_CLKCTRL;
    /* 48 */    volatile uint32_t I2C1_CLKCTRL;
    /* 4C */    volatile uint32_t SPI0_CLKCTRL;
    /* 50 */    volatile uint32_t SPI1_CLKCTRL;
                volatile uint32_t Reserved3[3];
    /* 60 */    volatile uint32_t L4LS_CLKCTRL;
                volatile uint32_t Reserved4;
    /* 68 */    volatile uint32_t MCASP1_CLKCTRL;
    /* 6C */    volatile uint32_t UART1_CLKCTRL;
    /* 70 */    volatile uint32_t UART2_CLKCTRL;
    /* 74 */    volatile uint32_t UART3_CLKCTRL;
    /* 78 */    volatile uint32_t UART4_CLKCTRL;
    /* 7C */    volatile uint32_t TIMER7_CLKCTRL;
    /* 80 */    volatile uint32_t TIMER2_CLKCTRL;
    /* 84 */    volatile uint32_t TIMER3_CLKCTRL;
    /* 88 */    volatile uint32_t TIMER4_CLKCTRL;
                volatile uint32_t Reserved5[8];
    /* AC */    volatile uint32_t GPIO1_CLKCTRL;
    /* B0 */    volatile uint32_t GPIO2_CLKCTRL;
    /* B4 */    volatile uint32_t GPIO3_CLKCTRL;
                volatile uint32_t Reserved6;
    /* BC */    volatile uint32_t TPCC_CLKCTRL;
    /* C0 */    volatile uint32_t DCAN0_CLKCTRL;
    /* C4 */    volatile uint32_t DCAN1_CLKCTRL;
                volatile uint32_t Reserved7;
    /* CC */    volatile uint32_t EPWMSS1_CLKCTRL;
                volatile uint32_t Reserved8;
    /* D4 */    volatile uint32_t EPWMSS0_CLKCTRL;
    /* D8 */    volatile uint32_t EPWMSS2_CLKCTRL;
    /* DC */    volatile uint32_t L3_INSTR_CLKCTRL;
    /* E0 */    volatile uint32_t L3_CLKCTRL;
    /* E4 */    volatile uint32_t IEEE5000_CLKCTRL;
    /* E8 */    volatile uint32_t PRU_CLKCTRL;
    /* EC */    volatile uint32_t TIMER5_CLKCTRL;
    /* F0 */    volatile uint32_t TIMER6_CLKCTRL;
    /* F4 */    volatile uint32_t MMC1_CLKCTRL;
    /* F8 */    volatile uint32_t MMC2_CLKCTRL;
    /* FC */    volatile uint32_t TPTC1_CLKCTRL;
    /* 100 */   volatile uint32_t TPTC2_CLKCTRL;
                volatile uint32_t Reserved9[2];
    /* 10C */   volatile uint32_t SPINLOCK_CLKCTRL;
    /* 110 */   volatile uint32_t MAILBOX0_CLKCTRL;
                volatile uint32_t Reserved10[2];
    /* 11C */   volatile uint32_t L4HS_CLKSTCTRL;
    /* 120 */   volatile uint32_t L4HS_CLKCTRL;
                volatile uint32_t Reserved11[2];
    /* 12C */   volatile uint32_t OCPWP_L3_CLKSTCTRL;
    /* 130 */   volatile uint32_t OCPWP_CLKCTRL;
                volatile uint32_t Reserved12[3];
    /* 140 */   volatile uint32_t PRU_CLKSTCTRL;
    /* 144 */   volatile uint32_t CPSW_CLKSTCTRL;
    /* 148 */   volatile uint32_t LCDC_CLKSTCTRL;
    /* 14C */   volatile uint32_t CLKDIV32K_CLKCTRL;
    /* 150 */   volatile uint32_t CLK_24MHZ_CLKSTCTRL;
} AM335X_CM_PER_t;

#define CM_PER_BASE 0x44E00000

#ifndef __KERNEL__
static AM335X_CM_PER_t* const CM_PER = (AM335X_CM_PER_t*)CM_PER_BASE;
#else
static AM335X_CM_PER_t* CM_PER;
#endif

#define CM_CLKCTRL_ENABLE 0x2
#define CM_CLKCTRL_DISABLED (1 << 16)
#define CM_CLKCTRL_IDLEST (0b11 << 16)

#define CLKACTIVITY_PRU_ICSS_OCP_GCLK (1 << 4)

#endif

#include "logging.h"
#include "soc/vmem.h"
#include "am335x/cm_per.h"
#include "am335x/cm_wkup.h"
#include "am335x/control_module.h"
#include "soc/gpio.h"
#include "am335x/gpio.h"
#include "am335x/dmtimer.h"
#include "cxz.h"
#include "soc/timer.h"

int Timer_ModuleClkConfig(uint8_t id)
{
    volatile uint32_t *regClkCtrl;

    switch (id) {
    case 0:
        regClkCtrl = &CM_WKUP->WKUP_TIMER0_CLKCTRL;
        break;
    case 1:
        regClkCtrl = &CM_WKUP->WKUP_TIMER1_CLKCTRL;
        break;
    case 2:
        regClkCtrl = &CM_PER->TIMER2_CLKCTRL;
        break;
    case 3:
        regClkCtrl = &CM_PER->TIMER3_CLKCTRL;
        break;
    case 4:
        regClkCtrl = &CM_PER->TIMER4_CLKCTRL;
        break;
    case 5:
        regClkCtrl = &CM_PER->TIMER5_CLKCTRL;
        break;
    case 6:
        regClkCtrl = &CM_PER->TIMER6_CLKCTRL;
        break;
    case 7:
        regClkCtrl = &CM_PER->TIMER7_CLKCTRL;
        break;
    default:
        return -1;
    }

    do {
        *regClkCtrl |= CM_CLKCTRL_ENABLE;
    } while (*regClkCtrl & CM_CLKCTRL_IDLEST);

    return 0;
}

int Timer_SwReset(uint8_t id)
{
    DMTIMER[id]->TSICR |= DMTIMER_TSICR_SFT;
    while (DMTIMER[id]->TIOCP_CFG & DMTIMER_TIOCP_CFG_SOFTRESET)
        ;
    return 0;
}

int Timer_SetCompare(uint8_t id, uint32_t compareVal)
{
    DMTIMER[id]->TCLR |= DMTIMER_TCLR_CE;
    do {
        DMTIMER[id]->TMAR = compareVal;
    } while (DMTIMER[id]->TMAR != compareVal);
    Timer_EnableIrq(id, DMTIMER_IRQ_MAT);
    return 0;
}

int Timer_SetOverflow(uint8_t id, uint32_t overflowVal)
{
    uint32_t val = 0xffffffff - overflowVal + 1;

    DMTIMER[id]->TCLR |= DMTIMER_TCLR_AR;
    do {
        DMTIMER[id]->TLDR = val;
    } while (DMTIMER[id]->TLDR != val);
    Timer_EnableIrq(id, DMTIMER_IRQ_OVF);
    Timer_Reload(id);
    return 0;
}

void Timer_Start(uint8_t id)
{
    DMTIMER[id]->TCLR |= DMTIMER_TCLR_ST;
}

void Timer_Stop(uint8_t id)
{
    DMTIMER[id]->TCLR &= ~DMTIMER_TCLR_ST;
}

void Timer_Reload(uint8_t id)
{
    DMTIMER[id]->TTGR |= 0x1;
}

void Timer_EnableIrq(uint8_t id, uint32_t irqFlags)
{
    DMTIMER[id]->IRQENABLE_SET |= irqFlags;
}

void Timer_DisableIrq(uint8_t id, uint32_t irqFlags)
{
    DMTIMER[id]->IRQENABLE_CLR |= irqFlags;
}

void Timer_ClearPendingIrq(uint8_t id, uint32_t irqFlags)
{
    DMTIMER[id]->IRQSTATUS |= irqFlags;
}

uint32_t Timer_GetTicks(uint8_t id)
{
    if (id != 1)
        return DMTIMER[id]->TCRR;
    else
        return DMTIMER1->TCRR;
}

int Timer_Init(uint8_t id)
{
    Timer_ModuleClkConfig(id);
    Timer_SwReset(id);
    return 0;
}

int Timer_SetFreq(uint8_t id, uint32_t freq)
{
    return Timer_SetOverflow(id, TIMER_CLK / freq);
}

#ifndef AM335X_EDMA3_H
#define AM335X_EDMA3_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct __attribute__((__packed__)) AM335X_EDMA3_PaRAM_t
{
    volatile uint32_t OPT;
    volatile uint32_t SRC;
    volatile uint16_t ACNT;
    volatile uint16_t BCNT;
    volatile uint32_t DST;
    volatile int16_t SRCBIDX;
    volatile int16_t DSTBIDX;
    volatile uint16_t LINK;
    volatile uint16_t BCNTRLD;
    volatile int16_t SRCCIDX;
    volatile int16_t DSTCIDX;
    volatile uint16_t CCNT;
    volatile uint16_t RSVD;
} AM335X_EDMA3_PaRAM_t;

typedef struct __attribute__((__packed__)) AM335X_EDMA3CC_t
{
    /*    0h */ volatile uint32_t PID           ; /* Peripheral Identification Register */
    /*    4h */ volatile uint32_t CCCFG         ; /* EDMA3CC Configuration Register */
                volatile uint32_t Reserved0[(0x10-0x04-sizeof(uint32_t))/4];
    /*   10h */ volatile uint32_t SYSCONFIG     ; /* EDMA3CC System Configuration Register */
                volatile uint32_t Reserved1[(0x100-0x10-sizeof(uint32_t))/4];
    /*  100h */ volatile uint32_t DCHMAP[64]    ; /* DMA Channel Mapping Registers 0-63 */
    /*  200h */ volatile uint32_t QCHMAP[8]     ; /* QDMA Channel Mapping Registers 0-7 */
                volatile uint32_t Reserved2[(0x240-0x200-8*sizeof(uint32_t))/4];
    /*  240h */ volatile uint32_t DMAQNUM[8]    ; /* DMA Queue Number Registers 0-7 */
    /*  260h */ volatile uint32_t QDMAQNUM      ; /* QDMA Queue Number Register */
                volatile uint32_t Reserved3[(0x284-0x260-sizeof(uint32_t))/4];
    /*  284h */ volatile uint32_t QUEPRI        ; /* Queue Priority Register */
                volatile uint32_t Reserved4[(0x300-0x284-sizeof(uint32_t))/4];
    /*  300h */ volatile uint64_t EMR           ; /* Event Missed Register */
    /*  308h */ volatile uint64_t EMCR          ; /* Event Missed Clear Register */
    /*  310h */ volatile uint32_t QEMR          ; /* QDMA Event Missed Register */
    /*  314h */ volatile uint32_t QEMCR         ; /* QDMA Event Missed Clear Register */
    /*  318h */ volatile uint32_t CCERR         ; /* EDMA3CC Error Register */
    /*  31Ch */ volatile uint32_t CCERRCLR      ; /* EDMA3CC Error Clear Register */
    /*  320h */ volatile uint32_t EEVAL         ; /* Error Evaluate Register */
                volatile uint32_t Reserved5[(0x340-0x320-sizeof(uint32_t))/4];
    /*  340h */ volatile uint64_t DRAE[8]       ; /* DMA Region Access Enable Register for Region 0-7 */
    /*  380h */ volatile uint32_t QRAE[8]       ; /* QDMA Region Access Enable Registers for Region 0-7 */
                volatile uint32_t Reserved6[(0x400-0x380-8*sizeof(uint32_t))/4];
    /*  400h */ volatile uint32_t QE[3][16]     ; /* Event Queue 0-2 Entry 0-15 Register */
                volatile uint32_t Reserved7[(0x600-0x480-16*sizeof(uint32_t))/4];
    /*  600h */ volatile uint32_t QSTAT[3]      ; /* Queue Status Registers 0-2 */
                volatile uint32_t Reserved8[(0x620-0x600-3*sizeof(uint32_t))/4];
    /*  620h */ volatile uint32_t QWMTHRA       ; /* Queue Watermark Threshold A Register */
                volatile uint32_t Reserved9[(0x640-0x620-sizeof(uint32_t))/4];
    /*  640h */ volatile uint32_t CCSTAT        ; /* EDMA3CC Status Register */
                volatile uint32_t Reserved10[(0x800-0x640-sizeof(uint32_t))/4];
    /*  800h */ volatile uint32_t MPFAR         ; /* Memory Protection Fault Address Register */
    /*  804h */ volatile uint32_t MPFSR         ; /* Memory Protection Fault Status Register */
    /*  808h */ volatile uint32_t MPFCR         ; /* Memory Protection Fault Command Register */
    /*  80Ch */ volatile uint32_t MPPAG         ; /* Memory Protection Page Attribute Register Global */
    /*  810h */ volatile uint32_t MPPA          ; /* Memory Protection Page Attribute Registers */
                volatile uint32_t Reserved11[(0x1000-0x810-sizeof(uint32_t))/4];
    /* 1000h */ volatile uint64_t ER            ; /* Event Register */
    /* 1008h */ volatile uint64_t ECR           ; /* Event Clear Register */
    /* 1010h */ volatile uint64_t ESR           ; /* Event Set Register */
    /* 1018h */ volatile uint64_t CER           ; /* Chained Event Register */
    /* 1020h */ volatile uint64_t EER           ; /* Event Enable Register */
    /* 1028h */ volatile uint64_t EECR          ; /* Event Enable Clear Register */
    /* 1030h */ volatile uint64_t EESR          ; /* Event Enable Set Register */
    /* 1038h */ volatile uint64_t SER           ; /* Secondary Event Register */
    /* 1040h */ volatile uint64_t SECR          ; /* Secondary Event Clear Register */
                volatile uint32_t Reserved12[2];
    /* 1050h */ volatile uint64_t IER           ; /* Interrupt Enable Register */
    /* 1058h */ volatile uint64_t IECR          ; /* Interrupt Enable Clear Register */
    /* 1060h */ volatile uint64_t IESR          ; /* Interrupt Enable Set Register */
    /* 1068h */ volatile uint64_t IPR           ; /* Interrupt Pending Register */
    /* 1070h */ volatile uint64_t ICR           ; /* Interrupt Clear Register */
    /* 1078h */ volatile uint32_t IEVAL         ; /* Interrupt Evaluate Register */
                volatile uint32_t Reserved13[1];
    /* 1080h */ volatile uint32_t QER           ; /* QDMA Event Register */
    /* 1084h */ volatile uint32_t QEER          ; /* QDMA Event Enable Register */
    /* 1088h */ volatile uint32_t QEECR         ; /* QDMA Event Enable Clear Register */
    /* 108Ch */ volatile uint32_t QEESR         ; /* QDMA Event Enable Set Register */
    /* 1090h */ volatile uint32_t QSER          ; /* QDMA Secondary Event Register */
    /* 1094h */ volatile uint32_t QSECR         ; /* QDMA Secondary Event Clear Register */
} AM335X_EDMA3CC_t;

#define EDMA3CC_BASE    0x49000000
#define EDMA3PaRAM_BASE 0x49004000
#define EDMA3TC0_BASE   0x49800000
#define EDMA3TC1_BASE   0x49900000
#define EDMA3TC2_BASE   0x49A00000

#ifndef __KERNEL__
static AM335X_EDMA3CC_t*     const EDMA3CC       = (AM335X_EDMA3CC_t*)EDMA3CC_BASE;
// static AM335X_EDMA3TC_t*     const EDMA3TC[3]    =
// {
//     (AM335X_EDMA3CC_t*) EDMA3TC0_BASE,
//     (AM335X_EDMA3CC_t*) EDMA3TC1_BASE,
//     (AM335X_EDMA3CC_t*) EDMA3TC2_BASE,
// };
static AM335X_EDMA3_PaRAM_t* const EDMA3PaRAM = (AM335X_EDMA3_PaRAM_t*)EDMA3PaRAM_BASE;
#else
static AM335X_EDMA3CC_t* EDMA3CC;
//static AM335X_EDMA3TC_t* EDMA3TC[3];
static AM335X_EDMA3_PaRAM_t* EDMA3PaRAM;
#endif

#define EDMA3CC_CHMAP_PAENTRY (0x1FF << 5)
#define EDMA3PaRAM_OPT_SAM (1 << 0)
#define EDMA3PaRAM_OPT_DAM (1 << 1)
#define EDMA3PaRAM_OPT_SYNCDIM (1 << 2)
#define EDMA3PaRAM_OPT_STATIC (1 << 3)
#define EDMA3PaRAM_OPT_TCC (0x3F << 12)
#define EDMA3PaRAM_OPT_TCCHEN (1 << 22)
#define EDMA3PaRAM_OPT_ITCCHEN (1 << 23)

#define EDMA_Event_TSC_ADC_FIFO0 53

#endif

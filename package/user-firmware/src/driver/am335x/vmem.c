#ifndef __PRU__

#include "logging.h"
#include "soc/vmem.h"

#ifndef __KERNEL__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

#define PERIPH_BASE   0x40000000
#define SRAM_BASE     0x80000000
#define MMAP_1GB      0x40000000
#define MMAP_512MB    0x20000000

int vmem_init (void)
{
    int devmem = -1;
    if ((devmem = open("/dev/mem", O_RDWR | O_SYNC)) < 0)
    {
        lprintf("Failed to open /dev/mem: %s\r\n", strerror(errno));
        exit(-1);
    }
    lprintf("vmem_init\n");
    uint32_t periph_addr = (uint32_t) mmap((void*)PERIPH_BASE, MMAP_1GB, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, devmem, PERIPH_BASE);
    lprintf("    0x%08x -> 0x%08x\n", PERIPH_BASE, periph_addr);
    uint32_t sram_addr = (uint32_t) mmap((void*)SRAM_BASE, MMAP_512MB, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, devmem, SRAM_BASE);
    lprintf("    0x%08x -> 0x%08x\n", SRAM_BASE, sram_addr);
    close(devmem);
    return 0;
}

int vmem_deinit (void)
{
    munmap((void*)PERIPH_BASE, MMAP_1GB);
    munmap((void*)SRAM_BASE, MMAP_512MB);
    return 0;
}

#else

#include <linux/types.h>
#include <asm/io.h>

void* vmem_mmap (unsigned long phys_addr, unsigned long size)
{
    void* virt_addr = ioremap_nocache(phys_addr, size);
    lprintf("vmem_mmap "
    #ifdef __KERNEL__
    "0x%08lx -> 0x%08lx"
    #else
    "0x%08x -> 0x%08x"
    #endif
    "\n", phys_addr, (unsigned long)virt_addr);
    return virt_addr;
}

int vmem_unmap (void* addr)
{
    iounmap((void*)addr);
    return 0;
}

#endif // #ifndef __KERNEL__

#endif // #ifndef __PRU__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/i2c.h"
#include "am335x/cm_per.h"
#include "am335x/cm_wkup.h"
#include "am335x/control_module.h"
#include "am335x/gpio.h"
#include "am335x/i2c.h"
#include "am335x/pinmux.h"
#include "soc/timing.h"
#include "cxz.h"

int I2C_ModuleClkConfig (const I2C_t* i2c)
{
    switch (i2c->I2Cx)
    {
        case I2C0:
            CM_WKUP->WKUP_I2C0_CLKCTRL |= CM_CLKCTRL_ENABLE;
            while (CM_WKUP->WKUP_I2C0_CLKCTRL & CM_CLKCTRL_IDLEST) ;
        break;
        case I2C1:
            CM_PER->I2C1_CLKCTRL |= CM_CLKCTRL_ENABLE;
            while (CM_PER->I2C1_CLKCTRL & CM_CLKCTRL_IDLEST) ;
        break;
        case I2C2:
            CM_PER->I2C2_CLKCTRL |= CM_CLKCTRL_ENABLE;
            while (CM_PER->I2C2_CLKCTRL & CM_CLKCTRL_IDLEST) ;
        break;
    }
    return 0;
}

int I2C_Reset (const I2C_t* i2c)
{
    int x = i2c->I2Cx;
    I2C[x]->SYSC |= I2C_SYSC_SRST; // software reset
    return 0;
}

/* FIXME: this actually has no effect, since CONTROL_MODULE registers can
          only be written in previledged mode (i.e. in kernel mode) */
int I2C_PinMuxSetUp (const I2C_t* i2c)
{
    int x = i2c->I2Cx;
    uint8_t mode_scl = 0, mode_sda = 0;
    volatile uint32_t *conf_scl = 0, *conf_sda = 0;

    switch (x)
    {
        case I2C0:
        {
            switch (i2c->SCL)
            {
                case I2C0_SCL:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_I2C0_SCL;
                    mode_scl = 0;
                break;
            }
            switch (i2c->SDA)
            {
                case I2C0_SDA:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_I2C0_SDA;
                    mode_sda = 0;
                break;

            }
        }
        break;

        case I2C1:
        {
            switch (i2c->SCL)
            {
                case MII1_RX_ER:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_RX_ER;
                    mode_scl = 3;
                break;
                case SPI0_CS0:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_CS0;
                    mode_scl = 2;
                break;
                case UART0_RTSN:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RTSN;
                    mode_scl = 3;
                break;
                case UART1_TXD:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_TXD;
                    mode_scl = 3;
                break;
            }
            switch (i2c->SDA)
            {
                case MII1_CRS:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_CRS;
                    mode_sda = 3;
                break;
                case SPI0_D1:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D1;
                    mode_sda = 2;
                break;
                case UART0_CTSN:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_CTSN;
                    mode_sda = 3;
                break;
                case UART1_RXD:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_RXD;
                    mode_sda = 3;
                break;
            }
        }
        break;

        case I2C2:
        {
            switch (i2c->SCL)
            {
                case SPI0_D0:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D0;
                    mode_scl = 2;
                break;
                case UART0_TXD:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_TXD;
                    mode_scl = 3;
                break;
                case UART1_RTSN:
                    conf_scl = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_RTSN;
                    mode_scl = 3;
                break;
            }
            switch (i2c->SDA)
            {
                case SPI0_SCLK:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_SCLK;
                    mode_sda = 2;
                break;
                case UART0_RXD:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RXD;
                    mode_sda = 3;
                break;
                case UART1_CTSN:
                    conf_sda = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_CTSN;
                    mode_sda = 3;
                break;
            }
        }
        break;
    }

    if (!(conf_scl && conf_sda))
    {
        lprintf("I2C_PinMuxSetUp error!\n");
        return -1;
    }

    *conf_scl =
        // slow slew
        CONTROL_MODULE_CONF_rxactive
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden  // pullup
        | mode_scl;
    *conf_sda =
        // slow slew
        CONTROL_MODULE_CONF_rxactive
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden  // pullup
        | mode_sda;

    return 0;
}

int I2C_Init (const I2C_t* i2c)
{
    I2C_ModuleClkConfig(i2c);
    I2C_PinMuxSetUp(i2c);
    I2C_Reset(i2c);
    int x = i2c->I2Cx;

    uint32_t sysClk         = 48000000;
    uint32_t internalClk    = 12000000; // 12 MHz I2C module clock
    uint32_t outputClk      = 400000; // 400 kHz I2C clock

    I2C[x]->PSC = (sysClk / internalClk) - 1;

    uint32_t divider = internalClk / outputClk / 2;
    I2C[x]->SCLL = divider - 7;
    I2C[x]->SCLH = divider - 5;

    I2C[x]->SYSC &= ~I2C_SYSC_AUTOIDLE; // Disable autoidle
    I2C[x]->CON |= I2C_CON_MST; // I2C master mode
    I2C[x]->CON |= I2C_CON_EN; // I2C module enable

    uint32_t timeout_counter = 0;
    while (!I2C[x]->SYSS) { if (timeout_counter++ > 1000) return -1; }; // wait for reset to be done

    return 0;
}

int I2C_TransmitWithHeader (const I2C_t* i2c, uint32_t addr, int header_len, uint8_t* header, int len, uint8_t* data)
{
    uint32_t timeout_counter = 0;

    int x = i2c->I2Cx;
    while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB) { if (timeout_counter++ > 1000) return -1; }; // wait for bus not busy
    I2C[x]->CON |= I2C_CON_MST; // I2C master mode
    I2C[x]->CON |= I2C_CON_TRX; // Transmitter mode
    I2C[x]->SA = addr; // slave address
    I2C[x]->CNT = header_len + len; // number of bytes
    I2C[x]->BUF |= I2C_BUF_TXFIFO_CLR ; // clear TX FIFO
    I2C[x]->BUF &= ~I2C_BUF_TXTRSH;
    I2C[x]->BUF |= 8 << ctz(I2C_BUF_TXTRSH);
    I2C[x]->CON |= I2C_CON_STT; // Start condition
    timeout_counter = 0;
    while (!(I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB)) { if (timeout_counter++ > 1000) return -1; };
    while (header_len)
    {
        timeout_counter = 0;
        while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_XRDY) { if (timeout_counter++ > 1000) return -1; };
        I2C[x]->DATA = *header++;
        header_len--;
    }
    while (len)
    {
        timeout_counter = 0;
        while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_XRDY) { if (timeout_counter++ > 1000) return -1; };
        I2C[x]->DATA = *data++;
        len--;
    }
    I2C[x]->CON |= I2C_CON_STP; // Stop condition
    timeout_counter = 0;
    while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB) { if (timeout_counter++ > 1000) return -1; };  // wait for bus not busy
    return 0;
}

int I2C_Receive (const I2C_t* i2c, uint32_t addr, int len, uint8_t* buffer)
{
    uint32_t timeout_counter = 0;

    int x = i2c->I2Cx;
    while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB) { if (timeout_counter++ > 1000) return -1; }; // wait for bus not busy

    I2C[x]->CON |= I2C_CON_MST; // I2C master mode
    I2C[x]->CON &= ~I2C_CON_TRX; // Receiver mode
    I2C[x]->SA = addr; // slave address
    I2C[x]->CNT = len; // number of bytes
    I2C[x]->BUF |= I2C_BUF_RXFIFO_CLR;
    I2C[x]->BUF &= ~I2C_BUF_RXTRSH;
    I2C[x]->BUF |= 1 << ctz(I2C_BUF_RXTRSH);
    I2C[x]->CON |= I2C_CON_STT; // Start condition
    timeout_counter = 0;
    while (!(I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB)) { if (timeout_counter++ > 1000) return -1; };

    timeout_counter = 0;
    while (len)
    {
        uint8_t rxstat = (I2C[x]->BUFSTAT & I2C_BUFSTAT_RXSTAT) >> ctz(I2C_BUFSTAT_RXSTAT);
        while (rxstat--)
        {
            *buffer++ = I2C[x]->DATA;
            len--;
        }
        if (timeout_counter++ > 1000) return -1;
    }
    I2C[x]->CON |= I2C_CON_STP; // Stop condition
    timeout_counter = 0;
    while (I2C[x]->IRQSTATUS_RAW & I2C_IRQ_BB) { if (timeout_counter++ > 1000) return -1; }; // wait for bus not busy

    return 0;
}

int I2C_Transmit (const I2C_t* i2c, uint32_t addr, int len, uint8_t* data)
{
    return I2C_TransmitWithHeader(i2c, addr, 0, 0, len, data);
}

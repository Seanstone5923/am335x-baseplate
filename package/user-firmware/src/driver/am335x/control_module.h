#ifndef AM335X_CONTROL_MODULE_H
#define AM335X_CONTROL_MODULE_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_CONTROL_MODULE_t
{
    /*    0 */   volatile uint32_t CONTROL_REVISION;
    /*    4 */   volatile uint32_t CONTROL_HWINFO;
                 volatile uint32_t reserved0[2];
    /*   10 */   volatile uint32_t CONTROL_SYSCONFIG;
                 volatile uint32_t reserved1[11];
    /*   40 */   volatile uint32_t CONTROL_STATUS;
                 volatile uint32_t reserved2[51];
    /*  110 */   volatile uint32_t CONTROL_EMIF_SDRAM_CONFIG;
                 volatile uint32_t reserved3[197];
    /*  428 */   volatile uint32_t CORE_SLDO_CTRL;
    /*  42C */   volatile uint32_t MPU_SLDO_CTRL;
                 volatile uint32_t reserved4[5];
    /*  444 */   volatile uint32_t CLK32KDIVRATIO_CTRL;
    /*  448 */   volatile uint32_t BANDGAP_CTRL;
    /*  44C */   volatile uint32_t BANDGAP_TRIM;
                 volatile uint32_t reserved5[2];
    /*  458 */   volatile uint32_t PLL_CLKINPULOW_CTRL;
                 volatile uint32_t reserved6[3];
    /*  468 */   volatile uint32_t MOSC_CTRL;
                 volatile uint32_t reserved7[1];
    /*  470 */   volatile uint32_t DEEPSLEEP_CTRL;
                 volatile uint32_t reserved8[38];
    /*  50C */   volatile uint32_t DPLL_PWR_SW_STATUS;
                 volatile uint32_t reserved9[60];
    /*  600 */   volatile uint32_t DEVICE_ID;
    /*  604 */   volatile uint32_t DEV_FEATURE;
    /*  608 */   volatile uint32_t INIT_PRIORITY_0;
    /*  60C */   volatile uint32_t INIT_PRIORITY_1;
                 volatile uint32_t reserved10[1];
    /*  614 */   volatile uint32_t TPTC_CFG;
                 volatile uint32_t reserved11[2];
    /*  620 */   volatile uint32_t USB_CTRL0;
    /*  624 */   volatile uint32_t USB_STS0;
    /*  628 */   volatile uint32_t USB_CTRL1;
    /*  62C */   volatile uint32_t USB_STS1;
    /*  630 */   volatile uint32_t MAC_ID0_LO;
    /*  634 */   volatile uint32_t MAC_ID0_HI;
    /*  638 */   volatile uint32_t MAC_ID1_LO;
    /*  63C */   volatile uint32_t MAC_ID1_HI;
                 volatile uint32_t reserved12[1];
    /*  644 */   volatile uint32_t DCAN_RAMINIT;
    /*  648 */   volatile uint32_t USB_WKUP_CTRL;
                 volatile uint32_t reserved13[1];
    /*  650 */   volatile uint32_t GMII_SEL;
                 volatile uint32_t reserved14[4];
    /*  664 */   volatile uint32_t PWMSS_CTRL;
                 volatile uint32_t reserved15[2];
    /*  670 */   volatile uint32_t MREQPRIO_0;
    /*  674 */   volatile uint32_t MREQPRIO_1;
                 volatile uint32_t reserved16[6];
    /*  690 */   volatile uint32_t HW_EVENT_SEL_GRP1;
    /*  694 */   volatile uint32_t HW_EVENT_SEL_GRP2;
    /*  698 */   volatile uint32_t HW_EVENT_SEL_GRP3;
    /*  69C */   volatile uint32_t HW_EVENT_SEL_GRP4;
    /*  6A0 */   volatile uint32_t SMRT_CTRL;
    /*  6A4 */   volatile uint32_t MPUSS_HW_DEBUG_SEL;
    /*  6A8 */   volatile uint32_t MPUSS_HW_DBG_INFO;
                 volatile uint32_t reserved17[49];
    /*  770 */   volatile uint32_t VDD_MPU_OPP_050;
    /*  774 */   volatile uint32_t VDD_MPU_OPP_100;
    /*  778 */   volatile uint32_t VDD_MPU_OPP_120;
    /*  77C */   volatile uint32_t VDD_MPU_OPP_TURBO;
                 volatile uint32_t reserved18[14];
    /*  7B8 */   volatile uint32_t VDD_CORE_OPP_050;
    /*  7BC */   volatile uint32_t VDD_CORE_OPP_100;
                 volatile uint32_t reserved19[4];
    /*  7D0 */   volatile uint32_t BB_SCALE;
                 volatile uint32_t reserved20[8];
    /*  7F4 */   volatile uint32_t USB_VID_PID;
                 volatile uint32_t reserved21[1];
    /*  7FC */   volatile uint32_t EFUSE_SMA;
    /*  800 */   volatile uint32_t CONF_GPMC_AD0;
    /*  804 */   volatile uint32_t CONF_GPMC_AD1;
    /*  808 */   volatile uint32_t CONF_GPMC_AD2;
    /*  80C */   volatile uint32_t CONF_GPMC_AD3;
    /*  810 */   volatile uint32_t CONF_GPMC_AD4;
    /*  814 */   volatile uint32_t CONF_GPMC_AD5;
    /*  818 */   volatile uint32_t CONF_GPMC_AD6;
    /*  81C */   volatile uint32_t CONF_GPMC_AD7;
    /*  820 */   volatile uint32_t CONF_GPMC_AD8;
    /*  824 */   volatile uint32_t CONF_GPMC_AD9;
    /*  828 */   volatile uint32_t CONF_GPMC_AD10;
    /*  82C */   volatile uint32_t CONF_GPMC_AD11;
    /*  830 */   volatile uint32_t CONF_GPMC_AD12;
    /*  834 */   volatile uint32_t CONF_GPMC_AD13;
    /*  838 */   volatile uint32_t CONF_GPMC_AD14;
    /*  83C */   volatile uint32_t CONF_GPMC_AD15;
    /*  840 */   volatile uint32_t CONF_GPMC_A0;
    /*  844 */   volatile uint32_t CONF_GPMC_A1;
    /*  848 */   volatile uint32_t CONF_GPMC_A2;
    /*  84C */   volatile uint32_t CONF_GPMC_A3;
    /*  850 */   volatile uint32_t CONF_GPMC_A4;
    /*  854 */   volatile uint32_t CONF_GPMC_A5;
    /*  858 */   volatile uint32_t CONF_GPMC_A6;
    /*  85C */   volatile uint32_t CONF_GPMC_A7;
    /*  860 */   volatile uint32_t CONF_GPMC_A8;
    /*  864 */   volatile uint32_t CONF_GPMC_A9;
    /*  868 */   volatile uint32_t CONF_GPMC_A10;
    /*  86C */   volatile uint32_t CONF_GPMC_A11;
    /*  870 */   volatile uint32_t CONF_GPMC_WAIT0;
    /*  874 */   volatile uint32_t CONF_GPMC_WPN;
    /*  878 */   volatile uint32_t CONF_GPMC_BEN1;
    /*  87C */   volatile uint32_t CONF_GPMC_CSN0;
    /*  880 */   volatile uint32_t CONF_GPMC_CSN1;
    /*  884 */   volatile uint32_t CONF_GPMC_CSN2;
    /*  888 */   volatile uint32_t CONF_GPMC_CSN3;
    /*  88C */   volatile uint32_t CONF_GPMC_CLK;
    /*  890 */   volatile uint32_t CONF_GPMC_ADVN_ALE;
    /*  894 */   volatile uint32_t CONF_GPMC_OEN_REN;
    /*  898 */   volatile uint32_t CONF_GPMC_WEN;
    /*  89C */   volatile uint32_t CONF_GPMC_BEN0_CLE;
    /*  8A0 */   volatile uint32_t CONF_LCD_DATA0;
    /*  8A4 */   volatile uint32_t CONF_LCD_DATA1;
    /*  8A8 */   volatile uint32_t CONF_LCD_DATA2;
    /*  8AC */   volatile uint32_t CONF_LCD_DATA3;
    /*  8B0 */   volatile uint32_t CONF_LCD_DATA4;
    /*  8B4 */   volatile uint32_t CONF_LCD_DATA5;
    /*  8B8 */   volatile uint32_t CONF_LCD_DATA6;
    /*  8BC */   volatile uint32_t CONF_LCD_DATA7;
    /*  8C0 */   volatile uint32_t CONF_LCD_DATA8;
    /*  8C4 */   volatile uint32_t CONF_LCD_DATA9;
    /*  8C8 */   volatile uint32_t CONF_LCD_DATA10;
    /*  8CC */   volatile uint32_t CONF_LCD_DATA11;
    /*  8D0 */   volatile uint32_t CONF_LCD_DATA12;
    /*  8D4 */   volatile uint32_t CONF_LCD_DATA13;
    /*  8D8 */   volatile uint32_t CONF_LCD_DATA14;
    /*  8DC */   volatile uint32_t CONF_LCD_DATA15;
    /*  8E0 */   volatile uint32_t CONF_LCD_VSYNC;
    /*  8E4 */   volatile uint32_t CONF_LCD_HSYNC;
    /*  8E8 */   volatile uint32_t CONF_LCD_PCLK;
    /*  8EC */   volatile uint32_t CONF_LCD_AC_BIAS_EN;
    /*  8F0 */   volatile uint32_t CONF_MMC0_DAT3;
    /*  8F4 */   volatile uint32_t CONF_MMC0_DAT2;
    /*  8F8 */   volatile uint32_t CONF_MMC0_DAT1;
    /*  8FC */   volatile uint32_t CONF_MMC0_DAT0;
    /*  900 */   volatile uint32_t CONF_MMC0_CLK;
    /*  904 */   volatile uint32_t CONF_MMC0_CMD;
    /*  908 */   volatile uint32_t CONF_MII1_COL;
    /*  90C */   volatile uint32_t CONF_MII1_CRS;
    /*  910 */   volatile uint32_t CONF_MII1_RX_ER;
    /*  914 */   volatile uint32_t CONF_MII1_TX_EN;
    /*  918 */   volatile uint32_t CONF_MII1_RX_DV;
    /*  91C */   volatile uint32_t CONF_MII1_TXD3;
    /*  920 */   volatile uint32_t CONF_MII1_TXD2;
    /*  924 */   volatile uint32_t CONF_MII1_TXD1;
    /*  928 */   volatile uint32_t CONF_MII1_TXD0;
    /*  92C */   volatile uint32_t CONF_MII1_TX_CLK;
    /*  930 */   volatile uint32_t CONF_MII1_RX_CLK;
    /*  934 */   volatile uint32_t CONF_MII1_RXD3;
    /*  938 */   volatile uint32_t CONF_MII1_RXD2;
    /*  93C */   volatile uint32_t CONF_MII1_RXD1;
    /*  940 */   volatile uint32_t CONF_MII1_RXD0;
    /*  944 */   volatile uint32_t CONF_RMII1_REF_CLK;
    /*  948 */   volatile uint32_t CONF_MDIO;
    /*  94C */   volatile uint32_t CONF_MDC;
    /*  950 */   volatile uint32_t CONF_SPI0_SCLK;
    /*  954 */   volatile uint32_t CONF_SPI0_D0;
    /*  958 */   volatile uint32_t CONF_SPI0_D1;
    /*  95C */   volatile uint32_t CONF_SPI0_CS0;
    /*  960 */   volatile uint32_t CONF_SPI0_CS1;
    /*  964 */   volatile uint32_t CONF_ECAP0_IN_PWM0_OUT;
    /*  968 */   volatile uint32_t CONF_UART0_CTSN;
    /*  96C */   volatile uint32_t CONF_UART0_RTSN;
    /*  970 */   volatile uint32_t CONF_UART0_RXD;
    /*  974 */   volatile uint32_t CONF_UART0_TXD;
    /*  978 */   volatile uint32_t CONF_UART1_CTSN;
    /*  97C */   volatile uint32_t CONF_UART1_RTSN;
    /*  980 */   volatile uint32_t CONF_UART1_RXD;
    /*  984 */   volatile uint32_t CONF_UART1_TXD;
    /*  988 */   volatile uint32_t CONF_I2C0_SDA;
    /*  98C */   volatile uint32_t CONF_I2C0_SCL;
    /*  990 */   volatile uint32_t CONF_MCASP0_ACLKX;
    /*  994 */   volatile uint32_t CONF_MCASP0_FSX;
    /*  998 */   volatile uint32_t CONF_MCASP0_AXR0;
    /*  99C */   volatile uint32_t CONF_MCASP0_AHCLKR;
    /*  9A0 */   volatile uint32_t CONF_MCASP0_ACLKR;
    /*  9A4 */   volatile uint32_t CONF_MCASP0_FSR;
    /*  9A8 */   volatile uint32_t CONF_MCASP0_AXR1;
    /*  9AC */   volatile uint32_t CONF_MCASP0_AHCLKX;
    /*  9B0 */   volatile uint32_t CONF_XDMA_EVENT_INTR0;
    /*  9B4 */   volatile uint32_t CONF_XDMA_EVENT_INTR1;
    /*  9B8 */   volatile uint32_t CONF_WARMRSTN;
                 volatile uint32_t reserved22[1];
    /*  9C0 */   volatile uint32_t CONF_NNMI;
                 volatile uint32_t reserved23[3];
    /*  9D0 */   volatile uint32_t CONF_TMS;
    /*  9D4 */   volatile uint32_t CONF_TDI;
    /*  9D8 */   volatile uint32_t CONF_TDO;
    /*  9DC */   volatile uint32_t CONF_TCK;
    /*  9E0 */   volatile uint32_t CONF_TRSTN;
    /*  9E4 */   volatile uint32_t CONF_EMU0;
    /*  9E8 */   volatile uint32_t CONF_EMU1;
                 volatile uint32_t reserved24[3];
    /*  9F8 */   volatile uint32_t CONF_RTC_PWRONRSTN;
    /*  9FC */   volatile uint32_t CONF_PMIC_POWER_EN;
    /*  A00 */   volatile uint32_t CONF_EXT_WAKEUP;
                 volatile uint32_t reserved25[6];
    /*  A1C */   volatile uint32_t CONF_USB0_DRVVBUS;
                 volatile uint32_t reserved26[5];
    /*  A34 */   volatile uint32_t CONF_USB1_DRVVBUS;
                 volatile uint32_t reserved27[242];
    /*  E00 */   volatile uint32_t CQDETECT_STATUS;
    /*  E04 */   volatile uint32_t DDR_IO_CTRL;
                 volatile uint32_t reserved28[1];
    /*  E0C */   volatile uint32_t VTP_CTRL;
                 volatile uint32_t reserved29[1];
    /*  E14 */   volatile uint32_t VREF_CTRL;
                 volatile uint32_t reserved30[94];
    /*  F90 */   volatile uint32_t TPCC_EVT_MUX_0_3;
    /*  F94 */   volatile uint32_t TPCC_EVT_MUX_4_7;
    /*  F98 */   volatile uint32_t TPCC_EVT_MUX_8_11;
    /*  F9C */   volatile uint32_t TPCC_EVT_MUX_12_15;
    /*  FA0 */   volatile uint32_t TPCC_EVT_MUX_16_19;
    /*  FA4 */   volatile uint32_t TPCC_EVT_MUX_20_23;
    /*  FA8 */   volatile uint32_t TPCC_EVT_MUX_24_27;
    /*  FAC */   volatile uint32_t TPCC_EVT_MUX_28_31;
    /*  FB0 */   volatile uint32_t TPCC_EVT_MUX_32_35;
    /*  FB4 */   volatile uint32_t TPCC_EVT_MUX_36_39;
    /*  FB8 */   volatile uint32_t TPCC_EVT_MUX_40_43;
    /*  FBC */   volatile uint32_t TPCC_EVT_MUX_44_47;
    /*  FC0 */   volatile uint32_t TPCC_EVT_MUX_48_51;
    /*  FC4 */   volatile uint32_t TPCC_EVT_MUX_52_55;
    /*  FC8 */   volatile uint32_t TPCC_EVT_MUX_56_59;
    /*  FCC */   volatile uint32_t TPCC_EVT_MUX_60_63;
    /*  FD0 */   volatile uint32_t TIMER_EVT_CAPT;
    /*  FD4 */   volatile uint32_t ECAP_EVT_CAPT;
    /*  FD8 */   volatile uint32_t ADC_EVT_CAPT;
                 volatile uint32_t reserved31[9];
    /* 1000 */   volatile uint32_t RESET_ISO;
                 volatile uint32_t reserved32[197];
    /* 1318 */   volatile uint32_t DPLL_PWR_SW_CTRL;
    /* 131C */   volatile uint32_t DDR_CKE_CTRL;
    /* 1320 */   volatile uint32_t SMA2;
    /* 1324 */   volatile uint32_t M3_TXEV_EOI;
    /* 1328 */   volatile uint32_t IPC_MSG_REG0;
    /* 132C */   volatile uint32_t IPC_MSG_REG1;
    /* 1330 */   volatile uint32_t IPC_MSG_REG2;
    /* 1334 */   volatile uint32_t IPC_MSG_REG3;
    /* 1338 */   volatile uint32_t IPC_MSG_REG4;
    /* 133C */   volatile uint32_t IPC_MSG_REG5;
    /* 1340 */   volatile uint32_t IPC_MSG_REG6;
    /* 1344 */   volatile uint32_t IPC_MSG_REG7;
                 volatile uint32_t reserved33[47];
    /* 1404 */   volatile uint32_t DDR_CMD0_IOCTRL;
    /* 1408 */   volatile uint32_t DDR_CMD1_IOCTRL;
    /* 140C */   volatile uint32_t DDR_CMD2_IOCTRL;
                 volatile uint32_t reserved34[12];
    /* 1440 */   volatile uint32_t DDR_DATA0_IOCTRL;
    /* 1444 */   volatile uint32_t DDR_DATA1_IOCTRL;
} AM335X_CONTROL_MODULE_t;

#define CONTROL_MODULE_BASE 0x44E10000

#ifndef __KERNEL__
static AM335X_CONTROL_MODULE_t* const CONTROL_MODULE = (AM335X_CONTROL_MODULE_t*)CONTROL_MODULE_BASE;
#else
static AM335X_CONTROL_MODULE_t* CONTROL_MODULE;
#endif

#define CONTROL_MODULE_DCAN_RAMINIT_dcan0_raminit_start (1 << 0)
#define CONTROL_MODULE_DCAN_RAMINIT_dcan1_raminit_start (1 << 1)
#define CONTROL_MODULE_DCAN_RAMINIT_dcan0_raminit_done (1 << 8)
#define CONTROL_MODULE_DCAN_RAMINIT_dcan1_raminit_done (1 << 9)

#define CONTROL_MODULE_CONF_slewctrl    (1 << 6)
#define CONTROL_MODULE_CONF_rxactive    (1 << 5)
#define CONTROL_MODULE_CONF_putypesel   (1 << 4)
#define CONTROL_MODULE_CONF_puden       (1 << 3)
#define CONTROL_MODULE_CONF_mmode       (0b111 << 0)

#endif

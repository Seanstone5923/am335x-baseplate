#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/spi.h"
#include "soc/gpio.h"
#include "soc/timing.h"
#include "am335x/cm_per.h"
#include "am335x/mcspi.h"
#include "cxz.h"
#include "am335x/pinmux.h"
#include "am335x/control_module.h"

int SPI_ModuleClkConfig (const SPI_t* spi)
{
    CM_PER->L3S_CLKSTCTRL       |= CM_CLKCTRL_ENABLE;
    CM_PER->L3_CLKSTCTRL        |= CM_CLKCTRL_ENABLE;
    CM_PER->L3_INSTR_CLKCTRL    |= CM_CLKCTRL_ENABLE;
    CM_PER->L3_CLKCTRL          |= CM_CLKCTRL_ENABLE;
    CM_PER->OCPWP_L3_CLKSTCTRL  |= CM_CLKCTRL_ENABLE;
    CM_PER->L4LS_CLKSTCTRL      |= CM_CLKCTRL_ENABLE;
    CM_PER->L4LS_CLKCTRL        |= CM_CLKCTRL_ENABLE;

    switch (spi->SPIx)
    {
        case 0:
        CM_PER->SPI0_CLKCTRL        |= CM_CLKCTRL_ENABLE;
        while (CM_PER->SPI0_CLKCTRL & CM_CLKCTRL_IDLEST) ;
        break;
        case 1:
        CM_PER->SPI1_CLKCTRL        |= CM_CLKCTRL_ENABLE;
        while (CM_PER->SPI1_CLKCTRL & CM_CLKCTRL_IDLEST) ;
        break;
    }

    int x = spi->SPIx;

    MCSPI[x]->SYSCONFIG   |= MCSPI_SYSCONFIG_SOFTRESET;   // reset MCSPIx
    while (!MCSPI[x]->SYSSTATUS) ;                        // wait for SPIx to reset

    MCSPI[x]->SYSCONFIG   |= MCSPI_SYSCONFIG_CLOCKACTIVITY;
    MCSPI[x]->SYSCONFIG   &= ~MCSPI_SYSCONFIG_SIDLEMODE;
    MCSPI[x]->SYSCONFIG   |= 0b1 << ctz(MCSPI_SYSCONFIG_SIDLEMODE);
    MCSPI[x]->SYSCONFIG   &= ~MCSPI_SYSCONFIG_AUTOIDLE;

    return 0;
}

/* FIXME: this actually has no effect, since CONTROL_MODULE registers can
          only be written in previledged mode (i.e. in kernel mode) */
int SPI_PinMuxSetUp (const SPI_t* spi)
{
    int x = spi->SPIx;
    uint8_t mode_sclk = 0, mode_miso = 0, mode_mosi = 0, mode_cs = 0;
    volatile uint32_t *conf_sclk = 0, *conf_miso = 0, *conf_mosi = 0, *conf_cs = 0;
    bool d0_output = false, d1_output = false;

    switch (x)
    {
        case SPI0:
        {
            switch (spi->SCLK)
            {
                case SPI0_SCLK:
                    conf_sclk = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_SCLK;
                    mode_sclk = 0;
                break;
            }
            switch (spi->MISO)
            {
                case SPI0_D0:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D0;
                    mode_miso = 0;
                    d0_output = false;
                break;
                case SPI0_D1:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D1;
                    mode_miso = 0;
                    d1_output = false;
                break;
            }
            switch (spi->MOSI)
            {
                case SPI0_D0:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D0;
                    mode_mosi = 0;
                    d0_output = true;
                break;
                case SPI0_D1:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_D1;
                    mode_mosi = 0;
                    d1_output = true;
                break;
            }
            switch (spi->CS)
            {
                case SPI0_CS0:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_CS0;
                    mode_cs = 0;
                break;
                case SPI0_CS1:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_SPI0_CS1;
                    mode_cs = 0;
                break;
            }
        }
        break;

        case SPI1:
        {
            switch (spi->SCLK)
            {
                case ECAP0_IN_PWM0_OUT:
                    conf_sclk = (volatile uint32_t*)&CONTROL_MODULE->CONF_ECAP0_IN_PWM0_OUT;
                    mode_sclk = 4;
                break;
                case MCASP0_ACLKX:
                    conf_sclk = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_ACLKX;
                    mode_sclk = 3;
                break;
                case MII1_COL:
                    conf_sclk = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_COL;
                    mode_sclk = 2;
                break;
            }
            switch (spi->MISO)
            {
                /* spi1_d0 */
                case MCASP0_FSX:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_FSX;
                    mode_miso = 3;
                    d0_output = false;
                break;
                case MII1_CRS:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_CRS;
                    mode_miso = 2;
                    d0_output = false;
                break;
                case UART0_CTSN:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_CTSN;
                    mode_miso = 4;
                    d0_output = false;
                break;
                /* spi1_d1 */
                case MCASP0_AXR0:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_AXR0;
                    mode_miso = 3;
                    d1_output = false;
                break;
                case MII1_RX_ER:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_RX_ER;
                    mode_miso = 2;
                    d1_output = false;
                break;
                case UART0_RTSN:
                    conf_miso = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RTSN;
                    mode_miso = 4;
                    d1_output = false;
                break;
            }
            switch (spi->MOSI)
            {
                /* spi1_d0 */
                case MCASP0_FSX:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_FSX;
                    mode_mosi = 3;
                    d0_output = true;
                break;
                case MII1_CRS:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_CRS;
                    mode_mosi = 2;
                    d0_output = true;
                break;
                case UART0_CTSN:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_CTSN;
                    mode_mosi = 4;
                    d0_output = true;
                break;
                /* spi1_d1 */
                case MCASP0_AXR0:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_AXR0;
                    mode_mosi = 3;
                    d1_output = true;
                break;
                case MII1_RX_ER:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_RX_ER;
                    mode_mosi = 2;
                    d1_output = true;
                break;
                case UART0_RTSN:
                    conf_mosi = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RTSN;
                    mode_mosi = 4;
                    d1_output = true;
                break;
            }
            switch (spi->CS)
            {
                /* spi1_cs0 */
                case MCASP0_AHCLKR:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_MCASP0_AHCLKR;
                    mode_cs = 3;
                    //spi->channel = 0;
                break;
                case RMII1_REF_CLK:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_RMII1_REF_CLK;
                    mode_cs = 2;
                    //spi->channel = 0;
                break;
                case UART0_RXD:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RXD;
                    mode_cs = 1;
                    //spi->channel = 0;
                break;
                case UART0_RTSN:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RTSN;
                    mode_cs = 5;
                    //spi->channel = 0;
                break;
                case UART1_CTSN:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_CTSN;
                    mode_cs = 4;
                    //spi->channel = 0;
                break;
                /* spi1_cs1 */
                case ECAP0_IN_PWM0_OUT:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_ECAP0_IN_PWM0_OUT;
                    mode_cs = 2;
                    //spi->channel = 1;
                break;
                case UART0_TXD:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_TXD;
                    mode_cs = 1;
                    //spi->channel = 1;
                break;
                case UART1_RTSN:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_RTSN;
                    mode_cs = 4;
                    //spi->channel = 1;
                break;
                case XDMA_EVENT_INTR0:
                    conf_cs = (volatile uint32_t*)&CONTROL_MODULE->CONF_XDMA_EVENT_INTR0;
                    mode_cs = 4;
                    //spi->channel = 1;
                break;
            }
        }
        break;
    }

    if (!(conf_sclk && conf_miso && conf_mosi /*&& conf_cs*/))
    {
        lprintf("SPI_PinMuxSetUp error!\n");
        return -1;
    }

    *conf_sclk =
        CONTROL_MODULE_CONF_rxactive
        | CONTROL_MODULE_CONF_slewctrl  // fast
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden  // pullup
        | mode_sclk;
    *conf_miso =
        CONTROL_MODULE_CONF_rxactive
        | CONTROL_MODULE_CONF_slewctrl
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden
        | mode_miso;
    *conf_mosi =
        CONTROL_MODULE_CONF_slewctrl  // fast
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden  // pullup
        | mode_mosi;
    if (conf_cs)
        *conf_cs =
            CONTROL_MODULE_CONF_slewctrl
            | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden
            | 7; // GPIO
            //| mode_cs;
    (void) mode_cs;

    GPIO_init();
    GPIO_set_dir(spi->CS, GPIO_DIR_OUTPUT);
    GPIO_write(spi->CS, 1);

    uint8_t ch = spi->channel;

    if (d0_output)
    {
        MCSPI[x]->SYST    &= ~MCSPI_SYST_SPIDATDIR0;      // set SPIDAT[0] as output
        MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_DPE0;  // SPIDAT[0] selected for transmission
    }
    else
    {
        MCSPI[x]->SYST    |=  MCSPI_SYST_SPIDATDIR0;      // set SPIDAT[0] as input
        MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_IS;    // SPIDAT[0] selected for reception
        MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_DPE0;  // no transmission on SPIDAT[0]
    }

    if (d1_output)
    {
        MCSPI[x]->SYST    &= ~MCSPI_SYST_SPIDATDIR1;      // set SPIDAT[1] as output
        MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_DPE1;  // SPIDAT[1] selected for transmission
    }
    else
    {
        MCSPI[x]->SYST    |=  MCSPI_SYST_SPIDATDIR1;      // set SPIDAT[1] as input
        MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_IS;    // SPIDAT[1] selected for reception
        MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_DPE1;  // no transmission on SPIDAT[1]
    }

    return 0;
}

int SPI_config (const SPI_t* spi)
{
    int x = spi->SPIx;
    uint8_t ch = spi->channel;

    MCSPI[x]->MODULCTRL   &= ~MCSPI_MODULCTRL_MS;         // SPI master mode
    if (spi->single)
        MCSPI[x]->MODULCTRL   |=  MCSPI_MODULCTRL_SINGLE;     // only single SPI channel will be used

    MCSPI[x]->CH[ch].CONF |=  0x7 << ctz(MCSPI_CHCONF_WL); // SPI word is 8-bits long
    MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_CLKG;  // 1 clock cycle granularity
    MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_EPOL;  // SPIEN low during active state
    MCSPI[x]->CH[ch].CONF |=  1 << ctz(MCSPI_CHCONF_TCS); // 1.5 clock cycles between CS and SPI clock

    spi->CPOL ? (MCSPI[x]->CH[ch].CONF |= MCSPI_CHCONF_POL) : (MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_POL);
    spi->CPHA ? (MCSPI[x]->CH[ch].CONF |= MCSPI_CHCONF_PHA) : (MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_PHA);

    MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_FFER;  // Enable receive FIFO for channel 0
    MCSPI[x]->CH[ch].CONF |=  MCSPI_CHCONF_FFEW;  // Enable transmit FIFO for channel 0
    MCSPI[x]->CH[ch].CTRL |=  MCSPI_CHCTRL_EN;    // Enable MCSPI0 channel 0
    return 0;
}

int SPI_init (const SPI_t* spi)
{
    SPI_ModuleClkConfig(spi);
    SPI_PinMuxSetUp(spi);
    SPI_config(spi);
    SPI_set_baud(spi);
    return 0;
}

int SPI_set_baud (const SPI_t* spi)
{
    int x = spi->SPIx;
    uint8_t ch = spi->channel;

    uint32_t divider = MCSPI_IN_CLK / spi->baud;
    uint32_t CLKD = (divider - 1) & 0xF;
    uint32_t EXTCLK = (divider - 1) >> 4;

    MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_CLKD;
    MCSPI[x]->CH[ch].CONF|= CLKD << ctz(MCSPI_CHCONF_CLKD);
    MCSPI[x]->CH[ch].CTRL &= ~MCSPI_CHCTRL_EXTCLK;
    MCSPI[x]->CH[ch].CONF |= EXTCLK << ctz(MCSPI_CHCTRL_EXTCLK);
    return 0;
}

uint8_t SPI_transfer (const SPI_t* spi, uint8_t data)
{
    uint8_t p_rx[1];
    SPI_transfern_with_header(spi, 0, 0, &data, p_rx, 1);
    return p_rx[0];
}

void SPI_transfern (const SPI_t* spi, uint8_t* p_tx, uint8_t* p_rx, unsigned int length)
{
    SPI_transfern_with_header(spi, 0, 0, p_tx, p_rx, length);
}

void SPI_transfern_with_header (const SPI_t* spi, volatile uint8_t* header, unsigned int header_length, volatile uint8_t* p_tx, volatile uint8_t* p_rx, unsigned int length)
{
    int x = spi->SPIx;
    uint8_t ch = spi->channel;

    volatile unsigned int header_tx_i = header_length;
    volatile unsigned int header_rx_i = header_length;
    volatile unsigned int tx_i = length;
    volatile unsigned int rx_i = length;

    asm(" nop");
    GPIO_write(spi->CS, 0);
    MCSPI[x]->CH[ch].CONF |= MCSPI_CHCONF_FORCE;
    while (1)
    {
        if (header_tx_i && !(MCSPI[x]->CH[ch].STAT & MCSPI_CHSTAT_TXFFF))
        {
            MCSPI[x]->CH[ch].TX = *header++;
            header_tx_i--;
        }
        if (!header_tx_i && tx_i && !(MCSPI[x]->CH[ch].STAT & MCSPI_CHSTAT_TXFFF))
        {
            if (p_tx) MCSPI[x]->CH[ch].TX = *p_tx++;
            else MCSPI[x]->CH[ch].TX = 0x00;
            tx_i--;
        }

        if (header_rx_i && MCSPI[x]->CH[ch].STAT & MCSPI_CHSTAT_RXS)
        {
            MCSPI[x]->CH[ch].RX;
            header_rx_i--;
        }
        if (!header_rx_i && rx_i && MCSPI[x]->CH[ch].STAT & MCSPI_CHSTAT_RXS)
        {
            if (p_rx) *p_rx++ = MCSPI[x]->CH[ch].RX;
            else MCSPI[x]->CH[ch].RX;
            rx_i--;
            if (!rx_i) break;
        }
    }
    MCSPI[x]->CH[ch].CONF &= ~MCSPI_CHCONF_FORCE;
    GPIO_write(spi->CS, 1);
    asm(" nop");
}

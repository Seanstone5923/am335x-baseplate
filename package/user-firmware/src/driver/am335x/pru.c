#include "pru.h"
#include "soc/pru.h"
#include "logging.h"
#include "soc/vmem.h"
#include "am335x/prm_per.h"
#include "am335x/cm_per.h"
#include "soc/gpio.h"
#include "am335x/gpio.h"
#include <fifo.h>

#ifdef __KERNEL__
#include <linux/types.h>
#include <linux/string.h>
#else
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include "am335x/cm_wkup.h"
#include "am335x/control_module.h"
#ifndef __PRU__
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/elf.h>
#include <pthread.h>
#endif
#endif

#define CHAR_RING_SIZE 0x100000

typedef struct __attribute__((__packed__)) {
    char buf[256];
    struct {
        FIFO_t fifo;
        uint8_t data[CHAR_RING_SIZE];
    };
} PRU_Printf_t;

static PRU_Printf_t *pf[2] = {
    (PRU_Printf_t *)0x94000000,
    (PRU_Printf_t *)0x95000000
};

#ifdef __PRU__

#include <tinyprintf/tinyprintf.h>

void PRU_printf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    int len = tfp_vsnprintf(pf[PRUx]->buf, sizeof(((PRU_Printf_t *)0)->buf),
                            fmt, args);
    if (len > 0)
        FIFO_Put(&pf[PRUx]->fifo, pf[PRUx]->buf, len);
    va_end(args);
}

#ifdef __GNUC__

void exit(int status)
{
    (void) status;
    while(1);
}

#endif

#else

static bool PRU_printf_enabled[2] = {true, true};

void PRU_printf_setEnabled (int x, bool enabled)
{
    PRU_printf_enabled[x] = enabled;
}

void PRU_printf_poll (void)
{
    const char* color[2] = {"33", "36"};
    for (int x = 0; x < 2; x++)
    {
        if (FIFO_IsFull(&pf[x]->fifo))
        {
            lprintf("\r\n\e[31mERROR: PRU CharRing overflow!\e[0m\r\n");
        }
        size_t len = FIFO_Length(&pf[x]->fifo);
        if (len && *(char *)FIFO_PeekN(&pf[x]->fifo, len - 1) == '\n') {
            static char buf[1024];
            int l = FIFO_Get(&pf[x]->fifo, buf, sizeof(buf));
            if (PRU_printf_enabled[x])
                lprintf("\e[%sm%.*s\e[0m", color[x], l, buf);
        }
    }
}

#ifndef __KERNEL__

pthread_t PRU_printf_thread;

int PRU_firmware_load (int x, const char* filename)
{
    PRU_halt(x);

    struct stat filestat;
    int status = stat(filename, &filestat);
    if (status < 0)
    {
        lprintf("PRU%u: failed to open firmware file: %s\r\n", x, filename);
        return -1;
    }

    int file = open(filename, O_RDONLY);
    if (file < 0)
    {
        lprintf("PRU%u: failed to open firmware file: %s\r\n", x, filename);
        return -1;
    }

    uint8_t* elf_data = mmap (0, filestat.st_size, PROT_READ, MAP_SHARED, file, 0);
    if (elf_data == MAP_FAILED)
    {
        lprintf("PRU%u: mmap failed\r\n", x);
        return -1;
    }

    lprintf("PRU%u: firmware file opened: %s, size %u\r\n", x, filename, filestat.st_size);

	struct elf32_hdr*  ehdr = (struct elf32_hdr*)  elf_data;
	struct elf32_phdr* phdr = (struct elf32_phdr*) (elf_data + ehdr->e_phoff);
    bool clpru = false;
    bool iram_loaded = false;

	/* go through the available ELF segments */
    int n_mem0 = 0;
    for (int i = 0; i < ehdr->e_phnum; i++, phdr++)
    {
		if (phdr->p_type != PT_LOAD) continue;
        if (phdr->p_paddr == 0x00000000) n_mem0++;
	}
    clpru = (n_mem0 > 1);

    phdr = (struct elf32_phdr*) (elf_data + ehdr->e_phoff);
	for (int i = 0; i < ehdr->e_phnum; i++, phdr++)
    {
		if (phdr->p_type != PT_LOAD)
			continue;

		//lprintf("  phdr: type %d paddr 0x%08x memsz %u filesz %u\n", phdr->p_type, phdr->p_paddr, phdr->p_memsz, phdr->p_filesz);

		if (phdr->p_filesz > phdr->p_memsz)
        {
			lprintf("  bad phdr filesz %u memsz %u\n", phdr->p_filesz, phdr->p_memsz);
			return -1;
		}

        if (phdr->p_filesz)
        {
            /* put the segment where the remote processor expects it */
            switch (phdr->p_paddr)
            {
                case 0x0000 ... 0x2000:
                    if (clpru && !iram_loaded) goto iram;
                //dram:
                    lprintf("    DRAM size %u\n", phdr->p_filesz);
                    memcpy((void*)&PRU_DRAM[x][phdr->p_paddr], elf_data + phdr->p_offset, phdr->p_filesz);
                break;
                case 0x10000 ... 0x13000:
                //sharedram:
                    lprintf("    SHARED RAM size %u\n", phdr->p_filesz);
                    memcpy((void*)PRU_DRAM[2], elf_data + phdr->p_offset, phdr->p_filesz);
                break;
                case 0x20000000:
                iram: {
                    int percent = phdr->p_filesz * 100 / 8192;
                    lprintf("    IRAM size %u (%d%%)\n", phdr->p_filesz, percent);
                    memcpy((void*)&PRU_IRAM[x][phdr->p_paddr & ~0x20000000], elf_data + phdr->p_offset, phdr->p_filesz);
                    iram_loaded = true;
                } break;
                default:
                break;
            }
        }
	}

    lprintf("PRU%u: firmware loaded\r\n", x);

    if (munmap(elf_data, filestat.st_size) < 0)
    {
        lprintf("PRU%u: mumap failed\r\n", x);
        return -1;
    }

    close(file);

	return 0;
}

int PRU_firmware_load_bin (int x, const char* filename)
{
    PRU_halt(x);

    struct stat filestat;
    int status = stat(filename, &filestat);
    if (status < 0)
    {
        lprintf("PRU%u: failed to open firmware file: %s\r\n", x, filename);
        return -1;
    }

    int file = open(filename, O_RDONLY);
    if (file < 0)
    {
        lprintf("PRU%u: failed to open firmware file: %s\r\n", x, filename);
        return -1;
    }

    uint8_t* data = mmap (0, filestat.st_size, PROT_READ, MAP_SHARED, file, 0);
    if (data == MAP_FAILED)
    {
        lprintf("PRU%u: mmap failed\r\n", x);
        return -1;
    }

    lprintf("PRU%u: firmware file opened: %s, size %u\r\n", x, filename, filestat.st_size);

    memcpy((void*)&PRU_IRAM[x][0], data, filestat.st_size);

    lprintf("PRU%u: firmware loaded\r\n", x);

    if (munmap(data, filestat.st_size) < 0)
    {
        lprintf("PRU%u: mumap failed\r\n", x);
        return -1;
    }

    close(file);

	return 0;
}

void* PRU_printf_main (void* data)
{
    while (1)
    {
        PRU_printf_poll();
        usleep(1000);
    }
    return NULL;
}

int PRU_init_printf (void)
{
    FIFO_Init(&pf[0]->fifo, CHAR_RING_SIZE, 1);
    FIFO_Init(&pf[1]->fifo, CHAR_RING_SIZE, 1);
    if (pthread_create(&PRU_printf_thread, NULL, PRU_printf_main, NULL))
    {
        lprintf("PRU_init_printf: Error creating thread\r\n");
        return -1;
    }
    return 0;
}

#endif

#endif

int PRU_ModuleClkConfig (void)
{
    CM_PER->PRU_CLKSTCTRL |= CM_CLKCTRL_ENABLE;
    CM_PER->PRU_CLKSTCTRL |= CLKACTIVITY_PRU_ICSS_OCP_GCLK;

    PRM_PER->RSTCTRL &= ~PRM_PER_RSTCTRL_PRU_ICSS_LRST;

    CM_PER->PRU_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_PER->PRU_CLKCTRL & CM_CLKCTRL_IDLEST) ;

    return 0;
}

int PRU_config (void)
{
    /* Allow OCP master port access by the PRU so the PRU can read external memories */
    PRU_CFG->SYSCFG &= ~PRU_CFG_SYSCFG_STANDBY_INIT;

    /* Globally enable interrupts */
    PRU_INTC->GER |= PRU_INTC_GER_EN_HINT_ANY;

    return 0;
}

int PRU_reset (int x)
{
    PRU_CTRL[x]->CTRL &= ~PRU_CTRL_CTRL_SOFT_RST_N;
    return 0;
}

int PRU_halt (int x)
{
    while (PRU_CTRL[x]->CTRL & PRU_CTRL_CTRL_RUNSTATE)
        PRU_CTRL[x]->CTRL &= ~PRU_CTRL_CTRL_EN;
    lprintf("PRU%u: halted\r\n", x);
    return 0;
}

int PRU_start (int x)
{
    PRU_CTRL[x]->CTRL |= PRU_CTRL_CTRL_EN;
    lprintf("PRU%u: started\r\n", x);
    return 0;
}

int PRU_cycle_reset (int x)
{
    PRU_CTRL[x]->CTRL &= ~PRU_CTRL_CTRL_CTR_EN; // Disable PRU cycle counter
    PRU_CTRL[x]->CYCLE = ~0;                    // Clear PRU cycle counter
    PRU_CTRL[x]->CTRL |= PRU_CTRL_CTRL_CTR_EN;  // Enable PRU cycle counter
    return 0;
}

int PRU_RAM_zero (void)
{
    for (int i = 0; i < 2; i++) memset((void*)PRU_IRAM[i], 0, 0x2000);
    for (int i = 0; i < 2; i++) memset((void*)PRU_DRAM[i], 0, 0x2000);
    memset((void*)PRU_DRAM[2], 0, 0x3000);
    return 0;
}

int PRU_init (void)
{
    #ifdef __KERNEL__
    PRU_mmap();
    #endif
    PRU_ModuleClkConfig();
    PRU_RAM_zero();
    PRU_config();
    PRU_reset(PRU0);
    PRU_reset(PRU1);
    PRU_cycle_reset(PRU0);
    PRU_cycle_reset(PRU1);
    #ifndef __PRU__
    #ifndef __KERNEL__
    PRU_init_printf();
    #endif
    #endif
    return 0;
}

#ifdef __KERNEL__

void PRU_mmap (void)
{
    if (!CM_PER) CM_PER = (AM335X_CM_PER_t*)vmem_mmap(CM_PER_BASE, sizeof(AM335X_CM_PER_t));
    if (!PRM_PER) PRM_PER = (AM335X_PRM_PER_t*)vmem_mmap(PRM_PER_BASE, sizeof(AM335X_PRM_PER_t));
    PRU_IRAM[0] = (volatile uint8_t*)vmem_mmap(PRU_IRAM0_BASE, 0x2000);
    PRU_IRAM[1] = (volatile uint8_t*)vmem_mmap(PRU_IRAM1_BASE, 0x2000);
    PRU_DRAM[0] = (volatile uint8_t*)vmem_mmap(PRU_DRAM0_BASE, 0x2000);
    PRU_DRAM[1] = (volatile uint8_t*)vmem_mmap(PRU_DRAM1_BASE, 0x2000);
    PRU_DRAM[2] = (volatile uint8_t*)vmem_mmap(PRU_DRAM2_BASE, 0x3000);
    PRU_CTRL[0] = (AM335X_PRU_CTRL_t*)vmem_mmap(PRU_CTRL0_BASE, sizeof(AM335X_PRU_CTRL_t));
    PRU_CTRL[1] = (AM335X_PRU_CTRL_t*)vmem_mmap(PRU_CTRL1_BASE, sizeof(AM335X_PRU_CTRL_t));
    PRU_CFG = (AM335X_PRU_CFG_t*)vmem_mmap(PRU_CFG_BASE, sizeof(AM335X_PRU_CFG_t));
    PRU_INTC = (AM335X_PRU_INTC_t*)vmem_mmap(PRU_INTC_BASE, sizeof(AM335X_PRU_INTC_t));
}

void PRU_unmap (void)
{
    vmem_unmap((void*)PRU_IRAM[0]);
    vmem_unmap((void*)PRU_IRAM[1]);
    vmem_unmap((void*)PRU_DRAM[0]);
    vmem_unmap((void*)PRU_DRAM[1]);
    vmem_unmap((void*)PRU_DRAM[2]);
    vmem_unmap((void*)PRU_CTRL[0]);
    vmem_unmap((void*)PRU_CTRL[1]);
    vmem_unmap((void*)PRU_CFG);
    vmem_unmap((void*)PRU_INTC);
    vmem_unmap((void*)CM_PER);
    vmem_unmap((void*)PRM_PER);
}

#endif

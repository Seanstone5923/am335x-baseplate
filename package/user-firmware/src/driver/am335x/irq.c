#ifdef __KERNEL__

#include "soc/irq.h"

static int IRQ_register(IRQ_t* irq)
{
    int ret;

    #ifndef __COBALT__

    if ((ret = request_irq(irq->irqn, irq->handler, 0, irq->name, NULL)))
    {
        lprintf("request_irq error %d.\n", ret);
        return -1;
    }

    #else

    if ((ret = xnintr_init(&irq->intr, irq->name, irq->irqn, irq->handler, NULL, 0)))
    {
        lprintf("xnintr_init error %d.\n", ret);
        return -1;
    }
    lprintf("xnintr_init ok.\n");

    if ((ret = xnintr_attach(&irq->intr, NULL)))
    {
        lprintf("xnintr_attach error %d.\n", ret);
        return -1;
    }
    lprintf("xnintr_attach ok.\n");

    xnintr_enable(&irq->intr);
    lprintf("xnintr_enable.\n");

    #endif

    return 0;
}

static int IRQ_unregister(IRQ_t* irq)
{
    #ifndef __COBALT__
    free_irq(irq->irqn, NULL);
    #else
    xnintr_disable(&irq->intr);
    xnintr_detach(&irq->intr);
    xnintr_destroy(&irq->intr);
    #endif
    return 0;
}

#endif

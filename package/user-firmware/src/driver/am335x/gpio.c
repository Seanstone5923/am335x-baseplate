#include "logging.h"
#ifndef __KERNEL__
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "soc/vmem.h"
#else
#include <linux/types.h>
#endif
#include "soc/gpio.h"
#include "am335x/cm_per.h"
#include "am335x/cm_wkup.h"
#include "am335x/control_module.h"
#include "am335x/gpio.h"

int GPIO_enable (void)
{
    /* Enable GPIO ports */
    CM_WKUP->WKUP_GPIO0_CLKCTRL |= CM_CLKCTRL_ENABLE;
    CM_PER->GPIO1_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_PER->GPIO1_CLKCTRL & CM_CLKCTRL_DISABLED) ;
    CM_PER->GPIO2_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_PER->GPIO2_CLKCTRL & CM_CLKCTRL_DISABLED) ;
    CM_PER->GPIO3_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_PER->GPIO3_CLKCTRL & CM_CLKCTRL_DISABLED) ;
    GPIO[0]->CTRL &= ~GPIO_CTRL_DISABLEMODULE;
    GPIO[1]->CTRL &= ~GPIO_CTRL_DISABLEMODULE;
    GPIO[2]->CTRL &= ~GPIO_CTRL_DISABLEMODULE;
    GPIO[3]->CTRL &= ~GPIO_CTRL_DISABLEMODULE;
    return 0;
}

int GPIO_init (void)
{
    GPIO_enable();
    return 0;
}

void GPIO_set_dir (GPIO_t gpio, GPIO_Dir dir)
{
    switch (dir)
    {
        case GPIO_DIR_INPUT:
            GPIOx(gpio)->OE |= (1 << (gpio & 0x1F));
        break;
        case GPIO_DIR_OUTPUT:
            GPIOx(gpio)->OE &= ~(1 << (gpio & 0x1F));
        break;
    }
}

void GPIO_write (GPIO_t gpio, uint8_t val)
{
    val ?
        (GPIOx(gpio)->DATAOUT |=  (1 << (gpio & 0x1F))) :
        (GPIOx(gpio)->DATAOUT &= ~(1 << (gpio & 0x1F)));
}

uint8_t GPIO_read (GPIO_t gpio)
{
    return (GPIOx(gpio)->DATAIN >> (gpio & 0x1F)) & 0x1;
}

#ifndef AM335X_PRU_H
#define AM335X_PRU_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct __attribute__((packed)) AM335X_PRU_CTRL_t
{
    volatile uint32_t CTRL;
    volatile uint32_t STS;
    volatile uint32_t WAKEUP_EN;
    volatile uint32_t CYCLE;
    volatile uint32_t STALL;
    volatile uint32_t CTBIR0;
    volatile uint32_t CTBIR1;
    volatile uint32_t CTPPR0;
    volatile uint32_t CTPPR1;
} AM335X_PRU_CTRL_t;

typedef struct __attribute__((packed)) AM335X_PRU_CFG_t
{
    /*  0 */        volatile uint32_t REVID;
    /*  4 */        volatile uint32_t SYSCFG;
    /*  8 */        volatile uint32_t GPCFG0;
    /*  C */        volatile uint32_t GPCFG1;
    /* 10 */        volatile uint32_t CGR;
    /* 14 */        volatile uint32_t ISRP;
    /* 18 */        volatile uint32_t ISP;
    /* 1C */        volatile uint32_t IESP;
    /* 20 */        volatile uint32_t IECP;
                    volatile uint32_t Reserved0[1];
    /* 28 */        volatile uint32_t PMAO;
    /* 2C */        volatile uint32_t MII_RT;
    /* 30 */        volatile uint32_t IEPCLK;
    /* 34 */        volatile uint32_t SPP;
                    volatile uint32_t Reserved1[2];
    /* 40 */        volatile uint32_t PIN_MX;
} AM335X_PRU_CFG_t;

typedef struct __attribute__((packed)) AM335X_PRU_INTC_t
{
    /*    0x0 */    volatile uint32_t REVID;
    /*    0x4 */    volatile uint32_t CR;
                    volatile uint32_t Reserved0[2];
    /*   0x10 */    volatile uint32_t GER;
                    volatile uint32_t Reserved1[2];
    /*   0x1C */    volatile uint32_t GNLR;
    /*   0x20 */    volatile uint32_t SISR;
    /*   0x24 */    volatile uint32_t SICR;
    /*   0x28 */    volatile uint32_t EISR;
    /*   0x2C */    volatile uint32_t EICR;
                    volatile uint32_t Reserved2[1];
    /*   0x34 */    volatile uint32_t HIEISR;
    /*   0x38 */    volatile uint32_t HIDISR;
                    volatile uint32_t Reserved3[(0x80-0x38)/4-1];
    /*   0x80 */    volatile uint32_t GPIR;
                    volatile uint32_t Reserved4[(0x200-0x80)/4-1];
    /*  0x200 */    volatile uint32_t SRSR0;
    /*  0x204 */    volatile uint32_t SRSR1;
                    volatile uint32_t Reserved5[(0x280-0x204)/4-1];
    /*  0x280 */    volatile uint32_t SECR0;
    /*  0x284 */    volatile uint32_t SECR1;
                    volatile uint32_t Reserved6[(0x300-0x284)/4-1];
    /*  0x300 */    volatile uint32_t ESR0;
    /*  0x304 */    volatile uint32_t ESR1;
                    volatile uint32_t Reserved7[(0x380-0x304)/4-1];
    /*  0x380 */    volatile uint32_t ECR0;
    /*  0x384 */    volatile uint32_t ECR1;
                    volatile uint32_t Reserved8[(0x400-0x384)/4-1];
    /*  0x400 */    volatile uint32_t CMR0;
    /*  0x404 */    volatile uint32_t CMR1;
    /*  0x408 */    volatile uint32_t CMR2;
    /*  0x40C */    volatile uint32_t CMR3;
    /*  0x410 */    volatile uint32_t CMR4;
    /*  0x414 */    volatile uint32_t CMR5;
    /*  0x418 */    volatile uint32_t CMR6;
    /*  0x41C */    volatile uint32_t CMR7;
    /*  0x420 */    volatile uint32_t CMR8;
    /*  0x424 */    volatile uint32_t CMR9;
    /*  0x428 */    volatile uint32_t CMR10;
    /*  0x42C */    volatile uint32_t CMR11;
    /*  0x430 */    volatile uint32_t CMR12;
    /*  0x434 */    volatile uint32_t CMR13;
    /*  0x438 */    volatile uint32_t CMR14;
    /*  0x43C */    volatile uint32_t CMR15;
                    volatile uint32_t Reserved9[(0x800-0x43C)/4-1];
    /*  0x800 */    volatile uint32_t HMR0;
    /*  0x804 */    volatile uint32_t HMR1;
    /*  0x808 */    volatile uint32_t HMR2;
                    volatile uint32_t Reserved10[(0x900-0x808)/4-1];
    /*  0x900 */    volatile uint32_t HIPIR0;
    /*  0x904 */    volatile uint32_t HIPIR1;
    /*  0x908 */    volatile uint32_t HIPIR2;
    /*  0x90C */    volatile uint32_t HIPIR3;
    /*  0x910 */    volatile uint32_t HIPIR4;
    /*  0x914 */    volatile uint32_t HIPIR5;
    /*  0x918 */    volatile uint32_t HIPIR6;
    /*  0x91C */    volatile uint32_t HIPIR7;
    /*  0x920 */    volatile uint32_t HIPIR8;
    /*  0x924 */    volatile uint32_t HIPIR9;
                    volatile uint32_t Reserved11[(0xD00-0x924)/4-1];
    /*  0xD00 */    volatile uint32_t SIPR0;
    /*  0xD04 */    volatile uint32_t SIPR1;
                    volatile uint32_t Reserved12[(0xD80-0xD04)/4-1];
    /*  0xD80 */    volatile uint32_t SITR0;
    /*  0xD84 */    volatile uint32_t SITR1;
                    volatile uint32_t Reserved13[(0x1100-0xD84)/4-1];
    /* 0x1100 */    volatile uint32_t HINLR0;
    /* 0x1104 */    volatile uint32_t HINLR1;
    /* 0x1108 */    volatile uint32_t HINLR2;
    /* 0x110C */    volatile uint32_t HINLR3;
    /* 0x1110 */    volatile uint32_t HINLR4;
    /* 0x1114 */    volatile uint32_t HINLR5;
    /* 0x1118 */    volatile uint32_t HINLR6;
    /* 0x111C */    volatile uint32_t HINLR7;
    /* 0x1120 */    volatile uint32_t HINLR8;
    /* 0x1124 */    volatile uint32_t HINLR9;
                    volatile uint32_t Reserved14[(0x1500-0x1124)/4-1];
    /* 0x1500 */    volatile uint32_t HIER;
} AM335X_PRU_INTC_t;

#ifndef __PRU__
#define PRU_DRAM0_BASE 0x4A300000
#define PRU_DRAM1_BASE 0x4A302000
#define PRU_DRAM2_BASE 0x4A310000
#define PRU_INTC_BASE  0x4A320000
#define PRU_CTRL0_BASE 0x4A322000
#define PRU_CTRL1_BASE 0x4A324000
#define PRU_CFG_BASE   0x4A326000
#else
#define PRU_DRAM0_BASE 0x00000000
#define PRU_DRAM1_BASE 0x00002000
#define PRU_DRAM2_BASE 0x00010000
#define PRU_INTC_BASE  0x00020000
#define PRU_CTRL0_BASE 0x00022000
#define PRU_CTRL1_BASE 0x00024000
#define PRU_CFG_BASE   0x00026000
#endif // #ifndef __PRU__

#define PRU_IRAM0_BASE 0x4A334000
#define PRU_IRAM1_BASE 0x4A338000

#ifndef __KERNEL__


static volatile uint8_t* const PRU_DRAM[3] =
{
    (volatile uint8_t*) PRU_DRAM0_BASE,
    (volatile uint8_t*) PRU_DRAM1_BASE,
    (volatile uint8_t*) PRU_DRAM2_BASE,
};
static AM335X_PRU_INTC_t* const PRU_INTC = (AM335X_PRU_INTC_t*) PRU_INTC_BASE;
static AM335X_PRU_CTRL_t* const PRU_CTRL[2] =
{
    (AM335X_PRU_CTRL_t*) PRU_CTRL0_BASE,
    (AM335X_PRU_CTRL_t*) PRU_CTRL1_BASE,
};
static AM335X_PRU_CFG_t* const PRU_CFG = (AM335X_PRU_CFG_t*) PRU_CFG_BASE;
static volatile uint8_t* const PRU_IRAM[2] =
{
    (volatile uint8_t*) PRU_IRAM0_BASE,
    (volatile uint8_t*) PRU_IRAM1_BASE,
};

#else

static volatile uint8_t* PRU_DRAM[3];
static AM335X_PRU_INTC_t* PRU_INTC;
static AM335X_PRU_CTRL_t* PRU_CTRL[2];
static AM335X_PRU_CFG_t* PRU_CFG;
static volatile uint8_t* PRU_IRAM[2];

#endif // ifndef __KERNEL__

#define PRU_CTRL_CTRL_SOFT_RST_N    (1 << 0)
#define PRU_CTRL_CTRL_EN            (1 << 1)
#define PRU_CTRL_CTRL_CTR_EN        (1 << 3)
#define PRU_CTRL_CTRL_RUNSTATE      (1 << 15)

#define PRU_INTC_GER_EN_HINT_ANY   0x1

#define PRU_CFG_SYSCFG_STANDBY_INIT (1 << 4)

#endif

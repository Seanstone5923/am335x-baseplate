#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/adc.h"
#include "am335x/cm_per.h"
#include "am335x/cm_wkup.h"
#include "am335x/control_module.h"
#include "am335x/gpio.h"
#include "am335x/adc_tsc.h"
#include "cxz.h"
#include "soc/dma.h"
#include "am335x/edma3.h"

int ADC_Init (void)
{
    ADC_clkconfig();
    ADC_TSC->SYSCONFIG |= 0b01 << ctz(ADC_TSC_SYSCONFIG_IdleMode);
    ADC_TSC->CTRL &= ~ADC_TSC_CTRL_Power_Down;
    ADC_config();
    return 0;
}

int ADC_clkconfig (void)
{
    CM_WKUP->WKUP_ADC_TSC_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_WKUP->WKUP_ADC_TSC_CLKCTRL & CM_CLKCTRL_IDLEST) ;
    return 0;
}

volatile uint32_t* ADC_STEPCONFIG (int step)
{
    return &ADC_TSC->TS_CHARGE_STEPCONFIG + step * 2;
}

volatile uint32_t* ADC_DELAY (int step)
{
    return &ADC_TSC->TS_CHARGE_DELAY + step * 2;
}

int ADC_enable (int enable)
{
    if (enable) ADC_TSC->CTRL |= ADC_TSC_CTRL_ENABLE;
    else ADC_TSC->CTRL &= ~ADC_TSC_CTRL_ENABLE;
    return 0;
}

int ADC_step_continuous (int step, int continuous)
{
    if (continuous) *ADC_STEPCONFIG(step) |= 0x1;
    else *ADC_STEPCONFIG(step) &= ~0x1;
    return 0;
}

int ADC_step_input (int step, int port)
{
    *ADC_STEPCONFIG(step) &= ~ADC_TSC_STEPCONFIG_SEL_INP_SWC_3_0;
    *ADC_STEPCONFIG(step) |= port << ctz(ADC_TSC_STEPCONFIG_SEL_INP_SWC_3_0);
    return 0;
}

int ADC_step_enable (int step, int enable)
{
    if (enable) ADC_TSC->STEPENABLE |= 1 << step;
    else ADC_TSC->STEPENABLE &= ~(1 << step);
    return 0;
}

int ADC_step_delay (int step, int delay)
{
    *ADC_DELAY(step) = delay;
    return 0;
}

int ADC_config (void)
{
    /* Disable ADC step config write protect */
    ADC_TSC->CTRL |= ADC_TSC_CTRL_StepConfig_WriteProtect_n_active_low;

    /* store the Step ID number with the captured ADC data in the FIFO */
    //ADC_TSC->CTRL |= ADC_TSC_CTRL_Step_ID_tag;
    return 0;
}

void ADC_clear_FIFO (void)
{
    while (ADC_TSC->FIFO0COUNT) ADC_TSC->FIFO0DATA;
}

int ADC_EnableIRQ (uint32_t irq)
{
    ADC_TSC->IRQENABLE_SET |= irq;
    return 0;
}

int ADC_DisableIRQ (uint32_t irq)
{
    ADC_TSC->IRQENABLE_CLR |= irq;
    return 0;
}

int ADC_ClearIRQ (uint32_t irq)
{
    ADC_TSC->IRQSTATUS |= irq;
    return 0;
}

uint32_t ADC_CheckIRQ (uint32_t irq)
{
    return ADC_TSC->IRQSTATUS & irq;
}

uint32_t ADC_convert_blocking (int step)
{
    if (!step)
    {
        lprintf("ADC_convert_blocking error: step should > 0.\r\n");
        return 0;
    }
    ADC_clear_FIFO();
    ADC_step_enable(step, true);
    while (!ADC_TSC->FIFO0COUNT) ;
    return ADC_TSC->FIFO0DATA & ADC_TSC_FIFODATA_ADCDATA;
}

int ADC_EnableDMA (uint32_t* buffer, uint8_t numSteps)
{
    int PaRAM_Start = DMA_GetFreePaRAM(numSteps + 1);
    if (PaRAM_Start < 0) return -1;
    for (int i = 0; i < numSteps; i++)
    {
        EDMA3PaRAM[PaRAM_Start + i] = (AM335X_EDMA3_PaRAM_t)
        {
            .SRC = (uint32_t)&ADC_TSC->FIFO0DATA,
            .DST = (uint32_t)&buffer[i],
            .ACNT = 4, // uint32_t = 4 bytes
            .BCNT = 1,
            .CCNT = 1,
            .LINK = (PaRAM_Start + ((i + 1) % numSteps)) * 0x20,
        };
    }
    EDMA3PaRAM[PaRAM_Start + numSteps] = EDMA3PaRAM[PaRAM_Start];
    DMA_Channel_SetPaEntry(EDMA_Event_TSC_ADC_FIFO0, PaRAM_Start + numSteps);
    DMA_EventEnable(EDMA_Event_TSC_ADC_FIFO0);
    ADC_TSC->DMA0REQ = 0;
    ADC_TSC->DMAENABLE_SET |= 0x1;
    return 0;
}

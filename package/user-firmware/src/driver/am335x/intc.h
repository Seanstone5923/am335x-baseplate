#ifndef AM335X_INTC_H
#define AM335X_INTC_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_INTC_t
{
    /*  0 */    volatile uint32_t REVISION;
                volatile uint32_t Reserved0[3];
    /* 10 */    volatile uint32_t SYSCONFIG;
    /* 14 */    volatile uint32_t SYSSTATUS;
                volatile uint32_t Reserved1[(0x40-0x14)/4-1];
    /* 40 */    volatile uint32_t SIR_IRQ;
    /* 44 */    volatile uint32_t SIR_FIQ;
    /* 48 */    volatile uint32_t CONTROL;
    /* 4C */    volatile uint32_t PROTECTION;
    /* 50 */    volatile uint32_t IDLE;
                volatile uint32_t Reserved2[(0x60-0x50)/4-1];
    /* 60 */    volatile uint32_t IRQ_PRIORITY;
    /* 64 */    volatile uint32_t FIQ_PRIORITY;
    /* 68 */    volatile uint32_t THRESHOLD;
                volatile uint32_t Reserved3[(0x80-0x68)/4-1];
    /* 80 */    volatile uint32_t ITR0;
    /* 84 */    volatile uint32_t MIR0;
    /* 88 */    volatile uint32_t MIR_CLEAR0;
    /* 8C */    volatile uint32_t MIR_SET0;
    /* 90 */    volatile uint32_t ISR_SET0;
    /* 94 */    volatile uint32_t ISR_CLEAR0;
    /* 98 */    volatile uint32_t PENDING_IRQ0;
    /* 9C */    volatile uint32_t PENDING_FIQ0;
    /* A0 */    volatile uint32_t ITR1;
    /* A4 */    volatile uint32_t MIR1;
    /* A8 */    volatile uint32_t MIR_CLEAR1;
    /* AC */    volatile uint32_t MIR_SET1;
    /* B0 */    volatile uint32_t ISR_SET1;
    /* B4 */    volatile uint32_t ISR_CLEAR1;
    /* B8 */    volatile uint32_t PENDING_IRQ1;
    /* BC */    volatile uint32_t PENDING_FIQ1;
    /* C0 */    volatile uint32_t ITR2;
    /* C4 */    volatile uint32_t MIR2;
    /* C8 */    volatile uint32_t MIR_CLEAR2;
    /* CC */    volatile uint32_t MIR_SET2;
    /* D0 */    volatile uint32_t ISR_SET2;
    /* D4 */    volatile uint32_t ISR_CLEAR2;
    /* D8 */    volatile uint32_t PENDING_IRQ2;
    /* DC */    volatile uint32_t PENDING_FIQ2;
    /* E0 */    volatile uint32_t ITR3;
    /* E4 */    volatile uint32_t MIR3;
    /* E8 */    volatile uint32_t MIR_CLEAR3;
    /* EC */    volatile uint32_t MIR_SET3;
    /* F0 */    volatile uint32_t ISR_SET3;
    /* F4 */    volatile uint32_t ISR_CLEAR3;
    /* F8 */    volatile uint32_t PENDING_IRQ3;
    /* FC */    volatile uint32_t PENDING_FIQ3;
} AM335X_INTC_t;

#define INTC_BASE 0x48200000

#ifndef __KERNEL__
static AM335X_INTC_t* const INTC = (AM335X_INTC_t*)INTC_BASE;
#else
static AM335X_INTC_t* INTC;
#endif

#endif

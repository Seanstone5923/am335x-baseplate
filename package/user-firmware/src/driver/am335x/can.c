#include <am335x/cm_per.h>
#include <am335x/control_module.h>
#include <am335x/dcan.h>
#include <am335x/pinmux.h>
#include <am335x/pru.h>

#include <soc/can.h>
#include <soc/vmem.h>
#include <soc/gpio.h>

#include <cxz.h>
#include <logging.h>
#include <stdio.h>

#define NUM_MSG_OBJS 64

#ifndef __PRU__
static void SwReset(CAN_t* can);
static void ConfigMsgObj(CAN_t* can);
static void EnableClk(CAN_t* can);
static void InitMsgRam(CAN_t* can);
static void SetupPinMux(CAN_t* can);
static void SetBitrate(CAN_t* can);
#endif
static void IfxCmdWrite(volatile uint32_t* cmd, uint8_t msgObj, uint32_t mask);

int CAN_Init(CAN_t* can)
{
#ifdef __PRU__
    (void)can;
#else
    EnableClk(can);
    InitMsgRam(can);
    GPIO_init();
    SetupPinMux(can);
    SwReset(can);
    DCAN[can->CANx]->CTL |= DCAN_CTL_ABO; // Enable auto-bus-on
    SetBitrate(can);
    ConfigMsgObj(can);
    CAN_SetTestMode(can, LOOPBACK);
    //CAN_SetTestMode(can, EXT_LOOPBACK);
#endif
    return 0;
}

void CAN_CheckError(CAN_t* can)
{
    int id = can->CANx;
    switch (id) {
    case CAN0:
    case CAN1:
        {
            uint32_t es = DCAN[id]->ES;
            can->error.lec     = (es & DCAN_ES_LEC) >> ctz(DCAN_ES_LEC);
            can->error.warning = (es & DCAN_ES_EWarn) >> ctz(DCAN_ES_EWarn);
            can->error.passive = (es & DCAN_ES_EPass) >> ctz(DCAN_ES_EPass);
            can->error.busOff  = (es & DCAN_ES_BOff) >> ctz(DCAN_ES_BOff);

            uint32_t errc = DCAN[id]->ERRC;
            can->error.rec = (errc & DCAN_ERRC_REC) >> ctz(DCAN_ERRC_REC);
            can->error.tec = (errc & DCAN_ERRC_TEC) >> ctz(DCAN_ERRC_TEC);
        }
        break;
    default:
        break;
    }
}

void CAN_PrintError(CAN_t* can)
{
    lprintf("CAN%u   : LEC = %u  REC = %3u  TEC = %3u"
            " Warning = %u  Passive = %u  Bus Off = %u\n",
            can->CANx, can->error.lec, can->error.rec, can->error.tec,
            can->error.warning, can->error.passive, can->error.busOff
    );
}

int CAN_SetTestMode(CAN_t* can, CAN_TestMode_t mode)
{
    int id = can->CANx;
    switch (mode) {
    case LOOPBACK:
        if (can->loopback) {
            DCAN[id]->CTL |= DCAN_CTL_Test;
            asm(" nop");
            DCAN[id]->TEST |= DCAN_TEST_LBack;
        } else {
            DCAN[id]->TEST &= ~DCAN_TEST_LBack;
            asm(" nop");
            DCAN[id]->CTL &= ~DCAN_CTL_Test;
        }
        break;
    case EXT_LOOPBACK:
        if (can->extLoopback) {
            DCAN[id]->CTL |= DCAN_CTL_Test;
            asm(" nop");
            DCAN[id]->TEST &= ~DCAN_TEST_LBack;
            DCAN[id]->TEST |= DCAN_TEST_EXL;
        } else {
            DCAN[id]->TEST &= ~DCAN_TEST_EXL;
            asm(" nop");
            DCAN[id]->CTL &= ~DCAN_CTL_Test;
        }
        break;
    default:
        break;
    }

    return 0;
}

int CAN_Transmit(CAN_t* can, CAN_Frame_t *frame)
{
    int id = can->CANx;

    // Find a free message object for transmit
    uint8_t msgNum = 0;
    uint64_t txRqst = (uint64_t)DCAN[id]->TXRQ34 << 32 | DCAN[id]->TXRQ12;
    txRqst &= ~((1ull << can->numRxBufs) - 1);

    if (!txRqst) msgNum = can->numRxBufs + 1;
    else msgNum = NUM_MSG_OBJS - clzll(txRqst) + 1;

    if (msgNum > NUM_MSG_OBJS) {
        //lprintf("CAN[%u] TX FIFO full!\n", id);
        return -1;
    }
    //lprintf("CAN[%u][TX](#%u) 0x%08x%08x ", id, msgNum, (uint32_t)(txRqst >> 32), (uint32_t)(txRqst & 0xFFFFFFFF)); CAN_PrintFrame(frame);

    DCAN[id]->IF1ARB &= ~DCAN_IFARB_ID28_to_ID0;
    DCAN[id]->IF1ARB |= frame->id << 18;  // Standard frame identifier
    DCAN[id]->IF1MCTL &= ~DCAN_IFMCTL_DLC;
    DCAN[id]->IF1MCTL |= frame->dlc;
    DCAN[id]->IF1DATA = frame->data & 0xFFFFFFFF;
    DCAN[id]->IF1DATB = frame->data >> 32;
    DCAN[id]->IF1MCTL |= DCAN_IFMCTL_TxRqst;

    IfxCmdWrite(&DCAN[id]->IF1CMD, msgNum, DCAN_IFCMD_WR_RD | DCAN_IFCMD_Arb |
            DCAN_IFCMD_Control | DCAN_IFCMD_Data_A | DCAN_IFCMD_Data_B |
            DCAN_IFCMD_TxRqst_NewDat);

    return 0;
}

int CAN_Receive(CAN_t* can, CAN_Frame_t *frame)
{
    int id = can->CANx;
    if (!(DCAN[id]->IF3OBS & DCAN_IF3OBS_Upd))
        return 0;

    frame->id = (DCAN[id]->IF3ARB & DCAN_IFARB_ID28_to_ID0) >> 18;
    frame->dlc = DCAN[id]->IF3MCTL & DCAN_IFMCTL_DLC;
    frame->data = (uint64_t) DCAN[id]->IF3DATA | (uint64_t) DCAN[id]->IF3DATB << 32;

    return 1;
}

void CAN_xPrintFrame(CAN_t* can, CAN_Dir_t dir, CAN_Frame_t *frame)
{
    lprintf("[CAN%u][%s] id=0x%03x dlc=%u "
    #ifdef __PRU__
    "0x%08lx%08lx"
    #else
    "0x%08x%08x"
    #endif
    "\n", can->CANx, dir ? "TX" : "RX", frame->id, frame->dlc, (uint32_t)(frame->data >> 32), (uint32_t)frame->data);
}

void CAN_PrintFrame(CAN_Frame_t *frame)
{
    lprintf("CAN: id=0x%03x dlc=%u "
    #ifdef __PRU__
    "0x%08lx%08lx"
    #else
    "0x%08x%08x"
    #endif
    "\n", frame->id, frame->dlc, (uint32_t)(frame->data >> 32), (uint32_t)frame->data);
}

void CAN_snPrintFrame(char* buffer, int size, CAN_Frame_t *frame)
{
    snprintf(buffer, size, "id=0x%03x dlc=%u "
    #ifdef __PRU__
    "0x%08lx%08lx"
    #else
    "0x%08x%08x"
    #endif
    "\n", frame->id, frame->dlc, (uint32_t)(frame->data >> 32), (uint32_t)frame->data);
}

#ifndef __PRU__

static void SwReset(CAN_t* can)
{
    int id = can->CANx;
    DCAN[id]->CTL |= DCAN_CTL_Init;
    DCAN[id]->CTL |= DCAN_CTL_SWR;
    DCAN[id]->CTL &= ~DCAN_CTL_Init;
}

static void ConfigMsgObj(CAN_t* can)
{
    int id = can->CANx;

    uint32_t cmdMask = DCAN_IFCMD_WR_RD | DCAN_IFCMD_Mask | DCAN_IFCMD_Arb |
                       DCAN_IFCMD_Control;

    // Disable filtering. This doesn't seem to be need at first place, but turns
    // out that resetting DCAN_IFMCTL_UMask isn't sufficient.
    DCAN[id]->IF1MCTL |= DCAN_IFMCTL_UMask | DCAN_IFMCTL_EoB;
    DCAN[id]->IF1MSK = 0;

    DCAN[id]->IF1ARB |= DCAN_IFARB_MsgVal;

    // RX
    DCAN[id]->IF1ARB &= ~DCAN_IFARB_Dir;
    for (uint8_t i = 1; i <= can->numRxBufs; i++) {
        IfxCmdWrite(&DCAN[id]->IF1CMD, i, cmdMask);
    }

    // Enable automatically updating received message objects to IF3
    if (can->numRxBufs) {
        DCAN[id]->IF3UPD12 = can->numRxBufs >= 32 ? 0xffffffff : ((1 << can->numRxBufs) - 1);
        DCAN[id]->IF3UPD34 = can->numRxBufs > 32 ? (1 << (can->numRxBufs - 32)) - 1 : 0;
        DCAN[id]->IF3OBS = DCAN_IF3OBS_DataB | DCAN_IF3OBS_DataA |
                           DCAN_IF3OBS_Ctrl | DCAN_IF3OBS_Arb;
    }

    // TX
    DCAN[id]->IF1ARB |= DCAN_IFARB_Dir;
    for (uint8_t i = can->numRxBufs + 1; i <= NUM_MSG_OBJS; ++i) {
        IfxCmdWrite(&DCAN[id]->IF1CMD, i, cmdMask);
    }
}

static void EnableClk(CAN_t* can)
{
    switch (can->CANx) {
    case CAN0:
        CM_PER->DCAN0_CLKCTRL |= CM_CLKCTRL_ENABLE;
        while (CM_PER->DCAN0_CLKCTRL & CM_CLKCTRL_IDLEST)
            ;
        break;
    case CAN1:
        CM_PER->DCAN1_CLKCTRL |= CM_CLKCTRL_ENABLE;
        while (CM_PER->DCAN1_CLKCTRL & CM_CLKCTRL_IDLEST)
            ;
        break;
    default:
        break;
    }
}

static void InitMsgRam(CAN_t* can)
{
    switch (can->CANx) {
    case CAN0:
        CONTROL_MODULE->DCAN_RAMINIT |=
            CONTROL_MODULE_DCAN_RAMINIT_dcan0_raminit_start;
        // Check https://bit.ly/2rEUfwp for why we don't wait til raminit_done
        // here.
        break;
    case CAN1:
        CONTROL_MODULE->DCAN_RAMINIT |=
            CONTROL_MODULE_DCAN_RAMINIT_dcan1_raminit_start;
        break;
    default:
        break;
    }
}

/* FIXME: this actually has no effect, since CONTROL_MODULE registers can
          only be written in previledged mode (i.e. in kernel mode) */
static void SetupPinMux(CAN_t* can)
{
    uint8_t mode_tx = 0, mode_rx = 0;
    volatile uint32_t *conf_tx, *conf_rx;

    switch (can->CANx) {
    case CAN0:
        switch (can->txPin)
        {
            case MII1_TXD3:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_TXD3;
                mode_tx = 1;
                break;
            case UART0_RXD:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RXD;
                mode_tx = 2;
                break;
            case UART1_CTSN:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_CTSN;
                mode_tx = 2;
                break;
            default:
                return;
        }
        switch (can->rxPin)
        {
            case MII1_TXD2:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_MII1_TXD2;
                mode_rx = 1;
                break;
            case UART0_TXD:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_TXD;
                mode_rx = 2;
                break;
            case UART1_RTSN:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_RTSN;
                mode_rx = 2;
                break;
            default:
                return;
        }
        break;
    case CAN1:
        switch (can->txPin)
        {
            case MMC0_CLK:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_MMC0_CLK;
                mode_tx = 4;
                break;
            case UART0_CTSN:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_CTSN;
                mode_tx = 2;
                break;
            case UART1_RXD:
                conf_tx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_RXD;
                mode_tx = 2;
                break;
            default:
                return;
        }
        switch (can->rxPin)
        {
            case MMC0_CMD:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_MMC0_CMD;
                mode_rx = 4;
                break;
            case UART0_RTSN:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART0_RTSN;
                mode_rx = 2;
                break;
            case UART1_TXD:
                conf_rx = (volatile uint32_t*)&CONTROL_MODULE->CONF_UART1_TXD;
                mode_rx = 2;
                break;
            default:
                return;
        }
    default:
        return;
    }

    GPIO_set_dir(can->txPin, GPIO_DIR_OUTPUT);
    GPIO_set_dir(can->rxPin, GPIO_DIR_INPUT);
    *conf_tx =
        CONTROL_MODULE_CONF_slewctrl  // fast
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden  // pullup
        | mode_tx;
    *conf_rx =
        CONTROL_MODULE_CONF_rxactive
        | CONTROL_MODULE_CONF_slewctrl
        | CONTROL_MODULE_CONF_putypesel | CONTROL_MODULE_CONF_puden
        | mode_rx;
}

static void SetBitrate(CAN_t* can)
{
    int id = can->CANx;

    // Configure baud rate prescaler
    uint32_t prescaler = 1000000 / can->bitrate;
    uint32_t BRP  = prescaler & 0x3F;
    uint32_t BRPE = prescaler >> 6;

    // Enter initialization mode
    DCAN[id]->CTL |= DCAN_CTL_Init;
    DCAN[id]->CTL |= DCAN_CTL_CCE;
    while (!(DCAN[id]->CTL & DCAN_CTL_Init))
        ;

    DCAN[id]->BTR &= ~DCAN_BTR_BRP;
    DCAN[id]->BTR &= ~DCAN_BTR_BRPE;
    DCAN[id]->BTR |= BRP  << ctz(DCAN_BTR_BRP);
    DCAN[id]->BTR |= BRPE << ctz(DCAN_BTR_BRPE);

    // lprintf("*[CAN%u] bit timing:\n", id);
    // lprintf("*   BRP:   %u\n", (DCAN[id]->BTR & DCAN_BTR_BRP) >> ctz(DCAN_BTR_BRP));
    // lprintf("*   BRPE:  %u\n", (DCAN[id]->BTR & DCAN_BTR_BRPE) >> ctz(DCAN_BTR_BRPE));
    // lprintf("*   SJW:   %u\n", (DCAN[id]->BTR & DCAN_BTR_SJW) >> ctz(DCAN_BTR_SJW));
    // lprintf("*   TSeg1: %u\n", (DCAN[id]->BTR & DCAN_BTR_TSeg1) >> ctz(DCAN_BTR_TSeg1));
    // lprintf("*   TSeg2: %u\n", (DCAN[id]->BTR & DCAN_BTR_TSeg2) >> ctz(DCAN_BTR_TSeg2));

    // FIXME
    switch (can->bitrate) {
    case 500000:
        DCAN[id]->BTR = 0x1c02;
        break;
    case 1000000:
        DCAN[id]->BTR = 0x2701;
        break;
    }

    // lprintf("[CAN%u] bit timing:\n", id);
    // lprintf("   BRP:   %u\n", (DCAN[id]->BTR & DCAN_BTR_BRP) >> ctz(DCAN_BTR_BRP));
    // lprintf("   BRPE:  %u\n", (DCAN[id]->BTR & DCAN_BTR_BRPE) >> ctz(DCAN_BTR_BRPE));
    // lprintf("   SJW:   %u\n", (DCAN[id]->BTR & DCAN_BTR_SJW) >> ctz(DCAN_BTR_SJW));
    // lprintf("   TSeg1: %u\n", (DCAN[id]->BTR & DCAN_BTR_TSeg1) >> ctz(DCAN_BTR_TSeg1));
    // lprintf("   TSeg2: %u\n", (DCAN[id]->BTR & DCAN_BTR_TSeg2) >> ctz(DCAN_BTR_TSeg2));

    // Exit initialization mode
    DCAN[id]->CTL &= ~DCAN_CTL_Init;
    DCAN[id]->CTL &= ~DCAN_CTL_CCE;
    while (DCAN[id]->CTL & DCAN_CTL_Init)
        ;
}

#endif  // !__PRU__

static void IfxCmdWrite(volatile uint32_t *cmd, uint8_t msgObj, uint32_t mask)
{
    *cmd = mask | msgObj;
    asm(" nop");
    // Wait for transfer to complete.
    while (*cmd & DCAN_IFCMD_Busy)
        ;
}

#ifndef AM335X_ADC_TSC_H
#define AM335X_ADC_TSC_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_ADC_TSC_t
{
    volatile uint32_t REVISION;
    volatile uint32_t Reserved0[(0x10-0x00)/4-1];
    volatile uint32_t SYSCONFIG;
    volatile uint32_t Reserved1[(0x24-0x10)/4-1];
    volatile uint32_t IRQSTATUS_RAW;
    volatile uint32_t IRQSTATUS;
    volatile uint32_t IRQENABLE_SET;
    volatile uint32_t IRQENABLE_CLR;
    volatile uint32_t IRQWAKEUP;
    volatile uint32_t DMAENABLE_SET;
    volatile uint32_t DMAENABLE_CLR;
    volatile uint32_t CTRL;
    volatile uint32_t ADCSTAT;
    volatile uint32_t ADCRANGE;
    volatile uint32_t ADC_CLKDIV;
    volatile uint32_t ADC_MISC;
    volatile uint32_t STEPENABLE;
    volatile uint32_t IDLECONFIG;
    volatile uint32_t TS_CHARGE_STEPCONFIG;
    volatile uint32_t TS_CHARGE_DELAY;
    volatile uint32_t STEPCONFIG1;
    volatile uint32_t STEPDELAY1;
    volatile uint32_t STEPCONFIG2;
    volatile uint32_t STEPDELAY2;
    volatile uint32_t STEPCONFIG3;
    volatile uint32_t STEPDELAY3;
    volatile uint32_t STEPCONFIG4;
    volatile uint32_t STEPDELAY4;
    volatile uint32_t STEPCONFIG5;
    volatile uint32_t STEPDELAY5;
    volatile uint32_t STEPCONFIG6;
    volatile uint32_t STEPDELAY6;
    volatile uint32_t STEPCONFIG7;
    volatile uint32_t STEPDELAY7;
    volatile uint32_t STEPCONFIG8;
    volatile uint32_t STEPDELAY8;
    volatile uint32_t STEPCONFIG9;
    volatile uint32_t STEPDELAY9;
    volatile uint32_t STEPCONFIG10;
    volatile uint32_t STEPDELAY10;
    volatile uint32_t STEPCONFIG11;
    volatile uint32_t STEPDELAY11;
    volatile uint32_t STEPCONFIG12;
    volatile uint32_t STEPDELAY12;
    volatile uint32_t STEPCONFIG13;
    volatile uint32_t STEPDELAY13;
    volatile uint32_t STEPCONFIG14;
    volatile uint32_t STEPDELAY14;
    volatile uint32_t STEPCONFIG15;
    volatile uint32_t STEPDELAY15;
    volatile uint32_t STEPCONFIG16;
    volatile uint32_t STEPDELAY16;
    volatile uint32_t FIFO0COUNT;
    volatile uint32_t FIFO0THRESHOLD;
    volatile uint32_t DMA0REQ;
    volatile uint32_t FIFO1COUNT;
    volatile uint32_t FIFO01HRESHOLD;
    volatile uint32_t DMA1REQ;
    volatile uint32_t Reserved2[(0x100-0xF8)/4-1];
    volatile uint32_t FIFO0DATA;
    volatile uint32_t Reserved3[(0x200-0x100)/4-1];
    volatile uint32_t FIFO1DATA;
} AM335X_ADC_TSC_t;

#define ADC_TSC_BASE 0x44E0D000

#ifndef __KERNEL__
static AM335X_ADC_TSC_t* const ADC_TSC = (AM335X_ADC_TSC_t*)ADC_TSC_BASE;
#else
static AM335X_ADC_TSC_t* ADC_TSC;
#endif

#define ADC_TSC_SYSCONFIG_IdleMode (0b11 << 2)

#define ADC_IRQ_End_of_Sequence (1 << 1)

#define ADC_TSC_CTRL_ENABLE             (1 << 0)
#define ADC_TSC_CTRL_Step_ID_tag        (1 << 1)
#define ADC_TSC_CTRL_StepConfig_WriteProtect_n_active_low (1 << 2)
#define ADC_TSC_CTRL_ADC_Bias_Select    (1 << 3)
#define ADC_TSC_CTRL_Power_Down         (1 << 4)
#define ADC_TSC_CTRL_AFE_Pen_Ctrl       (0b11 << 5)
#define ADC_TSC_CTRL_Touch_Screen_Enable (1 << 7)
#define ADC_TSC_CTRL_HW_event_mapping   (1 << 8)
#define ADC_TSC_CTRL_HW_preempt         (1 << 9)

#define ADC_TSC_STEPENABLE_STEP1 (1 << 1)
#define ADC_TSC_STEPENABLE_STEP2 (1 << 2)
#define ADC_TSC_STEPENABLE_STEP3 (1 << 3)
#define ADC_TSC_STEPENABLE_STEP4 (1 << 4)
#define ADC_TSC_STEPENABLE_STEP5 (1 << 5)
#define ADC_TSC_STEPENABLE_STEP6 (1 << 6)
#define ADC_TSC_STEPENABLE_STEP7 (1 << 7)
#define ADC_TSC_STEPENABLE_STEP8 (1 << 8)
#define ADC_TSC_STEPENABLE_STEP9 (1 << 9)
#define ADC_TSC_STEPENABLE_STEP10 (1 << 10)
#define ADC_TSC_STEPENABLE_STEP11 (1 << 11)
#define ADC_TSC_STEPENABLE_STEP12 (1 << 12)
#define ADC_TSC_STEPENABLE_STEP13 (1 << 13)
#define ADC_TSC_STEPENABLE_STEP14 (1 << 14)
#define ADC_TSC_STEPENABLE_STEP15 (1 << 15)
#define ADC_TSC_STEPENABLE_STEP16 (1 << 16)

#define ADC_TSC_STEPCONFIG_Mode         (0b11 << 0)
#define ADC_TSC_STEPCONFIG_Averaging    (0b111 << 2)
#define ADC_TSC_STEPCONFIG_XPPSW_SWC    (1 << 5)
#define ADC_TSC_STEPCONFIG_XNNSW_SWC    (1 << 6)
#define ADC_TSC_STEPCONFIG_YPPSW_SWC    (1 << 7)
#define ADC_TSC_STEPCONFIG_YNNSW_SWC    (1 << 8)
#define ADC_TSC_STEPCONFIG_XNPSW_SWC    (1 << 9)
#define ADC_TSC_STEPCONFIG_YPNSW_SWC    (1 << 10)
#define ADC_TSC_STEPCONFIG_WPNSW_SWC    (1 << 11)
#define ADC_TSC_STEPCONFIG_SEL_RFP_SWC_2_0  (0b111 << 12)
#define ADC_TSC_STEPCONFIG_SEL_INM_SWC_3_0  (0b1111 << 15)
#define ADC_TSC_STEPCONFIG_SEL_INP_SWC_3_0  (0b1111 << 19)
#define ADC_TSC_STEPCONFIG_SEL_RFM_SWC_1_0  (0b11 << 23)
#define ADC_TSC_STEPCONFIG_Diff_CNTRL   (1 << 25)
#define ADC_TSC_STEPCONFIG_FIFO_select  (1 << 26)
#define ADC_TSC_STEPCONFIG_Range_check  (1 << 27)

#define ADC_TSC_FIFODATA_ADCDATA (0xFFF)
#define ADC_TSC_FIFODATA_ADCCHNLID (0xF << 16)

#define AIN0 0
#define AIN1 1
#define AIN2 2
#define AIN3 3
#define AIN4 4
#define AIN5 5
#define AIN6 6
#define AIN7 7

#endif

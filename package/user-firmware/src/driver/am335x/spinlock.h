#ifndef AM335X_SPINLOCK_H
#define AM335X_SPINLOCK_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_SPINLOCK_t
{
    volatile uint32_t REV;
    volatile uint32_t Reserved0[(0x10-0x04)/4];
    volatile uint32_t SYSCONFIG;
    volatile uint32_t SYSSTATUS;
    volatile uint32_t Reserved1[(0x800-0x18)/4];
    volatile uint32_t LOCK_REG_0;
    volatile uint32_t LOCK_REG_1;
    volatile uint32_t LOCK_REG_2;
    volatile uint32_t LOCK_REG_3;
    volatile uint32_t LOCK_REG_4;
    volatile uint32_t LOCK_REG_5;
    volatile uint32_t LOCK_REG_6;
    volatile uint32_t LOCK_REG_7;
    volatile uint32_t LOCK_REG_8;
    volatile uint32_t LOCK_REG_9;
    volatile uint32_t LOCK_REG_10;
    volatile uint32_t LOCK_REG_11;
    volatile uint32_t LOCK_REG_12;
    volatile uint32_t LOCK_REG_13;
    volatile uint32_t LOCK_REG_14;
    volatile uint32_t LOCK_REG_15;
    volatile uint32_t LOCK_REG_16;
    volatile uint32_t LOCK_REG_17;
    volatile uint32_t LOCK_REG_18;
    volatile uint32_t LOCK_REG_19;
    volatile uint32_t LOCK_REG_20;
    volatile uint32_t LOCK_REG_21;
    volatile uint32_t LOCK_REG_22;
    volatile uint32_t LOCK_REG_23;
    volatile uint32_t LOCK_REG_24;
    volatile uint32_t LOCK_REG_25;
    volatile uint32_t LOCK_REG_26;
    volatile uint32_t LOCK_REG_27;
    volatile uint32_t LOCK_REG_28;
    volatile uint32_t LOCK_REG_29;
    volatile uint32_t LOCK_REG_30;
    volatile uint32_t LOCK_REG_31;
} AM335X_SPINLOCK_t;

#define SPINLOCK_BASE 0x480CA000

#ifndef __KERNEL__
static AM335X_SPINLOCK_t* const SPINLOCK = (AM335X_SPINLOCK_t*)SPINLOCK_BASE;
#else
static AM335X_SPINLOCK_t* SPINLOCK;
#endif

#define SPINLOCK_SYSCONFIG_CLOCKACTIVITY   (0b1 << 8)
#define SPINLOCK_SYSCONFIG_SIDLEMODE       (0b11 << 3)
#define SPINLOCK_SYSCONFIG_SOFTRESET       (1 << 1)
#define SPINLOCK_SYSCONFIG_AUTOGATING      (1 << 0)

#define SPINLOCK_SYSSTATUS_RESETDONE       (1 << 0)

#endif

#ifndef AM335X_I2C_H
#define AM335X_I2C_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif


typedef struct AM335X_I2C_t
{
    /*  0 */    volatile uint32_t REVNB_LO; /* Module Revision Register (low bytes) */
    /*  4 */    volatile uint32_t REVNB_HI; /* Module Revision Register (high bytes) */
                volatile uint32_t Reserved0[2];
    /* 10 */    volatile uint32_t SYSC; /* System Configuration Register */
                volatile uint32_t Reserved1[(0x24-0x10)/4-1];
    /* 24 */    volatile uint32_t IRQSTATUS_RAW; /* I2C Status Raw Register */
    /* 28 */    volatile uint32_t IRQSTATUS; /* I2C Status Register */
    /* 2C */    volatile uint32_t IRQENABLE_SET; /* I2C Interrupt Enable Set Register */
    /* 30 */    volatile uint32_t IRQENABLE_CLR; /* I2C Interrupt Enable Clear Register */
    /* 34 */    volatile uint32_t WE; /* I2C Wakeup Enable Register */
    /* 38 */    volatile uint32_t DMARXENABLE_SET; /* Receive DMA Enable Set Register */
    /* 3C */    volatile uint32_t DMATXENABLE_SET; /* Transmit DMA Enable Set Register */
    /* 40 */    volatile uint32_t DMARXENABLE_CLR; /* Receive DMA Enable Clear Register */
    /* 44 */    volatile uint32_t DMATXENABLE_CLR; /* Transmit DMA Enable Clear Register */
    /* 48 */    volatile uint32_t DMARXWAKE_EN; /* Receive DMA Wakeup Register */
    /* 4C */    volatile uint32_t DMATXWAKE_EN; /* Transmit DMA Wakeup Register */
                volatile uint32_t Reserved2[(0x90-0x4C)/4-1];
    /* 90 */    volatile uint32_t SYSS; /* System Status Register */
    /* 94 */    volatile uint32_t BUF; /* Buffer Configuration Register */
    /* 98 */    volatile uint32_t CNT; /* Data Counter Register */
    /* 9C */    volatile uint32_t DATA; /* Data Access Register */
                volatile uint32_t Reserved3[1];
    /* A4 */    volatile uint32_t CON; /* I2C Configuration Register */
    /* A8 */    volatile uint32_t OA; /* I2C Own Address Register */
    /* AC */    volatile uint32_t SA; /* I2C Slave Address Register */
    /* B0 */    volatile uint32_t PSC; /* I2C Clock Prescaler Register */
    /* B4 */    volatile uint32_t SCLL; /* I2C SCL Low Time Register */
    /* B8 */    volatile uint32_t SCLH; /* I2C SCL High Time Register */
    /* BC */    volatile uint32_t SYSTEST; /* System Test Register */
    /* C0 */    volatile uint32_t BUFSTAT; /* I2C Buffer Status Register */
    /* C4 */    volatile uint32_t OA1; /* I2C Own Address 1 Register */
    /* C8 */    volatile uint32_t OA2; /* I2C Own Address 2 Register */
    /* CC */    volatile uint32_t OA3; /* I2C Own Address 3 Register */
    /* D0 */    volatile uint32_t ACTOA; /* Active Own Address Register */
    /* D4 */    volatile uint32_t SBLOCK; /* I2C Clock Blocking Enable Register */
} AM335X_I2C_t;

#define I2C0_BASE 0x44E0B000
#define I2C1_BASE 0x4802A000
#define I2C2_BASE 0x4819C000

#ifndef __KERNEL__
static AM335X_I2C_t* const I2C[3] =
{
    (AM335X_I2C_t*) I2C0_BASE,
    (AM335X_I2C_t*) I2C1_BASE,
    (AM335X_I2C_t*) I2C2_BASE,
};
#else
static AM335X_I2C_t* I2C[3];
#endif

#define I2C0 0
#define I2C1 1
#define I2C2 2

#define I2C_IRQ_XDR (1 << 14)
#define I2C_IRQ_RDR (1 << 13)
#define I2C_IRQ_BB (1 << 12)
#define I2C_IRQ_XRDY (1 << 4)
#define I2C_IRQ_RRDY (1 << 3)

#define I2C_SYSC_SRST (1 << 1)
#define I2C_SYSC_AUTOIDLE (1 << 0)

#define I2C_CON_MST (1 << 10)
#define I2C_CON_EN (1 << 15)
#define I2C_CON_TRX (1 << 9)
#define I2C_CON_STT (1 << 0)
#define I2C_CON_STP (1 << 1)

#define I2C_BUF_RXFIFO_CLR (1 << 14)
#define I2C_BUF_RXTRSH (0b111111 << 8)
#define I2C_BUF_TXFIFO_CLR (1 << 6)
#define I2C_BUF_TXTRSH (0b111111 << 0)

#define I2C_BUFSTAT_TXSTAT (0b111111 << 0)
#define I2C_BUFSTAT_RXSTAT (0b111111 << 8)

#endif

#include "am335x/pru.h"
#include "soc/pru.h"
#include "soc/pru_intc.h"
#include "logging.h"

void PRU_INTC_SysEvent_Enable (uint8_t sysEvent)
{
    PRU_INTC->EISR = sysEvent;
}

void PRU_INTC_SysEvent_Disable (uint8_t sysEvent)
{
    PRU_INTC->EICR = sysEvent;
}

void PRU_INTC_SysEvent_SetChannel (uint8_t sysEvent, uint8_t channel)
{
	uint32_t offset = (sysEvent%4) * 8;
    (&PRU_INTC->CMR0)[sysEvent/4] &= ~(0xF << offset);
	(&PRU_INTC->CMR0)[sysEvent/4] |= channel << offset;
}

void PRU_INTC_SysEvent_SetPending (uint8_t sysEvent)
{
    PRU_INTC->SISR = sysEvent;
}

void PRU_INTC_SysEvent_ClearPending (uint8_t sysEvent)
{
    PRU_INTC->SICR = sysEvent;
}

void PRU_INTC_Channel_SetHostInterrupt (uint8_t channel, uint8_t hostIntr)
{
	uint32_t offset = (channel%4) * 8;
	(&PRU_INTC->HMR0)[channel/4] &= ~(0xF << offset);
	(&PRU_INTC->HMR0)[channel/4] |= hostIntr << offset;
}

void PRU_INTC_HostInterrupt_Enable (uint8_t hostIntr)
{
    PRU_INTC->HIEISR = hostIntr;
}

void PRU_INTC_HostInterrupt_Disable (uint8_t hostIntr)
{
    PRU_INTC->HIDISR = hostIntr;
}

int PRU_INTC_GetSysEvent (uint8_t host)
{
    uint32_t event = (&PRU_INTC->HIPIR0)[host];
    return ((event >> 31) & 1) ? -1 : (int)event;
}

#ifdef __PRU__

int PRU_INTC_CheckInterrupt (uint8_t host)
{
    /* Bit 30 of R31 maps to Host Interrupt 0 */
    /* Bit 31 of R31 maps to Host Interrupt 1 */
    return (read_r31() >> (30 + host)) & 1;
}

#endif

#ifndef __PRU__
#ifndef __KERNEL__

#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

int PRU_INTC_Open (struct pollfd* pru_intc)
{
    const char* proc_entry = "/proc/pru-intc";
    int fd = open(proc_entry, O_RDONLY);
    if (fd < 0)
    {
        lprintf("Failed to open %s\n", proc_entry);
        return -1;
    }
    pru_intc->fd = fd;
    pru_intc->events = POLLIN;
    return 0;
}

#endif
#endif

#ifdef __KERNEL__

#include <linux/irq.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include "soc/irq.h"
#include "fifo.h"

static FIFO_t* IRQ_FIFO;
static uint8_t IRQ_FIFO_buffer[sizeof(FIFO_t) + 32];

static IRQ_t irqs[8] =
{
	{ .name = "host2" },
	{ .name = "host3" },
	{ .name = "host4" },
	{ .name = "host5" },
	{ .name = "host6" },
	{ .name = "host7" },
	{ .name = "host8" },
	{ .name = "host9" },
};

static DECLARE_WAIT_QUEUE_HEAD(chardev_waitqueue); // Declare waitqueue variable

#ifndef __COBALT__
static irqreturn_t prurt_intc_irq_handler(int irq, void *dev_id)
#else
static int prurt_intc_irq_handler(struct xnintr * intr)
#endif
{
    for (;;)
    {
        int32_t event = PRU_INTC->GPIR;
        if (event >= 0)
        {
            //lprintf("PRU SysEvent %u\n", event);
            PRU_INTC_SysEvent_ClearPending(event);
            uint8_t e = (uint8_t)event;
            FIFO_Put(IRQ_FIFO, &e, 1);
            wake_up_interruptible(&chardev_waitqueue);
        }
        else break;
    }
    #ifndef __COBALT__
	return IRQ_HANDLED;
    #else
    return XN_IRQ_HANDLED;
    #endif
}

static int prurt_intc_probe(struct platform_device *pdev)
{
    struct device *dev = &pdev->dev;
	dev_info(dev, "probe");

    IRQ_FIFO = FIFO_Init((void*)IRQ_FIFO_buffer, 32, sizeof(uint8_t));

    for (int i = 0; i < sizeof(irqs[i])/sizeof(irqs[0]); i++)
    {
		irqs[i].irqn = platform_get_irq_byname(pdev, irqs[i].name);
		irqs[i].handler = prurt_intc_irq_handler;
		IRQ_register(&irqs[i]);
    }

    return 0;
}

static int prurt_intc_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	dev_info(dev, "remove");
	for (int i = 0; i < sizeof(irqs[i])/sizeof(irqs[0]); i++)
		IRQ_unregister(&irqs[i]);
	return 0;
}

static const struct of_device_id prurt_intc_of_match[] = {
	{
		.compatible = "ti,am335x-pru-intc",
		.data = NULL,
	},
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, prurt_intc_of_match);

static struct platform_driver prurt_intc_driver = {
	.driver = {
		.name = "prurt-intc",
		.of_match_table = prurt_intc_of_match,
	},
	.probe  = prurt_intc_probe,
	.remove = prurt_intc_remove,
};

#endif

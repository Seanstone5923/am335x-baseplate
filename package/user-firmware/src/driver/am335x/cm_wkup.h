#ifndef AM335X_CM_WKUP_H
#define AM335X_CM_WKUP_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_CM_WKUP_t
{
    /*  0 */    volatile uint32_t WKUP_CLKSTCTRL;
    /*  4 */    volatile uint32_t WKUP_CONTROL_CLKCTRL;
    /*  8 */    volatile uint32_t WKUP_GPIO0_CLKCTRL;
    /*  C */    volatile uint32_t WKUP_L4WKUP_CLKCTRL;
    /* 10 */    volatile uint32_t WKUP_TIMER0_CLKCTRL;
    /* 14 */    volatile uint32_t WKUP_DEBUGSS_CLKCTRL;
    /* 18 */    volatile uint32_t L3_AON_CLKSTCTRL;
    /* 1C */    volatile uint32_t AUTOIDLE_DPLL_MPU;
    /* 20 */    volatile uint32_t IDLEST_DPLL_MPU;
    /* 24 */    volatile uint32_t SSC_DELTAMSTEP_DPLL_MPU;
    /* 28 */    volatile uint32_t SSC_MODFREQDIV_DPLL_MPU;
    /* 2C */    volatile uint32_t CLKSEL_DPLL_MPU;
    /* 30 */    volatile uint32_t AUTOIDLE_DPLL_DDR;
    /* 34 */    volatile uint32_t IDLEST_DPLL_DDR;
    /* 38 */    volatile uint32_t SSC_DELTAMSTEP_DPLL_DDR;
    /* 3C */    volatile uint32_t SSC_MODFREQDIV_DPLL_DDR;
    /* 40 */    volatile uint32_t CLKSEL_DPLL_DDR;
    /* 44 */    volatile uint32_t AUTOIDLE_DPLL_DISP;
    /* 48 */    volatile uint32_t IDLEST_DPLL_DISP;
    /* 4C */    volatile uint32_t SSC_DELTAMSTEP_DPLL_DISP;
    /* 50 */    volatile uint32_t SSC_MODFREQDIV_DPLL_DISP;
    /* 54 */    volatile uint32_t CLKSEL_DPLL_DISP;
    /* 58 */    volatile uint32_t AUTOIDLE_DPLL_CORE;
    /* 5C */    volatile uint32_t IDLEST_DPLL_CORE;
    /* 60 */    volatile uint32_t SSC_DELTAMSTEP_DPLL_CORE;
    /* 64 */    volatile uint32_t SSC_MODFREQDIV_DPLL_CORE;
    /* 68 */    volatile uint32_t CLKSEL_DPLL_CORE;
    /* 6C */    volatile uint32_t AUTOIDLE_DPLL_PER;
    /* 70 */    volatile uint32_t IDLEST_DPLL_PER;
    /* 74 */    volatile uint32_t SSC_DELTAMSTEP_DPLL_PER;
    /* 78 */    volatile uint32_t SSC_MODFREQDIV_DPLL_PER;
    /* 7C */    volatile uint32_t CLKDCOLDO_DPLL_PER;
    /* 80 */    volatile uint32_t DIV_M4_DPLL_CORE;
    /* 84 */    volatile uint32_t DIV_M5_DPLL_CORE;
    /* 88 */    volatile uint32_t CLKMODE_DPLL_MPU;
    /* 8C */    volatile uint32_t CLKMODE_DPLL_PER;
    /* 90 */    volatile uint32_t CLKMODE_DPLL_CORE;
    /* 94 */    volatile uint32_t CLKMODE_DPLL_DDR;
    /* 98 */    volatile uint32_t CLKMODE_DPLL_DISP;
    /* 9C */    volatile uint32_t CLKSEL_DPLL_PERIPH;
    /* A0 */    volatile uint32_t DIV_M2_DPLL_DDR;
    /* A4 */    volatile uint32_t DIV_M2_DPLL_DISP;
    /* A8 */    volatile uint32_t DIV_M2_DPLL_MPU;
    /* AC */    volatile uint32_t DIV_M2_DPLL_PER;
    /* B0 */    volatile uint32_t WKUP_WKUP_M3_CLKCTRL;
    /* B4 */    volatile uint32_t WKUP_UART0_CLKCTRL;
    /* B8 */    volatile uint32_t WKUP_I2C0_CLKCTRL;
    /* BC */    volatile uint32_t WKUP_ADC_TSC_CLKCTRL;
    /* C0 */    volatile uint32_t WKUP_SMARTREFLEX0_CLKCTRL;
    /* C4 */    volatile uint32_t WKUP_TIMER1_CLKCTRL;
    /* C8 */    volatile uint32_t WKUP_SMARTREFLEX1_CLKCTRL;
    /* CC */    volatile uint32_t L4_WKUP_AON_CLKSTCTRL;
                volatile uint32_t Reserved;
    /* D4 */    volatile uint32_t WKUP_WDT1_CLKCTRL;
    /* D8 */    volatile uint32_t DIV_M6_DPLL_CORE;
} AM335X_CM_WKUP_t;

#define CM_WKUP_BASE 0x44E00400

#ifndef __KERNEL__
static AM335X_CM_WKUP_t* const CM_WKUP = (AM335X_CM_WKUP_t*)CM_WKUP_BASE;
#else
static AM335X_CM_WKUP_t* CM_WKUP;
#endif

#endif

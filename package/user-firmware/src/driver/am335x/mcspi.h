#ifndef AM335X_MCSPI_H
#define AM335X_MCSPI_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

typedef struct AM335X_MCSPI_CH_t
{
    volatile uint32_t CONF;
    volatile uint32_t STAT;
    volatile uint32_t CTRL;
    volatile uint32_t TX;
    volatile uint32_t RX;
} AM335X_MCSPI_CH_t;

typedef struct AM335X_MCSPI_t
{
    volatile uint32_t REVISION;
    volatile uint32_t Reserved0[67];
    volatile uint32_t SYSCONFIG;
    volatile uint32_t SYSSTATUS;
    volatile uint32_t IRQSTATUS;
    volatile uint32_t IRQENABLE;
    volatile uint32_t Reserved1;
    volatile uint32_t SYST;
    volatile uint32_t MODULCTRL;
    AM335X_MCSPI_CH_t CH[4];
    volatile uint32_t XFERLEVEL;
    volatile uint32_t DAFTX;
    volatile uint32_t DAFRX;
} AM335X_MCSPI_t;

#define MCSPI0_BASE 0x48030000
#define MCSPI1_BASE 0x481A0000

#ifndef __KERNEL__
static AM335X_MCSPI_t* const MCSPI[2] = {(AM335X_MCSPI_t*)MCSPI0_BASE,(AM335X_MCSPI_t*)MCSPI1_BASE};
#else
static AM335X_MCSPI_t* MCSPI[2];
#endif

#define SPI0 0
#define SPI1 1

#define MCSPI_IN_CLK 48000000u

#define MCSPI_SYSCONFIG_CLOCKACTIVITY   (0b11 << 8)
#define MCSPI_SYSCONFIG_SIDLEMODE       (0b11 << 3)
#define MCSPI_SYSCONFIG_SOFTRESET       (1 << 1)
#define MCSPI_SYSCONFIG_AUTOIDLE        (1 << 0)

#define MCSPI_IRQ_RX0_FULL          (1 << 2)
#define MCSPI_IRQ_TX0_EMPTY         (1 << 0)

#define MCSPI_SYST_SSB              (1 << 11)
#define MCSPI_SYST_SPIENDIR         (1 << 10)
#define MCSPI_SYST_SPIDATDIR1       (1 << 9)
#define MCSPI_SYST_SPIDATDIR0       (1 << 8)
#define MCSPI_SYST_SPICLK           (1 << 6)
#define MCSPI_SYST_SPIDAT_1         (1 << 5)
#define MCSPI_SYST_SPIDAT_0         (1 << 4)

#define MCSPI_MODULCTRL_MS          (1 << 2)
#define MCSPI_MODULCTRL_PIN34       (1 << 1)
#define MCSPI_MODULCTRL_SINGLE      (1 << 0)

#define MCSPI_CHCONF_CLKG           (1 << 29)
#define MCSPI_CHCONF_FFER           (1 << 28)
#define MCSPI_CHCONF_FFEW           (1 << 27)
#define MCSPI_CHCONF_TCS            (0b11 << 25)
#define MCSPI_CHCONF_SBPOL          (1 << 24)
#define MCSPI_CHCONF_SBE            (1 << 23)
#define MCSPI_CHCONF_SPIENSLV       (0b11 << 21)
#define MCSPI_CHCONF_FORCE          (1 << 20)
#define MCSPI_CHCONF_TURBO          (1 << 19)
#define MCSPI_CHCONF_IS             (1 << 18)
#define MCSPI_CHCONF_DPE1           (1 << 17)
#define MCSPI_CHCONF_DPE0           (1 << 16)
#define MCSPI_CHCONF_DMAR           (1 << 15)
#define MCSPI_CHCONF_DMAW           (1 << 14)
#define MCSPI_CHCONF_TRM            (0b11 << 12)
#define MCSPI_CHCONF_WL             (0b11111 << 7)
#define MCSPI_CHCONF_EPOL           (1 << 6)
#define MCSPI_CHCONF_CLKD           (0b1111 << 2)
#define MCSPI_CHCONF_POL            (1 << 1)
#define MCSPI_CHCONF_PHA            (1 << 0)

#define MCSPI_CHCTRL_EN             (1 << 0)
#define MCSPI_CHCTRL_EXTCLK         (0xFF << 8)

#define MCSPI_CHSTAT_RXFFE          (1 << 5)
#define MCSPI_CHSTAT_TXFFF          (1 << 4)
#define MCSPI_CHSTAT_TXFFE          (1 << 3)
#define MCSPI_CHSTAT_TXS            (1 << 1)
#define MCSPI_CHSTAT_RXS            (1 << 0)

#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "logging.h"
#include "soc/vmem.h"
#include "soc/spinlock.h"
#include "am335x/cm_per.h"
#include "am335x/spinlock.h"

#ifndef __PRU__

void Spinlock_init (void)
{
    CM_PER->SPINLOCK_CLKCTRL |= CM_CLKCTRL_ENABLE;
    while (CM_PER->SPINLOCK_CLKCTRL & CM_CLKCTRL_IDLEST) ;
    SPINLOCK->SYSCONFIG |= SPINLOCK_SYSCONFIG_SOFTRESET; // reset SPINLOCK
    while (!(SPINLOCK->SYSSTATUS & SPINLOCK_SYSSTATUS_RESETDONE)) ; // wait for SPINLOCK to reset
}

#endif

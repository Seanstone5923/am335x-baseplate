#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include "logging.h"
#include "soc/vmem.h"
#include "am335x/vmem.c"
#include "am335x/pru.c"
#include "soc/irq.h"
#include "am335x/irq.c"
#include "am335x/pru_intc.c"
#include "fifo.c"
#include "pru-rt_chardev.c"

MODULE_DESCRIPTION("PRU-RT");
MODULE_AUTHOR("En Shih <sean@tispace.com>");
MODULE_LICENSE("GPL");

static int __init prurt_init(void)
{
    #pragma GCC diagnostic ignored "-Wdate-time"
    lprintf("Build " __DATE__ " " __TIME__"\n");
    #pragma GCC diagnostic pop
    PRU_init();
    platform_driver_register(&prurt_intc_driver);
    chardev_init();
    lprintf("init done.\n");
    return 0;
}

static void __exit prurt_exit(void)
{
    chardev_cleanup();
    platform_driver_unregister(&prurt_intc_driver);
    PRU_unmap();
    lprintf("exit.\n");
    return;
}

module_init (prurt_init);
module_exit (prurt_exit);

#include <linux/proc_fs.h>
#include <linux/wait.h>
#include <linux/poll.h>

#define DEVICE_NAME "pru-intc"

static int chardev_init(void);
static void chardev_cleanup(void);
static ssize_t chardev_read(struct file *, char *, size_t, loff_t *);
static ssize_t chardev_write(struct file *, const char *, size_t, loff_t *);
static unsigned int chardev_poll(struct file *file, poll_table *wait);

static struct proc_dir_entry *proc_entry;

static struct file_operations fops =
{
    .owner = THIS_MODULE,
    .read = chardev_read,
    .write = chardev_write,
    .poll = chardev_poll,
};

static int chardev_init(void)
{
    proc_entry = proc_create("pru-intc", 0644, NULL, &fops);

    if (proc_entry == NULL) {
       lprintf("Failed to create proc entry\n");
       return -ENOMEM;
    }

    lprintf("Created proc entry /proc/%s\n", DEVICE_NAME);

    return 0; // SUCCESS
}

static void chardev_cleanup(void)
{
    proc_remove(proc_entry);
}

static ssize_t chardev_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
    int bytes_read = 0;

    if (!(FIFO_IsEmpty(IRQ_FIFO)))
    {
        uint8_t sysevent[32];
        int length = FIFO_Get(IRQ_FIFO, sysevent, sizeof(sysevent));
        for (int i = 0; i < length; i++)
        {
            put_user(sysevent[i], &buffer[i]);
        }
        bytes_read = length;
    }

    return bytes_read;
}

static ssize_t chardev_write(struct file *filp, const char *buf, size_t len, loff_t *off)
{
    return -EINVAL;
}

static unsigned int chardev_poll(struct file *file, poll_table *wait)
{
    poll_wait(file, &chardev_waitqueue, wait);
    if (!FIFO_IsEmpty(IRQ_FIFO))
         return POLLIN | POLLRDNORM;
    return 0;
}

#ifndef AM335X_TIMER_H
#define AM335X_TIMER_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

/* 24 MHz timer tick frequnecy */
#define TIMER_CLK 24000000

typedef struct AM335X_DMTIMER_t
{
    /*  0 */ volatile uint32_t TIDR;           /* Identification Register */
    volatile uint32_t Reserved0[3];
    /* 10 */ volatile uint32_t TIOCP_CFG;      /* Timer OCP Configuration Register */
    volatile uint32_t Reserved1[3];
    /* 20 */ volatile uint32_t IRQ_EOI;        /* Timer IRQ End-of-Interrupt Register */
    /* 24 */ volatile uint32_t IRQSTATUS_RAW;  /* Timer Status Raw Register */
    /* 28 */ volatile uint32_t IRQSTATUS;      /* Timer Status Register */
    /* 2C */ volatile uint32_t IRQENABLE_SET;  /* Timer Interrupt Enable Set Register */
    /* 30 */ volatile uint32_t IRQENABLE_CLR;  /* Timer Interrupt Enable Clear Register */
    /* 34 */ volatile uint32_t IRQWAKEEN;      /* Timer IRQ Wakeup Enable Register */
    /* 38 */ volatile uint32_t TCLR;           /* Timer Control Register */
    /* 3C */ volatile uint32_t TCRR;           /* Timer Counter Register */
    /* 40 */ volatile uint32_t TLDR;           /* Timer Load Register */
    /* 44 */ volatile uint32_t TTGR;           /* Timer Trigger Register */
    /* 48 */ volatile uint32_t TWPS;           /* Timer Write Posting Bits Register */
    /* 4C */ volatile uint32_t TMAR;           /* Timer Match Register */
    /* 50 */ volatile uint32_t TCAR1;          /* Timer Capture Register */
    /* 54 */ volatile uint32_t TSICR;          /* Timer Synchronous Interface Control Register */
    /* 58 */ volatile uint32_t TCAR2;          /* Timer Capture Register */
} AM335X_DMTIMER_t;

typedef struct AM335X_DMTIMER_1MS_t
{
    /*  0 */ volatile uint32_t TIDR;
    volatile uint32_t Reserved0[3];
    /* 10 */ volatile uint32_t TIOCP_CFG;
    /* 14 */ volatile uint32_t TISTAT;
    /* 18 */ volatile uint32_t TISR;
    /* 1C */ volatile uint32_t TIER;
    /* 20 */ volatile uint32_t TWER;
    /* 24 */ volatile uint32_t TCLR;
    /* 28 */ volatile uint32_t TCRR;
    /* 2C */ volatile uint32_t TLDR;
    /* 30 */ volatile uint32_t TTGR;
    /* 34 */ volatile uint32_t TWPS;
    /* 38 */ volatile uint32_t TMAR;
    /* 3C */ volatile uint32_t TCAR1;
    /* 40 */ volatile uint32_t TSICR;
    /* 44 */ volatile uint32_t TCAR2;
    /* 48 */ volatile uint32_t TPIR;
    /* 4C */ volatile uint32_t TNIR;
    /* 50 */ volatile uint32_t TCVR;
    /* 54 */ volatile uint32_t TOCR;
    /* 58 */ volatile uint32_t TOWR;
} AM335X_DMTIMER_1MS_t;


#ifndef __KERNEL__
static AM335X_DMTIMER_t* const DMTIMER[8] =
{
    (AM335X_DMTIMER_t*) 0x44E05000,
    0,
    (AM335X_DMTIMER_t*) 0x48040000,
    (AM335X_DMTIMER_t*) 0x48042000,
    (AM335X_DMTIMER_t*) 0x48044000,
    (AM335X_DMTIMER_t*) 0x48046000,
    (AM335X_DMTIMER_t*) 0x48048000,
    (AM335X_DMTIMER_t*) 0x4804A000,
};
static AM335X_DMTIMER_1MS_t* DMTIMER1 = (AM335X_DMTIMER_1MS_t*) 0x44E31000;
#else
static AM335X_DMTIMER_t* DMTIMER[8];
static AM335X_DMTIMER_1MS_t* DMTIMER1;
#endif

#define DMTIMER0 0
#define DMTIMER2 2
#define DMTIMER3 3
#define DMTIMER4 4
#define DMTIMER5 5
#define DMTIMER6 6
#define DMTIMER7 7

#define DMTIMER_TIOCP_CFG_SOFTRESET (1 << 0)

#define DMTIMER_TSICR_SFT       (1 << 1)
#define DMTIMER_TSICR_POSTED    (1 << 2)

#define DMTIMER_TCLR_ST (1 << 0)
#define DMTIMER_TCLR_AR (1 << 1)
#define DMTIMER_TCLR_PRE (1 << 5)
#define DMTIMER_TCLR_CE (1 << 6)

#define DMTIMER_IRQ_MAT  (1 << 0)
#define DMTIMER_IRQ_OVF  (1 << 1)
#define DMTIMER_IRQ_TCAR (1 << 2)

#endif

#ifndef AM335X_GPIO_H
#define AM335X_GPIO_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#include "soc/gpio.h"

typedef struct AM335X_GPIO_t
{
    /* 0   */   volatile uint32_t REVISION;
                volatile uint32_t Reserved0[3];
    /* 10  */   volatile uint32_t SYSCONFIG;
                volatile uint32_t Reserved1[3];
    /* 20  */   volatile uint32_t EOI;
    /* 24  */   volatile uint32_t IRQSTATUS_RAW_0;
    /* 28  */   volatile uint32_t IRQSTATUS_RAW_1;
    /* 2C  */   volatile uint32_t IRQSTATUS_0;
    /* 30  */   volatile uint32_t IRQSTATUS_1;
    /* 34  */   volatile uint32_t IRQSTATUS_SET_0;
    /* 38  */   volatile uint32_t IRQSTATUS_SET_1;
    /* 3C  */   volatile uint32_t IRQSTATUS_CLR_0;
    /* 40  */   volatile uint32_t IRQSTATUS_CLR_1;
    /* 44  */   volatile uint32_t IRQWAKEN_0;
    /* 48  */   volatile uint32_t IRQWAKEN_1;
                volatile uint32_t Reserved2[(0x114-0x4C)/4];
    /* 114 */   volatile uint32_t SYSSTATUS;
                volatile uint32_t Reserved3[(0x130-0x118)/4];
    /* 130 */   volatile uint32_t CTRL;
    /* 134 */   volatile uint32_t OE;
    /* 138 */   volatile uint32_t DATAIN;
    /* 13C */   volatile uint32_t DATAOUT;
    /* 140 */   volatile uint32_t LEVELDETECT0;
    /* 144 */   volatile uint32_t LEVELDETECT1;
    /* 148 */   volatile uint32_t RISINGDETECT;
    /* 14C */   volatile uint32_t FALLINGDETECT;
    /* 150 */   volatile uint32_t DEBOUNCENABLE;
    /* 154 */   volatile uint32_t DEBOUNCINGTIME;
                volatile uint32_t Reserved4[(0x190-0x158)/4];
    /* 190 */   volatile uint32_t CLEARDATAOUT;
    /* 194 */   volatile uint32_t SETDATAOUT;
} AM335X_GPIO_t;

#define GPIO0_BASE 0x44E07000
#define GPIO1_BASE 0x4804C000
#define GPIO2_BASE 0x481AC000
#define GPIO3_BASE 0x481AE000

#ifndef __KERNEL__
static AM335X_GPIO_t* const GPIO[4] =
{
    (AM335X_GPIO_t*) GPIO0_BASE,
    (AM335X_GPIO_t*) GPIO1_BASE,
    (AM335X_GPIO_t*) GPIO2_BASE,
    (AM335X_GPIO_t*) GPIO3_BASE,
};
#else
static AM335X_GPIO_t* GPIO[4];
#endif

#define GPIO_CTRL_DISABLEMODULE (1 << 0)

static inline AM335X_GPIO_t* GPIOx (GPIO_t gpio)
{
    return GPIO[gpio/32];
}

#endif

#ifndef AM335X_DCAN_H
#define AM335X_DCAN_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

/* Beaglebone uses 24 MHz main oscillator */
#define CAN_CLK 24000000

typedef struct __attribute__((__packed__)) AM335X_DCAN_MsgObj_t
{
    uint8_t Data[8]; // in reverse order
    uint8_t DLC : 4;
    uint32_t ID : 29;
    uint8_t Dir : 1;
    uint8_t Xtd : 1;
    uint32_t Msk : 29;
    uint8_t MDir : 1;
    uint8_t MXtd : 1;
    uint8_t EOB : 1;
    uint8_t RmtEn : 1;
    uint8_t RxTE : 1;
    uint8_t TxIE : 1;
    uint8_t UMask : 1;
    uint8_t MsgLst : 1;
    uint8_t Unused : 4;
    uint8_t Parity : 5;
} AM335X_DCAN_MsgObj_t;

typedef struct AM335X_DCAN_t
{
    volatile uint32_t   /*   0h */      CTL     ;    /* CAN Control Register */
    volatile uint32_t   /*   4h */      ES      ;    /* Error and Status Register */
    volatile uint32_t   /*   8h */      ERRC    ;    /* Error Counter Register */
    volatile uint32_t   /*   Ch */      BTR     ;    /* Bit Timing Register */
    volatile uint32_t   /*  10h */      INT     ;    /* Interrupt Register */
    volatile uint32_t   /*  14h */      TEST    ;    /* Test Register */
    volatile uint32_t                   Reserverd0[1];
    volatile uint32_t   /*  1Ch */      PERR    ;    /* Parity Error Code Register */
    volatile uint32_t                   Reserverd1[(0x80-0x1C)/4-1];
    volatile uint32_t   /*  80h */      ABOTR   ;    /* Auto-Bus-On Time Register */
    volatile uint32_t   /*  84h */      TXRQ_X  ;    /* Transmission Request X Register */
    volatile uint32_t   /*  88h */      TXRQ12  ;    /* Transmission Request Register 12 */
    volatile uint32_t   /*  8Ch */      TXRQ34  ;    /* Transmission Request Register 34 */
    volatile uint32_t   /*  90h */      TXRQ56  ;    /* Transmission Request Register 56 */
    volatile uint32_t   /*  94h */      TXRQ78  ;    /* Transmission Request Register 78 */
    volatile uint32_t   /*  98h */      NWDAT_X ;    /* New Data X Register */
    volatile uint32_t   /*  9Ch */      NWDAT12 ;    /* New Data Register 12 */
    volatile uint32_t   /*  A0h */      NWDAT34 ;    /* New Data Register 34 */
    volatile uint32_t   /*  A4h */      NWDAT56 ;    /* New Data Register 56 */
    volatile uint32_t   /*  A8h */      NWDAT78 ;    /* New Data Register 78 */
    volatile uint32_t   /*  ACh */      INTPND_X;    /* Interrupt Pending X Register */
    volatile uint32_t   /*  B0h */      INTPND12;    /* Interrupt Pending Register 12 */
    volatile uint32_t   /*  B4h */      INTPND34;    /* Interrupt Pending Register 34 */
    volatile uint32_t   /*  B8h */      INTPND56;    /* Interrupt Pending Register 56 */
    volatile uint32_t   /*  BCh */      INTPND78;    /* Interrupt Pending Register 78 */
    volatile uint32_t   /*  C0h */      MSGVAL_X;    /* Message Valid X Register */
    volatile uint32_t   /*  C4h */      MSGVAL12;    /* Message Valid Register 12 */
    volatile uint32_t   /*  C8h */      MSGVAL34;    /* Message Valid Register 34 */
    volatile uint32_t   /*  CCh */      MSGVAL56;    /* Message Valid Register 56 */
    volatile uint32_t   /*  D0h */      MSGVAL78;    /* Message Valid Register 78 */
    volatile uint32_t                   Reserverd2[1];
    volatile uint32_t   /*  D8h */      INTMUX12;    /* Interrupt Multiplexer Register 12 */
    volatile uint32_t   /*  DCh */      INTMUX34;    /* Interrupt Multiplexer Register 34 */
    volatile uint32_t   /*  E0h */      INTMUX56;    /* Interrupt Multiplexer Register 56 */
    volatile uint32_t   /*  E4h */      INTMUX78;    /* Interrupt Multiplexer Register 78 */
    volatile uint32_t                   Reserverd3[(0x100-0xE4)/4-1];
    volatile uint32_t   /* 100h */      IF1CMD  ;    /* IF1 Command Registers */
    volatile uint32_t   /* 104h */      IF1MSK  ;    /* IF1 Mask Register */
    volatile uint32_t   /* 108h */      IF1ARB  ;    /* IF1 Arbitration Register */
    volatile uint32_t   /* 10Ch */      IF1MCTL ;    /* IF1 Message Control Register */
    volatile uint32_t   /* 110h */      IF1DATA ;    /* IF1 Data A Register */
    volatile uint32_t   /* 114h */      IF1DATB ;    /* IF1 Data B Register */
    volatile uint32_t                   Reserverd4[2];
    volatile uint32_t   /* 120h */      IF2CMD  ;    /* IF2 Command Registers */
    volatile uint32_t   /* 124h */      IF2MSK  ;    /* IF2 Mask Register */
    volatile uint32_t   /* 128h */      IF2ARB  ;    /* IF2 Arbitration Register */
    volatile uint32_t   /* 12Ch */      IF2MCTL ;    /* IF2 Message Control Register */
    volatile uint32_t   /* 130h */      IF2DATA ;    /* IF2 Data A Register */
    volatile uint32_t   /* 134h */      IF2DATB ;    /* IF2 Data B Register */
    volatile uint32_t                   Reserverd5[2];
    volatile uint32_t   /* 140h */      IF3OBS  ;    /* IF3 Observation Register */
    volatile uint32_t   /* 144h */      IF3MSK  ;    /* IF3 Mask Register */
    volatile uint32_t   /* 148h */      IF3ARB  ;    /* IF3 Arbitration Register */
    volatile uint32_t   /* 14Ch */      IF3MCTL ;    /* IF3 Message Control Register */
    volatile uint32_t   /* 150h */      IF3DATA ;    /* IF3 Data A Register */
    volatile uint32_t   /* 154h */      IF3DATB ;    /* IF3 Data B Register */
    volatile uint32_t                   Reserverd6[2];
    volatile uint32_t   /* 160h */      IF3UPD12;    /* IF3 Update Enable Register 12 */
    volatile uint32_t   /* 164h */      IF3UPD34;    /* IF3 Update Enable Register 34 */
    volatile uint32_t   /* 168h */      IF3UPD56;    /* IF3 Update Enable Register 56 */
    volatile uint32_t   /* 16Ch */      IF3UPD78;    /* IF3 Update Enable Register 78 */
    volatile uint32_t                   Reserverd7[(0x1E0-0x16C)/4-1];
    volatile uint32_t   /* 1E0h */      TIOC    ;    /* CAN TX IO Control Register */
    volatile uint32_t   /* 1E4h */      RIOC    ;    /* CAN RX IO Control Register */
    volatile uint32_t                   Reserverd8[(0x1000-0x1E4)/4-1];
    volatile AM335X_DCAN_MsgObj_t       MsgObj[64];
} AM335X_DCAN_t;

#define DCAN0_BASE 0x481CC000
#define DCAN1_BASE 0x481D0000

#ifndef __KERNEL__
static AM335X_DCAN_t* const DCAN[2] = {(AM335X_DCAN_t*)DCAN0_BASE,(AM335X_DCAN_t*)DCAN1_BASE};
#else
static AM335X_DCAN_t* DCAN[2];
#endif

#define DCAN_CTL_Init             (1u << 0)
#define DCAN_CTL_IE0              (1u << 1)
#define DCAN_CTL_SIE              (1u << 2)
#define DCAN_CTL_EIE              (1u << 3)
#define DCAN_CTL_DAR              (1u << 5)
#define DCAN_CTL_CCE              (1u << 6)
#define DCAN_CTL_Test             (1u << 7)
#define DCAN_CTL_IDS              (1u << 8)
#define DCAN_CTL_ABO              (1u << 9)
#define DCAN_CTL_PMD              (0b1111u << 10)
#define DCAN_CTL_SWR              (1u << 15)
#define DCAN_CTL_InitDbg          (1u << 16)
#define DCAN_CTL_IE1              (1u << 17)
#define DCAN_CTL_DE1              (1u << 18)
#define DCAN_CTL_DE2              (1u << 19)
#define DCAN_CTL_DE3              (1u << 20)
#define DCAN_CTL_PDR              (1u << 24)
#define DCAN_CTL_WUBA             (1u << 25)

#define DCAN_ES_LEC               (0b111 << 0)
#define DCAN_ES_EPass             (1u << 5)
#define DCAN_ES_EWarn             (1u << 6)
#define DCAN_ES_BOff              (1u << 7)

#define DCAN_ERRC_RP              (1u << 15)
#define DCAN_ERRC_REC             (0x7F << 8)
#define DCAN_ERRC_TEC             (0xFF << 0)

#define DCAN_BTR_BRP              (0x3Fu << 0)
#define DCAN_BTR_BRPE             (0xFu << 16)
#define DCAN_BTR_TSeg2            (0x111 << 12)
#define DCAN_BTR_TSeg1            (0b1111 << 8)
#define DCAN_BTR_SJW              (0b11 << 6)

#define DCAN_TEST_LBack           (1u << 4)
#define DCAN_TEST_EXL             (1u << 8)

#define DCAN_IFCMD_Message_Number (0xFFu << 0)
#define DCAN_IFCMD_DMAactive      (1u << 14)
#define DCAN_IFCMD_Busy           (1u << 15)
#define DCAN_IFCMD_Data_B         (1u << 16)
#define DCAN_IFCMD_Data_A         (1u << 17)
#define DCAN_IFCMD_TxRqst_NewDat  (1u << 18)
#define DCAN_IFCMD_ClrIntPnd      (1u << 19)
#define DCAN_IFCMD_Control        (1u << 20)
#define DCAN_IFCMD_Arb            (1u << 21)
#define DCAN_IFCMD_Mask           (1u << 22)
#define DCAN_IFCMD_WR_RD          (1u << 23)

#define DCAN_IFARB_MsgVal         (1u << 31)
#define DCAN_IFARB_Xtd            (1u << 30)
#define DCAN_IFARB_Dir            (1u << 29)
#define DCAN_IFARB_ID28_to_ID0    (0x1FFFFFFFu)

#define DCAN_IFMCTL_NewDat        (1u << 15)
#define DCAN_IFMCTL_MsgLst        (1u << 14)
#define DCAN_IFMCTL_IntPnd        (1u << 13)
#define DCAN_IFMCTL_UMask         (1u << 12)
#define DCAN_IFMCTL_TxIE          (1u << 11)
#define DCAN_IFMCTL_RxIE          (1u << 10)
#define DCAN_IFMCTL_TxRqst        (1u << 8)
#define DCAN_IFMCTL_EoB           (1u << 7)
#define DCAN_IFMCTL_DLC           (0b1111u << 0)

#define DCAN_IF3OBS_Upd           (1u << 15)
#define DCAN_IF3OBS_SDB           (1u << 12)
#define DCAN_IF3OBS_SDA           (1u << 11)
#define DCAN_IF3OBS_SC            (1u << 10)
#define DCAN_IF3OBS_SA            (1u << 9)
#define DCAN_IF3OBS_SM            (1u << 8)
#define DCAN_IF3OBS_DataB         (1u << 4)
#define DCAN_IF3OBS_DataA         (1u << 3)
#define DCAN_IF3OBS_Ctrl          (1u << 2)
#define DCAN_IF3OBS_Arb           (1u << 1)
#define DCAN_IF3OBS_Mask          (1u << 0)

#endif

#include <time.h>
#include "soc/pru.h"
#include "am335x/pru.h"
#include "soc/timer.h"
#include "am335x/dmtimer.h"
#include "soc/timestamp.h"
#include "logging.h"

uint32_t TimespecToTOCR (struct timespec* ts)
{
    return (uint32_t)((((uint64_t)ts->tv_sec * 1000000000 + ts->tv_nsec) / 1000 * 24) / (uint64_t)4294967296ull);
}

void Timestamp_Get(struct timespec* ts)
{
    uint32_t tcrr = DMTIMER1->TCRR;
    uint32_t tocr = DMTIMER1->TOCR;
    uint64_t ns = ((uint64_t)tocr * 4294967296ull + tcrr) * 1000 / 24;
    ts->tv_sec = ns / 1000000000;
    ts->tv_nsec = ns % 1000000000;
}

#ifndef __PRU__

void Timestamp_Init (void)
{
    DMTIMER1->TOWR = 0xFFFFFFFF;
    struct timespec ts;

retry:
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    DMTIMER1->TOCR = TimespecToTOCR(&ts);

    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    if (DMTIMER1->TOCR != TimespecToTOCR(&ts))
        goto retry;
}

void Timestamp_Sync (TimestampSync_t* tsSync)
{
    tsSync->PRU_ack = false;
    tsSync->ARM_ack = false;
    tsSync->delta = 0;
    tsSync->samples = 0;

    PRU_firmware_load(PRU0, "/lib/firmware/timestamp_sync.gnupru.elf");
    Timer_Init(DMTIMER3);
    Timer_SetFreq(DMTIMER3, 1000);
    Timer_Start(DMTIMER3);
    PRU_start(PRU0);

    struct timespec ts;
    while (tsSync->samples < 100)
    {
        if (DMTIMER[3]->IRQSTATUS & DMTIMER_IRQ_OVF)
        {
            clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
            if (!tsSync->ARM_ack)
            {
                tsSync->ARM_ack = true;
                tsSync->ARM_ns = ts.tv_nsec;

                if (tsSync->PRU_ack)
                {
                    Timer_ClearPendingIrq(DMTIMER3, DMTIMER_IRQ_OVF);
                    int32_t delta = (int32_t)tsSync->PRU_ns - tsSync->ARM_ns;
                    lprintf("delta: %d\n", delta);
                    tsSync->delta += delta;
                    tsSync->samples++;
                    tsSync->PRU_ack = false;
                    tsSync->ARM_ack = false;
                }
            }
        }
    }

    tsSync->delta /= tsSync->samples;
    lprintf("%u samples: delta=%d\n", tsSync->samples, tsSync->delta);

    PRU_halt(PRU0);
    Timer_Stop(DMTIMER3);
}

#endif

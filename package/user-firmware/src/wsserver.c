#ifndef __PRU__

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>
#include <libwebsockets.h>
#include "util/wsserver.h"
#include "logging.h"
#include "fifo.h"

pthread_t WsServer_thread;

WsServer_Handler_t WsServer_Handler;
WsServer_ConnectHandler_t WsServer_ConnectHandler;

#define WS_FIFO_LENGTH 10
FIFO_t* WsFIFO;
uint8_t WsFifoBuffer[sizeof(FIFO_t) + WS_FIFO_LENGTH * sizeof(WsMsg_t)];

/* one of these is created for each client connecting to us */
struct per_session_data {
    struct per_session_data *pss_list;
    struct lws *wsi;
};

/* one of these is created for each vhost our protocol is used with */
struct per_vhost_data {
    struct per_session_data *pss_list; /* linked-list of live pss*/
};

static int WsServer_Callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

static struct lws_protocols protocols[] =
{
    { "http", lws_callback_http_dummy, 0, 0 },
    {
        .name                   = "lws-minimal",
        .callback               = WsServer_Callback,
        .per_session_data_size  = sizeof(struct per_session_data),
        .rx_buffer_size         = 1024,
        .id                     = 0,
        .user                   = NULL,
        .tx_packet_size         = 0,
    },
    { NULL, NULL, 0, 0 } /* terminator */
};

static struct lws_http_mount mount = {
    .mount_next             = NULL,   /* linked-list "next" */
    .mountpoint             = "/",    /* mountpoint URL */
    .origin                 = "/srv", /* serve from dir */
    .def                    = "index.html",   /* default filename */
    .protocol               = NULL,
    .cgienv                 = NULL,
    .extra_mimetypes        = NULL,
    .interpret              = NULL,
    .cgi_timeout            = 0,
    .cache_max_age          = 0,
    .auth_mask              = 0,
    .cache_reusable         = 0,
    .cache_revalidate       = 0,
    .cache_intermediaries   = 0,
    .origin_protocol        = LWSMPRO_FILE, /* files in a dir */
    .mountpoint_len         = 1, /* char count */
    .basic_auth_login_file  = NULL,
};

static struct per_vhost_data *vhd;

static int WsServer_Callback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len)
{
    struct per_session_data *pss = (struct per_session_data *)user;
    //vhd = (struct per_vhost_data *) lws_protocol_vh_priv_get(lws_get_vhost(wsi), lws_get_protocol(wsi));

    switch (reason)
    {
        case LWS_CALLBACK_PROTOCOL_INIT:
            lwsl_user("Protocol init\r\n");
            vhd = lws_protocol_vh_priv_zalloc(lws_get_vhost(wsi), lws_get_protocol(wsi), sizeof(struct per_vhost_data));
            break;

        case LWS_CALLBACK_ESTABLISHED:
            lwsl_user("Connection established\r\n");
            /* add ourselves to the list of live pss held in the vhd */
            lws_ll_fwd_insert(pss, pss_list, vhd->pss_list);
            pss->wsi = wsi;
            lws_callback_on_writable(wsi);
            if (WsServer_ConnectHandler) WsServer_ConnectHandler();
            break;

        case LWS_CALLBACK_CLOSED:
            lwsl_user("Connection closed\r\n");
            /* remove our closing pss from the list of live pss */
            lws_ll_fwd_remove(struct per_session_data, pss_list, pss, vhd->pss_list);
            break;

        case LWS_CALLBACK_SERVER_WRITEABLE:
        {
            //lwsl_user("LWS_CALLBACK_SERVER_WRITEABLE\r\n");
            if (!FIFO_IsEmpty(WsFIFO))
            {
                WsMsg_t* msg = FIFO_PeekOut(WsFIFO);
                /* notice we allowed for LWS_PRE in the payload already */
                int m = lws_write(wsi, msg->payload + LWS_PRE, msg->len, msg->protocol);
                if (m < (int)msg->len) {
                    lwsl_err("ERROR %d writing to ws\n", m);
                    return -1;
                }
                FIFO_AdvanceOut(WsFIFO, 1);
            }

            if (!FIFO_IsEmpty(WsFIFO))
                lws_callback_on_writable(wsi);
        } break;

        case LWS_CALLBACK_RECEIVE:
        {
            //lwsl_user("Receive: len %u\r\n", len);
            if (WsServer_Handler) WsServer_Handler(in, len);
        } break;

        default:
            break;
    }

    return 0;
}

void WsServer_SetHandler (WsServer_Handler_t handler)
{
    WsServer_Handler = handler;
}

void WsServer_SetConnectHandler (WsServer_ConnectHandler_t handler)
{
    WsServer_ConnectHandler = handler;
}

int WsServer_Start (const char* docroot, const char* index)
{
    WsFIFO = FIFO_Init((void *)WsFifoBuffer, WS_FIFO_LENGTH, sizeof(WsMsg_t));

    mount.origin = docroot;
    mount.def = index;
    if (pthread_create(&WsServer_thread, NULL, &WsServer_Main, NULL) < 0)
    {
        lprintf("Error creating LWS server thread\r\n");
        return -1;
    }
    return 0;
}

void* WsServer_Main (void* settings)
{
    lws_set_log_level(LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
            /* for LLL_ verbosity above NOTICE to be built into lws,
             * lws must have been configured and built with
             * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
            /* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
            /* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
            /* | LLL_DEBUG */, NULL);

    struct lws_context_creation_info info =
    {
        .port       = 80,
        .mounts     = &mount,
        .protocols  = protocols,
        .pt_serv_buf_size = 32 * 1024,
    };
    struct lws_context * context = lws_create_context(&info);
    if (!context) {
        lwsl_err("lws init failed\n");
        return 0;
    }

    int n = 0;
    while (n >= 0)
        n = lws_service(context, 1000);

    lws_context_destroy(context);

    return 0;
}

int WsServer_Send(const char* str, int len, enum lws_write_protocol protocol)
{
    if (!WsFIFO)
    {
        //lprintf("WsFIFO not initialized!\n");
        return -1;
    }

    if (FIFO_IsFull(WsFIFO))
    {
        //lprintf("WsFIFO full!\n");
        return -1;
    }

    WsMsg_t* msg = FIFO_PeekIn(WsFIFO);
    msg->len = len;
    msg->protocol = protocol;
    memcpy(msg->payload + LWS_PRE, str, len);
    FIFO_AdvanceIn(WsFIFO, 1);

    if (vhd)
    {
        /*
         * let everybody know we want to write something on them
         * as soon as they are ready
         */
        lws_start_foreach_llp(struct per_session_data **, ppss, vhd->pss_list)
        {
            lws_callback_on_writable((*ppss)->wsi);
        }
        lws_end_foreach_llp(ppss, pss_list);
    }

    return 0;
}

int WsServer_vprintf(const char* fmt, va_list args)
{
    char buffer[1024];
    int n = vsnprintf(buffer, sizeof(buffer), fmt, args);
    WsServer_Send(buffer, n, LWS_WRITE_TEXT);
    return n;
}

#endif

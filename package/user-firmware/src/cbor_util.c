#ifndef __PRU__

#include <stdint.h>
#include <cbor.h>
#include "cbor_util.h"

void CBOR_add_Uint32(cbor_item_t * item, const char* name)
{
    cbor_map_add(item, (struct cbor_pair) {
        .key = cbor_build_string(name),
        .value = cbor_build_uint32(0)
    });
}

void CBOR_add_Uint8(cbor_item_t * item, const char* name)
{
    cbor_map_add(item, (struct cbor_pair) {
        .key = cbor_build_string(name),
        .value = cbor_build_uint8(0)
    });
}

#endif

#include <fifo.h>
#include <minmax.h>
#include <myassert.h>
#ifdef __KERNEL__
#include <linux/string.h>
#else
#include <string.h>
#endif
#include <logging.h>

#define NEXT(p, offset) ({             \
    uint32_t next = fifo->p + offset;  \
    if (next >= fifo->maxIndex)        \
        next -= fifo->maxIndex;        \
    next;                              \
})

static size_t LenInBytes(const FIFO_t *fifo) {
    uint32_t in = fifo->in, out = fifo->out;
    return in == out ? 0 : in > out ? in - out : fifo->maxIndex - out + in;
}

static size_t AvailInBytes(const FIFO_t *fifo) {
    return fifo->maxIndex - fifo->itemSize - LenInBytes(fifo);
}

FIFO_t *FIFO_Init(void *mem, size_t capacity, size_t itemSize)
{
    if (!mem)
        return NULL;

    FIFO_t *fifo = mem;
    fifo->in = 0;
    fifo->out = 0;
    fifo->maxIndex = capacity * itemSize;
    fifo->itemSize = itemSize;
    fifo->AdvanceOutCallback = (typeof(fifo->AdvanceOutCallback)) NULL;

    memset(fifo->data, 0, fifo->maxIndex);

    return fifo;
}

size_t FIFO_Put(FIFO_t *fifo, void *item, size_t len)
{
    if (FIFO_IsFull(fifo))
        return 0;
    uint32_t in = fifo->in;
    size_t itemsToPut = MIN(len, AvailInBytes(fifo) / fifo->itemSize);
    size_t bytesToPut = itemsToPut * fifo->itemSize;
    size_t wrapLen = fifo->maxIndex - in;
    if (bytesToPut <= wrapLen) {
        memcpy(fifo->data + in, item, bytesToPut);
    } else {
        memcpy(fifo->data + in, item, wrapLen);
        memcpy(fifo->data, (uint8_t *)item + wrapLen, bytesToPut - wrapLen);
    }
    FIFO_AdvanceIn(fifo, itemsToPut);
    return itemsToPut;
}

size_t FIFO_Get(FIFO_t *fifo, void *item, size_t len)
{
    if (FIFO_IsEmpty(fifo))
        return 0;
    uint32_t out = fifo->out;
    size_t itemsToGet = MIN(len, LenInBytes(fifo) / fifo->itemSize);
    size_t bytesToGet = itemsToGet * fifo->itemSize;
    size_t wrapLen = fifo->maxIndex - out;
    if (bytesToGet <= wrapLen) {
        memcpy(item, fifo->data + out, bytesToGet);
    } else {
        memcpy(item, fifo->data + out, wrapLen);
        memcpy((uint8_t *)item + wrapLen, fifo->data, bytesToGet - wrapLen);
    }
    FIFO_AdvanceOut(fifo, itemsToGet);
    return itemsToGet;
}

void *FIFO_PeekIn(FIFO_t *fifo)
{
    return fifo->data + fifo->in;
}

void *FIFO_PeekOut(FIFO_t *fifo)
{
    return fifo->data + fifo->out;
}

void *FIFO_PeekN(FIFO_t *fifo, uint32_t n)
{
    Assert(n < FIFO_Length(fifo));
    uint32_t offset = fifo->out + n * fifo->itemSize;
    if (offset >= fifo->maxIndex)
        offset -= fifo->maxIndex;
    return fifo->data + offset;
}

void *FIFO_AdvanceIn(FIFO_t *fifo, size_t num)
{
    uint32_t offset = num * fifo->itemSize;
    Assert(offset <= AvailInBytes(fifo));
    fifo->in = NEXT(in, offset);
    return FIFO_PeekIn(fifo);
}

void *FIFO_AdvanceOut(FIFO_t *fifo, size_t num)
{
    uint32_t offset = num * fifo->itemSize;
    Assert(offset <= LenInBytes(fifo));

    #ifndef __PRU__
    if (fifo->AdvanceOutCallback)
    {
        for (int i = 0; i < num; i++)
        {
            fifo->AdvanceOutCallback(fifo->data + NEXT(out, i * fifo->itemSize));
        }
    }
    #endif

    fifo->out = NEXT(out, offset);
    return FIFO_PeekOut(fifo);
}

bool FIFO_IsFull(const FIFO_t *fifo)
{
    return NEXT(in, fifo->itemSize) == fifo->out ? true : false;
}

bool FIFO_IsEmpty(const FIFO_t *fifo)
{
    return fifo->in == fifo->out;
}

size_t FIFO_Length(const FIFO_t *fifo)
{
    return LenInBytes(fifo) / fifo->itemSize;
}

size_t FIFO_Available(const FIFO_t *fifo)
{
    return AvailInBytes(fifo) / fifo->itemSize;
}

void FIFO_Reset(FIFO_t *fifo)
{
    fifo->in = fifo->out = 0;
    memset(fifo->data, 0, fifo->maxIndex);
}

#include <myassert.h>
#include <logging.h>
#ifndef __PRU__
#include <stdlib.h>
#endif

void _myassert(const char *exp, const char *file, int line)
{
    lprintf("Assertion '%s' failed, file %s line %d.\n", exp, file, line);
#ifdef __PRU__
    __halt();
#else
    abort();
#endif
}

################################################################################
#
# gnupru
#
################################################################################

GNUPRU_VERSION = 91292a1
GNUPRU_SITE = https://github.com/dinuxbg/gnupru.git
GNUPRU_SITE_METHOD := git

define HOST_GNUPRU_BUILD_CMDS
	export PREFIX=$(HOST_DIR) ;\
	cd $(@D) ;\
	./download-and-patch.sh ;\
	$(HOST_CONFIGURE_OPTS) ./build.sh
endef

$(eval $(host-generic-package))

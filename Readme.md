# Overview

# Basic Usage

## 1. Init Git Submodules

```shell
$ git submodule init
$ git submodule update
```

## 2. Build System Image

For AM335x,

```shell
$ make am335x
```

For Raspberry Pi Zero (BCM2835),

```shell
$ make rpi0
```

The whole build process takes about an hour on a typical PC. The built image is located at `build/buildroot/output/images/sdcard.img`

## 3. Flash built image to SD card

If ```/dev/sdx``` refers to the SD card,
```shell
$ make flash-sdx
```

This will flash `build/buildroot/output/images/sdcard.img` onto the SD card.

# Makefile targets

## Select Platform

* Configure for AM335x

    ```console
    $ make config-am335x
    ```

    This will configure Buildroot for the AM335x target. The Buildroot output directory will be redirected by symlinking `build/buildroot/output` to `build/buildroot/output-am335x`.

* Configure for Raspberry Pi Zero

    ```console
    $ make config-rpi0
    ```

    This will configure Buildroot for the Raspberry PiZero target. The Buildroot output directory will be redirected by symlinking `build/buildroot/output` to `build/buildroot/output-rpi0`.

## Build

* Build toolchain for current platform:

    ```console
    $ make toolchain-build
    ```

    This will configure and build the toolchain to `build/buildroot/output/toolchain`.

* Build system image for current platform (toolchain will be built if not already built):

    ```console
    $ make
    ```

    This will build packages and finally the system image for the chosen target to `build/buildroot/output`.

* Configure for AM335x & build system image:

    ```console
    $ make am335x
    ```

    This will execute `make config-am335x`and `make`in a single step.

* Configure for Raspberry Pi Zero & build system image:

    ```console
    $ make rpi0
    ```

    This will execute `make config-rpi0`and `make`in a single step.

* Build firmware & user app only:

    ```console
    $ make fw
    ```

    This will build only the packages ([`package`](package)) and not the system image. Useful while developing when the system image is already built and minor modifications need to be done to the user space software.

## Upload

* Upload user app after modifications:

    ```console
    $ make upload
    ```

    This will upload the built binaries onto the target via SSH. It is required that the system already be built and the target running the system is connected to the host PC via USB. Useful while developing when the system image is already built and minor modifications need to be done to the user space software.

## Modifying configuration

* Modify Buildroot main configuration:

    ```console
    $ make menuconfig
    ```

* Modify Linux configuration:

    ```console
    $ make linux-menuconfig
    ```

* Modify u-boot configuration:

    ```console
    $ make uboot-menuconfig
    ```

* Modify Busybox configuration:

    ```console
    $ make busybox-menuconfig
    ```

## Update configs after modifications

* Update Buildroot main configuration:

    ```console
    $ make savedefconfig
    ```

* Update Linux configuration:

    ```console
    $ make linux-update-config
    ```

* Update u-boot configuration:

    ```console
    $ make uboot-update-config
    ```

* Update Busybox configuration:

    ```console
    $ make busybox-update-config
    ```

## Clean

* Clean the whole build (except toolchain):

    ```console
    $ make clean
    ```

* Clean Linux only:

    ```console
    $ make linux-dirclean
    ```

* Clean firmware only:

    ```console
    $ make fw-clean
    ```

* Clean toolchain:

    ```console
    $ make toolchain-clean
    ```

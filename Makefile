SHELL := /bin/bash

BUILD_DIR = $(CURDIR)/build
TARGET_DIR = $(BUILD_DIR)/buildroot/output/target

################################################################################
# Buildroot
################################################################################

default: toolchain-build
	$(MAKE) O=$(BUILD_DIR)/buildroot/output BR2_EXTERNAL=$(CURDIR) -C buildroot

.PHONY: prep
prep:
	@ for file in patches/*; do \
		if ! patch -d buildroot -Rf --dry-run --silent -p1 < $$file > /dev/null; then \
			patch -d buildroot -p1 < $$file; \
		fi \
	done

## Pass Makefile targets not defined here to buildroot
%:
	$(MAKE) O=$(BUILD_DIR)/buildroot/output BR2_EXTERNAL=$(CURDIR) -C buildroot $*

## Initial setup for specific platform
.PHONY: config-%
config-%: prep
	mkdir -p $(BUILD_DIR)/buildroot/output-$*
	rm -rf $(BUILD_DIR)/buildroot/output
	cd $(BUILD_DIR)/buildroot && ln -sf output-$* output && cd -
	$(MAKE) O=$(BUILD_DIR)/buildroot/output BR2_EXTERNAL=$(CURDIR) BR2_DEFCONFIG=$(CURDIR)/configs/$*/config -C buildroot defconfig

## Toolchain for specific platform
.PHONY: toolchain toolchain-build toolchain-%
toolchain: $(BUILD_DIR)/buildroot/output/toolchain/host

$(BUILD_DIR)/buildroot/output/toolchain/host:
	mkdir -p $@
	$(MAKE) toolchain-build

toolchain-build: toolchain-defconfig
	PLATFORM=$$(readlink $(BUILD_DIR)/buildroot/output | cut -d'-' -f 2);\
	$(MAKE) O=$(BUILD_DIR)/buildroot/output/toolchain BR2_EXTERNAL=$(CURDIR) BR2_DEFCONFIG=$(CURDIR)/configs/$$PLATFORM/toolchain.config -C buildroot

toolchain-%: toolchain
	mkdir -p $(BUILD_DIR)/buildroot/output/toolchain
	PLATFORM=$$(readlink $(BUILD_DIR)/buildroot/output | cut -d'-' -f 2);\
	$(MAKE) O=$(BUILD_DIR)/buildroot/output/toolchain BR2_EXTERNAL=$(CURDIR) BR2_DEFCONFIG=$(CURDIR)/configs/$$PLATFORM/toolchain.config -C buildroot $*

## Initial setup for AM335x

.PHONY: am335x
am335x: config-am335x
	$(MAKE)

## Initial setup for Raspberry Pi Zero (BCM2835)
.PHONY: rpi0
rpi0: config-rpi0
	$(MAKE)

## Firmware
.PHONY: fw
fw: user-firmware-reconfigure

.PHONY: fw-clean
fw-clean: user-firmware-dirclean

################################################################################
# Deployment
################################################################################

.PHONY: clean-target
clean-target:
	rm -rf $(BUILD_DIR)/buildroot/output/target
	find $(BUILD_DIR)/buildroot/output/ -name ".stamp_target_installed" |xargs rm -rf

define flash =
@mkdir -p mnt
@sudo umount mnt || true
@sudo umount /dev/$*?* || true
@sudo dd if=$(BUILD_DIR)/buildroot/output/images/sdcard.img of=/dev/$* bs=4k
@sync
@sudo configs/expand-rootfs.sh /dev/$* || true
@sudo e2fsck -f /dev/$*2 || true
@sudo resize2fs /dev/$*2
@sudo mount /dev/$*2 mnt
endef

## Flash built image to SD card, e.g., make flash-sdx.
.PHONY: flash-%
flash-%:
	$(eval NAME := $(shell echo $(DEV) | grep -P '\D+' -o))
	$(flash)
	@sudo mount /dev/$*1 mnt/boot
	@sudo sed -i s/obu/$(NAME)/g mnt/boot/uEnv.txt
	@sudo dd if=/dev/zero of=mnt/opt/vfat.img count=32 bs=1M
	@sudo mkfs.vfat mnt/opt/vfat.img
	@sudo fatlabel mnt/opt/vfat.img $$(echo $(NAME) | tr [a-z] [A-Z])
	@sleep 3
	@sync
	@sudo umount /dev/$*?*

.PHONY: flash-rpi-%
flash-rpi-%:
	$(flash)
	@sudo umount mnt

# Use more specific pattern to avoid matching both, which the variable with
# longer stem is defined first.
flash-sd% upload sys-upload srv: DEV ?= bbb
flash-rpi-sd%: DEV ?= rpi0

SSH_KEY = configs/am335x/rootfs/root/.ssh/id_rsa

.PHONY: upload
upload:
	mkdir -p $(TARGET_DIR)/cf
	rsync -avzz -e "ssh -i $(SSH_KEY)" --chown=root:root $(TARGET_DIR)/{lib,bin,usr,srv,cf}  root@$(DEV).local:/
	if [ $(OVERWRITE) = true ]; then \
		rsync -avzz -e "ssh -i $(SSH_KEY)" --chown=root:root --delete $(TARGET_DIR)/etc  root@$(DEV).local:/; \
	else \
		rsync -avzz -e "ssh -i $(SSH_KEY)" --chown=root:root --ignore-existing $(TARGET_DIR)/etc  root@$(DEV).local:/; \
	fi

.PHONY: upload-%
upload-%:
	DEV=$* $(MAKE) upload

.PHONY: upload-overwrite
upload-overwrite:
	OVERWRITE=true $(MAKE) upload

.PHONY: upload-overwrite-%
upload-overwrite-%:
	DEV=$* OVERWRITE=true $(MAKE) upload

.PHONY: sys-upload
sys-upload:
	rsync -avzz -e "ssh -i $(SSH_KEY)" --chown=root:root --inplace $(BUILD_DIR)/buildroot/output/images/{MLO,u-boot.img,*.dtb,*.txt,zImage}  root@$(DEV).local:/boot/

.PHONY: sys-upload-%
sys-upload-%:
	DEV=$* $(MAKE) sys-upload

## Copy HTTP server files to device
.PHONY: srv-%
srv-%:
	DEV=$* $(MAKE) srv

.PHONY: srv
srv:
	rsync -avzz -e "ssh -i $(SSH_KEY)" --chown=root:root package/user-firmware/www/*  root@$(DEV).local:/srv/
	#cd $(BUILD_DIR)/buildroot/output/target/srv; echo CACHE MANIFEST > cache.manifest; echo \# $$(date) >> cache.manifest; find * -type f \( ! -iname "cache.manifest" \) >> cache.manifest

################################################################################
# Debug
################################################################################
.PHONY: debug
debug:
	sudo openocd \
		-s "/usr/share/openocd/scripts" \
		-f interface/xds110.cfg \
		-c "transport select jtag" \
		-c "adapter_khz 16000" \
		-c "reset_config trst_and_srst" \
		-f target/am335x.cfg \
		-c "init" \
		-c "reset halt"

.PHONY: gdb
gdb:
	$(BUILD_DIR)/buildroot/output/host/bin/arm-linux-gdb -tui \
		-ex "set pagination off" \
		-ex "target remote localhost:3333" \
		-ex "layout split" \
		-ex "layout regs" \
		-ex "tui reg all" \

#!/bin/sh

set -u
set -e

if [ ! -d ${TARGET_DIR}/boot ]; then
	mkdir ${TARGET_DIR}/boot
fi

cp ${BINARIES_DIR}/*.dtb ${TARGET_DIR}/boot
cp ${BINARIES_DIR}/zImage ${TARGET_DIR}/boot

# For faster boot-up, see #104. Ignore the error when the file has renamed
# already.
cd ${TARGET_DIR}/etc/init.d/
mv S*avahi-daemon S23avahi-daemon || true
mv S*dropbear S24dropbear || true

rm ${TARGET_DIR}/etc/init.d/S40network
